<?php

namespace app\admin\controller;

use app\common\model\ModuleField as Obj;
use app\common\model\Module as ModuleObj;
use think\Request;

/**
 * 模块标签管理
 * Class AModuleTags
 * @package app\admin\controller
 */
class AModuleTags extends Base
{

    public $obj, $now_keyword = "模块标签";

    public function initialize()
    {
        parent::initialize(); // TODO: Change the autogenerated stub

        $this->obj = new Obj();

        $this->is_search = false;

    }


    public function index(Request $request) {

        $module_id = $request->get('id/d',0);

        $page = $request->get('page/d',1);

        $limit = $request->get('limit/d',1);

        $module_info = ModuleObj::where([

            ['id','eq',$module_id]

        ])->find();

        if(empty($module_info)) $this->error("模块不存在");

        $where = [

            ['a.is_del','eq',Obj::NO_DEL],

            ['a.module_id','eq',$module_info['id']]

        ];

        $search_where = $request->get('where/s',"");

        if(!empty($search_where)) $where = array_merge($where,json2array($search_where));

        $result = $this->defaultIndex([

            "field_list" => [

                ['field'=>'id_str','title'=>"#"],
                ['field'=>'name','title'=>"标签名称"],
                ['field'=>'price','title'=>"价格"],
                ['field'=>'sort','title'=>"排序"],
                ['field'=>'create_time','title'=>"创建时间"],
                ['field'=>'caozuo','title'=>"操作"],

            ],

            "is_customer_toolbar" => 1,

            "limit" => $limit,

            "order" => "a.sort DESC,a.id DESC",

            "where" => $where,

            "is_add" => $module_info['is_allow_operate'] == 1 ? false : true,

            "sub_title" => $module_info['name']."の标签列表"


        ]);

        if(!$request->isAjax()) return $result;

        if (!empty($result['data'])) {

            foreach ($result['data'] as $k => &$v) {

                $v['id_str'] = ($limit * ($page - 1)) + ($k + 1);

                $v['create_time'] = date("Y-m-d H:i:s",$v['create_time']);

                $caozuo = [

                    $this->updateButton()

                ];

                if($module_info['is_allow_operate'] != 1) array_push($caozuo,$this->deleteButton());

                $v['caozuo'] = implode("",$caozuo);

            }
        }

        $this->success([

            'param' => $result

        ]);

    }



    private function getFieldList($id = 0) {

        $field_list = [

            ['form_type'=>"text","name"=>"name",'title'=>"名称",'value'=>""],
            ['form_type'=>"text","name"=>"price",'title'=>"价格",'value'=>""],
            ['form_type'=>"text","name"=>"sort",'title'=>"排序",'value'=>""],

        ];

        return $field_list;

    }


    private function getPostData(Request $request) {

        $field_list = array_column($this->getFieldList(),"name");

        $data = $request->only($field_list,"post");

        return $data;

    }



    public function add(Request $request) {

        $module_id = $request->get('module_id/d',0);

        if ($request->isPost()) {

            try{

                $module_info = ModuleObj::where([

                    ['id','eq',$module_id]

                ])->find();

                if(empty($module_info)) $this->error("模块不存在");

                if($module_info['is_allow_operate'] == 1) throw new \Exception("禁止添加");

                $data = $this->getPostData($request);

                if(isset($data['name']) && !empty($data['name'])) {

                    $is_exists = Obj::where([

                        ['name','eq',$data['name']],

                        ['is_del','eq',Obj::NO_DEL],

                        ['module_id','eq',$module_id]

                    ])->find();

                    if(!empty($is_exists)) throw new \Exception("该模块下标签名称已存在");

                }

                $data['module_id'] = $module_id;

                $data['create_time'] = time();

                Obj::insertGetId($data);

            }catch (\Exception $e) {

                $this->error($e->getMessage());

            }

            $this->success('添加成功');

        } else {

            $field_list = $this->getFieldList();

            return view('form', [

                'field_list' => $field_list,

                'title' => "添加{$this->now_keyword}",

            ]);
        }

    }


    public function publish(Request $request) {

        $id = $request->get('id/d',0);

        if($id < 1) $this->error('参数有误');

        if($request->isPost()) {

            try{

                $info = Obj::where('id',$id)->find();

                $data = $this->getPostData($request);

                if(isset($data['name']) && !empty($data['name'])) {

                    $is_exists = Obj::where([

                        ['name','eq',$data['name']],

                        ['id','neq',$id],

                        ['is_del','eq',Obj::NO_DEL],

                        ['module_id','eq',$info['module_id']]

                    ])->find();

                    if(!empty($is_exists)) throw new \Exception("该模块名称已存在");

                }

                Obj::where('id',$id)->update($data);

            }catch (\Exception $e) {

                $this->error($e->getMessage());

            }

            $this->success("修改成功");

        }else{

            $field_list = $this->getFieldList($id);

            $fields = array_column($field_list,"name");

            $info = Obj::field($fields)->where('id',$id)->find();

            $render_array = [

                'field_list' => $field_list,

                'title' => "修改{$this->now_keyword}",

                'info' => $info

            ];

            return view('form', $render_array);

        }

    }


    public function del(Request $request) {

        try{

            $id = $request->get('id/d',0);

            if($id < 1) throw new \Exception("参数有误");

            $is_exists = Obj::where([

                ['id','eq',$id],

                ['is_del','eq',Obj::NO_DEL]

            ])->find();

            if(empty($is_exists)) throw new \Exception("数据不存在");

            if($is_exists['is_allow_del'] == 1) throw new \Exception("禁止删除");

            $module_info = ModuleObj::where([

                ['id','eq',$is_exists['module_id']]

            ])->find();

            if(empty($module_info)) $this->error("模块不存在");

            if($module_info['is_allow_operate'] == 1) throw new \Exception("禁止删除");

            Obj::where('id',$is_exists['id'])->update([

                "is_del" => Obj::YES_DEL

            ]);

        }catch (\Exception $e) {

            $this->error($e->getMessage());

        }

        $this->success("已删除");

    }


}