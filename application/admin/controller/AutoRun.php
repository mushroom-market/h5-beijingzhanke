<?php


namespace app\admin\controller;

use app\common\model\Product as ProductObj;
use app\common\model\ProductStatistics;

class AutoRun {


    public function autoShowProduct() {

        try{

            $productObj = new ProductObj();

            $productList = $productObj->where([

                ['is_del','eq',ProductObj::NO_DEL],

                ['status','eq',2],

            ])->field("id,show_time,hide_time")->limit(0,100)->select();

            if($productList->isEmpty()) throw new \Exception("暂无更新数据");

            $productList = $productList->toArray();

            $num = 0;

            foreach ($productList as $v) {

                $time = strtotime("+1 days",date("Y-m-d 00:00:00",$v['hide_time'])) + $v['show_time'];

                if($time < time()) continue;

                $productObj->where([

                    ['id','eq',$v['id']]

                ])->update([

                    "status" => ProductObj::STATUS_NORMAL,

                ]);

                $num ++;

            }

        }catch (\Exception $e) {

            echo $e->getMessage();exit;

        }

        echo "已完成{$num}个";exit;

    }



    public function autoCreateDayStatistics() {

        $time = date("Ymd",time());

        $product_list = ProductObj::where([

            ['is_del','eq',ProductObj::NO_DEL]

        ])->field("id,day_uv_max_num,settlement_type,one_uv_price,one_register_price,return_commission_rate")->select();

        $product_list = $product_list->isEmpty() ? [] : $product_list->toArray();

        foreach ($product_list as $v) {

            $is_exists = ProductStatistics::where([

                ['product_id','eq',$v['id']],

                ['time','eq',$time]

            ])->field("id")->find();

            if(empty($is_exists)) {

                $unit_price = 0;

                if($v['settlement_type'] == 1) $unit_price = $v['one_uv_price'];

                if($v['settlement_type'] == 2) $unit_price = $v['one_register_price'];

                if($v['settlement_type'] == 3) $unit_price = $v['return_commission_rate'];

                ProductStatistics::insertGetId([

                    "product_id" => $v['id'],

                    "unit_price" => $unit_price,

                    "settlement_type" => $v['settlement_type'],

                    "day_uv_max_num" => $v['day_uv_max_num'],

                    "time" => $time,

                    "create_time" => time()

                ]);

            }

        }

    }




}