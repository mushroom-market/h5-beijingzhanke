<?php

namespace app\admin\controller;

use app\common\model\Channel as Obj;
use think\Request;
use think\Validate;

/**
 * 渠道商管理
 * Class Channel
 * @package app\admin\controller
 */
class Channel extends Base
{

    public $obj, $now_keyword = "渠道商";

    public function initialize()
    {
        parent::initialize(); // TODO: Change the autogenerated stub

        $this->obj = new Obj();

    }


    public function index(Request $request) {

        $where = [

            ['a.is_del','eq',Obj::NO_DEL]

        ];

        $field_list = [

            ['field'=>'id','title'=>"#"],
            ['field'=>'name','title'=>"账号"],
            ['field'=>'company_name','title'=>"公司名称"],
            ['field'=>'real_name','title'=>"姓名"],
            ['field'=>'mobile','title'=>"手机号"],
            ['field'=>'data_scale','title'=>"数据比例"],
            ['field'=>'status','title'=>"状态"],
            ['field'=>'create_time','title'=>"创建时间"],

        ];

        $result = $this->defaultIndex([

            "field_list" => $field_list,

            "where" => $where,

            "field" => array_column($field_list,"field")

        ]);

        if(!$request->isAjax()) return $result;

        if (!empty($result['data'])) {

            foreach ($result['data'] as $k => &$v) {

                $v['status'] = $v['status'] == Obj::STATUS_NORMAL ? "正常" : "禁用";

                $v['create_time'] = date("Y-m-d H:i:s",$v['create_time']);

            }
        }

        $this->success([

            'param' => $result

        ]);

    }



    private function getFieldList() {

        $field_list = [

            ['form_type'=>"text","name"=>"name",'title'=>"账号",'value'=>""],
            ['form_type'=>"password","name"=>"password",'title'=>"密码",'value'=>""],
            ['form_type'=>"text","name"=>"company_name",'title'=>"公司名称",'value'=>""],
            ['form_type'=>"text","name"=>"real_name",'title'=>"姓名",'value'=>""],
            ['form_type'=>"text","name"=>"mobile",'title'=>"手机号",'value'=>""],

            ['form_type'=>"text","name"=>"data_scale",'title'=>"数据比例",'value'=>""],
            
            ['form_type'=>"radio","name"=>"status",'title'=>"状态",'value'=>"",'data'=>[

                ['title'=>"正常",'value'=>Obj::STATUS_NORMAL],
                ['title'=>"禁用",'value'=>Obj::STATUS_DISABLED],

            ]],

        ];

        return $field_list;

    }


    private function getPostData(Request $request) {

        $field_list = array_column($this->getFieldList(),"name");

        $data = $request->only($field_list,"post");

        $id = $request->get('id/d',0);

        $validate = new Validate([

            "name" => "require",

            "password" => "require",

            "company_name" => "require",

            "real_name" => "require",

            "mobile" => "require|mobile",

        ],[

            "name.require" => "请输入账号",

            "password.require" => "请输入密码",

            "company_name.require" => "请输入公司名称",

            "real_name.require" => "请输入姓名",

            "mobile.require" => "请输入手机号",

            "mobile.mobile" => "手机号格式有误",

        ]);

        if($id > 0) $validate->remove(['password']);

        if(!$validate->check($data)) throw new \Exception($validate->getError());

        $is_exists = Obj::where([

            ['name','eq',$data['name']],

            ['is_del','eq',Obj::NO_DEL],

            ['id','neq',$id]

        ])->find();

        if(!empty($is_exists)) throw new \Exception("该账号已存在");

        $is_exists = Obj::where([

            ['mobile','eq',$data['mobile']],

            ['is_del','eq',Obj::NO_DEL],

            ['id','neq',$id]

        ])->find();

        if(!empty($is_exists)) throw new \Exception("该手机号已存在");

        if(!empty($data['password'])) {

            $data['password'] = md5(md5($data['password'])."login");

        }else{

            unset($data['password']);

        }

        return $data;

    }



    public function add(Request $request) {

        if ($request->isPost()) {

            try{

                $data = $this->getPostData($request);

                $data['create_time'] = time();

                Obj::insertGetId($data);

            }catch (\Exception $e) {

                $this->error($e->getMessage());

            }

            $this->success('添加成功');

        } else {

            $field_list = $this->getFieldList();

            return view('form', [

                'field_list' => $field_list,

                'title' => "添加{$this->now_keyword}",

            ]);
        }

    }


    public function publish(Request $request) {

        $id = $request->get('id/d',0);

        if($id < 1) $this->error('参数有误');

        if($request->isPost()) {

            try{

                $data = $this->getPostData($request);

                Obj::where('id',$id)->update($data);

            }catch (\Exception $e) {

                $this->error($e->getMessage());

            }

            $this->success("修改成功");

        }else{

            $field_list = $this->getFieldList();

            $fields = array_column($field_list,"name");

            $info = Obj::field($fields)->where('id',$id)->find();

            unset($info['password']);

            $render_array = [

                'field_list' => $field_list,

                'title' => "修改{$this->now_keyword}",

                'info' => $info

            ];

            return view('form', $render_array);

        }

    }


    public function del(Request $request) {

        parent::del($request);

    }


}