<?php

namespace app\admin\controller;

use app\common\model\ExtensionPlan as Obj;
use app\common\model\Channel as ChannelObj;
use app\common\model\Region;
use think\Request;
use think\Validate;

/**
 * 推广计划管理
 * Class ExtensionPlan
 * @package app\admin\controller
 */
class ExtensionPlan extends Base
{

    public $obj, $now_keyword = "推广计划";

    public function initialize()
    {
        parent::initialize(); // TODO: Change the autogenerated stub

        $this->obj = new Obj();

        $this->is_search = false;

    }


    public function index(Request $request) {

        $where = [

            ['a.is_del','eq',Obj::NO_DEL],

        ];

        $search_where = $request->get('where/s',"");

        if(!empty($search_where)) $where = array_merge($where,json2array($search_where));

        $result = $this->defaultIndex([

            "field_list" => [

                ['field'=>'id','title'=>"#"],
                ['field'=>'region_name','title'=>"区域"],
                ['field'=>'channel','title'=>"渠道"],
                ['field'=>'extension_time','title'=>"推广时间"],
                ['field'=>'status','title'=>"状态"],
                ['field'=>'create_time','title'=>"创建时间"],

            ],

            "where" => $where,


        ]);

        if(!$request->isAjax()) return $result;

        if (!empty($result['data'])) {

            foreach ($result['data'] as $k => &$v) {

                $v['region_name'] =

                $channelList = ChannelObj::where([

                    ['id','in',$v['channel_id']],

                    ['is_del','eq',ChannelObj::NO_DEL]

                ])->column("real_name");

                $regionNameList = Region::where([

                    ['region_id','in',$v['region_id']]

                ])->column("region_name");

                $channelArray = $region_name = [];

                foreach ($channelList as $vv) {

                    array_push($channelArray,"<span class='layui-btn layui-btn-xs'>{$vv}</span>");

                }

                foreach ($regionNameList as $vv) {

                    array_push($region_name,"<span class='layui-btn layui-btn-xs'>{$vv}</span>");

                }

                $v['channel'] = implode('&nbsp;',$channelArray);

                $v['region_name'] = implode('&nbsp;',$region_name);

                $v['extension_time'] = date("H",strtotime(date("Y-m-d 00:00:00")) + $v['start_time'])."时 - ".date("H",strtotime(date("Y-m-d 00:00:00")) + $v['end_time'])."时";

                $v['status'] = $v['status'] == 1 ? "禁用":"正常";

                $v['create_time'] = date("Y-m-d H:i:s",$v['create_time']);

            }
        }

        $this->success([

            'param' => $result

        ]);

    }



    private function getFieldList($id = 0) {

        $channel_id = $region_id = "";

        $info = Obj::where('id',$id)->find();

        if(!empty($info)) {

            $channel_id = $info['channel_id'];

            $region_id = $info['region_id'];

        }

        $regionList = Region::where([

            ['level','in',[2]]

        ])->field("region_id as value,merger_name as name")->select();

        $regionList = $regionList->isEmpty() ? [] : $regionList->toArray();

        foreach ($regionList as $k=>$v) {

            $regionList[$k]['name'] = str_replace("中国,","",$v['name']);

        }

        $channel_list = ChannelObj::where([

            ['is_del','eq',ChannelObj::NO_DEL]

        ])->field("id,real_name as name")->select();

        $channel_list = $channel_list->isEmpty() ? [] : $channel_list->toArray();

        $timeList = [];

        for ($i=0;$i<=24;$i++) {

            array_push($timeList,[

                "id" => $i,

                "name" => $i." 时"

            ]);

        }

        $redirect_type = [

            ['id' => Obj::EXTENSION_PLAN_REDIRECT_TYPE_OF_LOGIN,'name'=>"注册页"],
            ['id' => Obj::EXTENSION_PLAN_REDIRECT_TYPE_OF_PRODUCT,'name'=>"产品首页"],

        ];

        $field_list = [

            ['form_type'=>"select_all","name"=>"region_id",'title'=>"区域",'value'=>$region_id,'data' => $regionList],
            ['form_type'=>"select","name"=>"channel_id",'title'=>"渠道商",'value'=>$channel_id,'data'=>$channel_list],
            ['form_type'=>"select","name"=>"start_time",'title'=>"推广开始时间",'value'=>"-1",'data'=>$timeList],
            ['form_type'=>"select","name"=>"end_time",'title'=>"推广结束时间",'value'=>"-1",'data'=>$timeList],
            ['form_type'=>"select","name"=>"redirect_type",'title'=>"审批未通过跳转至",'value'=>"-1",'data'=>$redirect_type],
            ['form_type'=>"radio","name"=>"status",'title'=>"状态",'value'=>"0",'data'=>[

                ['title'=>"正常",'value'=>"0"],
                ['title'=>"禁用",'value'=>"1"],

            ]],

        ];

        return $field_list;

    }


    private function getPostData(Request $request) {

        $id = $request->get('id/d',0);

        $field_list = array_column($this->getFieldList(),"name");

        $data = $request->only($field_list,"post");

        $validate = new Validate([

            "region_id" => "require",

            "channel_id" => "require",

            "start_time" => "require",

            "end_time" => "require",

        ],[

            "region_id.require" => "请选择区域",

            "channel_id.require" => "请选择渠道商",

            "start_time.require" => "请选择推广开始时间",

            "end_time.require" => "请选择推广结束时间",

        ]);

        if(!$validate->check($data)) throw new \Exception($validate->getError());

        if($data['end_time'] <= $data['start_time']) throw new \Exception("推广结束时间必须晚于开始时间");

        $regionList = Region::where([

            ['region_id','in',$data['region_id']]

        ])->field("region_id,region_name")->select();

        $time_slot = [];

        for ($i=intval($data['start_time']);$i<$data['end_time'];$i++) {

            array_push($time_slot,$i);

        }

        $extensionPlanList = Obj::where([

            ['is_del','eq',Obj::NO_DEL],

            ['channel_id','eq',$data['channel_id']],

            ['id','neq',$id]

        ])->select();

        $extensionPlanList = $extensionPlanList->isEmpty() ? [] : $extensionPlanList->toArray();

        foreach ($extensionPlanList as $v) {

            $region_id = empty($v['region_id']) ? [] : explode(',',$v['region_id']);

            $start_time = empty($v['start_time']) ? 0 : $v['start_time'] / 3600;

            $end_time = empty($v['end_time']) ? 0 : $v['end_time'] / 3600;

            $start_end_slot = [];

            for ($i=intval($start_time);$i<$end_time;$i++) {

                array_push($start_end_slot,$i);

            }

            foreach ($regionList as $vv) {

                if(in_array($vv['region_id'],$region_id) && !empty(array_intersect($start_end_slot,$time_slot))) {

                    throw new \Exception("此计划中".$vv['region_name']."与计划ID：【{$v['id']}】中的{$vv['region_name']}推广时间冲突");

                    break;

                }

            }

        }

        $data['start_time'] = $data['start_time'] * 60 * 60;

        $data['end_time'] = $data['end_time'] * 60 * 60;

        return $data;

    }



    public function add(Request $request) {

        if ($request->isPost()) {

            try{

                $data = $this->getPostData($request);

                $data['create_time'] = time();

                Obj::insertGetId($data);

            }catch (\Exception $e) {

                $this->error($e->getMessage());

            }

            $this->success('添加成功');

        } else {

            $field_list = $this->getFieldList();

            return view('form', [

                'field_list' => $field_list,

                'title' => "添加{$this->now_keyword}",

            ]);
        }

    }


    public function publish(Request $request) {

        $id = $request->get('id/d',0);

        if($id < 1) $this->error('参数有误');

        if($request->isPost()) {

            try{

                $data = $this->getPostData($request);

                Obj::where('id',$id)->update($data);

            }catch (\Exception $e) {

                $this->error($e->getMessage());

            }

            $this->success("修改成功");

        }else{

            $field_list = $this->getFieldList($id);

            $fields = array_column($field_list,"name");

            $info = Obj::field($fields)->where('id',$id)->find();

            $info['start_time'] = empty($info['start_time']) ? "0" : $info['start_time'] / 3600;

            $info['end_time'] = empty($info['end_time']) ? "0" : $info['end_time'] / 3600;

            $render_array = [

                'field_list' => $field_list,

                'title' => "修改{$this->now_keyword}",

                'info' => $info

            ];

            return view('form', $render_array);

        }

    }


    public function del(Request $request) {

        parent::del($request);

    }


}