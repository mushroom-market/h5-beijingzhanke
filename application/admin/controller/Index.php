<?php

namespace app\admin\controller;

/**
 * 后台
 * Class Index
 * @package app\admin\controller
 */
class Index extends Base
{
    /**
     * 后台首页
     */
    public function index()
    {
        return $this->fetch();
    }



}
