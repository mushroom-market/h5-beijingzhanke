<?php

namespace app\admin\controller;

use app\common\model\Admin;
use think\captcha\Captcha;
use think\facade\Config;
use think\Controller;
use think\facade\Cookie;
use think\Request;

/**
 * 登录
 * Class Login
 * @package app\admin\controller
 */
class Login extends Base
{

    //后台登录
    public function login(Request $request)
    {


        $is_open_qrcode = !Config::get($this->module.'.is_open_qrcode') ? 0 : 1;

        if ($request->isPost()) {

            try{

                $qrcode = $request->post('qrcode/s','');

                $adminname = $request->post('adminname/s','');

                $password = $request->post('password/s','');

                if ($adminname == '' || $password == '') throw new \Exception("参数不能为空");

                /*if($is_open_qrcode == 1) {

                    if(empty($qrcode)) throw new \Exception("请输入验证码");

                    if(!captcha_check($qrcode)) throw new \Exception("验证码有误");

                }*/

                $adminObj = new Admin();

                $admininfo = $adminObj->where([

                    ['name','eq',$adminname],

                    ['status','eq',Admin::STATUS_NORMAL],

                    ['is_del','eq',Admin::NO_DEL],

                ])->find();

                if(empty($admininfo)) throw new \Exception("用户名不存在");

                $admininfo = $admininfo->toArray();

                if ($admininfo['password'] !== md5(md5($password) . 'login')) throw new \Exception("密码有误");

                unset($admininfo['password']);

                Cookie::set($this->cookie_name,$admininfo);

            }catch (\Exception $e) {

                $this->error($e->getMessage());

            }

            $this->success("登录成功");

        } else {

            if(Cookie::has($this->cookie_name)) $this->redirect(url('index/index'));

            return view('login',[

                'is_open_qrcode'=>$is_open_qrcode,

            ]);

        }
    }



    /**
     * 退出登录
     */
    public function logout()
    {
        if (Cookie::has($this->cookie_name)) {

            Cookie::delete($this->cookie_name);

        }

        $this->redirect(url('login/login'));

    }



    public function captchaImg() {

        $is_open_qrcode = !Config::get($this->module.'.is_open_qrcode') ? 0 : 1;

        if($is_open_qrcode == 0) return "";

        $obj = new Captcha();

        $obj->codeSet = "0123456789";

        $obj->useCurve = false;

        $obj->useNoise = false;

        $obj->length = 4;

        return $obj->entry();

    }



}
