<?php

namespace app\admin\controller;

use app\common\model\ChannelInviteCode;
use app\common\model\OfflineShopMoneyLog;
use app\common\model\OfflineShopUser;
use app\common\model\PageStatistics;
use app\common\model\ProductLinkVisitLog;
use app\common\model\ProductStatistics;
use app\common\model\StickPlan;
use app\common\model\UserA as UserObj;
use app\common\model\UserB as UserBObj;
use app\common\model\Product as ProductObj;
use app\common\model\Channel as ChannelObj;
use app\common\model\OfflineShop as OfflineShopObj;
use think\Request;

class Statistics extends Base {

    public function platform(Request $request)
    {


        if ($this->request->isAjax()) {

            $start_time = strtotime(date("Y-m-d 00:00:00"));

            $end_time = strtotime("+1 days",$start_time);

            $input_progress = "";

            $search_where = $request->get("where/s",'');

            $search_where = empty($search_where) ? [] : json2array($search_where);

            foreach ($search_where as $k=>$v) {

                if($v[0] == "date") {

                    $start_time = strtotime(date("Y-m-d 00:00:00",$v[2][0]));

                    $end_time = strtotime(date("Y-m-d 00:00:00",$v[2][1]));

                }

                if($v[0] == "input_progress") $input_progress = $v[2];

            }

            $result = [];

            $pageStatisticsList = PageStatistics::where([

                ['page','eq',PageStatistics::PAGE_OF_REGISTER],

                ['create_time','between',[$start_time,$end_time]]

            ])->select();

            $pageStatisticsList = $pageStatisticsList->isEmpty() ? [] : $pageStatisticsList->toArray();

            if($input_progress == "a") {

                $registerAList = UserObj::where([

                    ['create_time','between',[$start_time,$end_time]],

                    ['progress','in',[

                        UserObj::PROGRESS_OF_REGISTER,

                        UserObj::PROGRESS_OF_PERFECT,

                        UserObj::PROGRESS_OF_INPUT

                    ]]

                ])->column("create_time");

                $registerBList = [];

                $inputAList = UserObj::where([

                    ['input_time','between',[$start_time,$end_time]],

                    ['progress','eq',UserObj::PROGRESS_OF_INPUT]

                ])->column("input_time");

                $inputBList = [];

            }elseif($input_progress == "b") {

                $registerAList = [];

                $registerBList = UserBObj::where([

                    ['create_time','between',[$start_time,$end_time]],

                ])->column("create_time");

                $inputAList = [];

                $inputBList = UserBObj::where([

                    ['submit_time','between',[$start_time,$end_time]],

                    ['progress','eq',UserBObj::PROGRESS_OF_SUBMIT]

                ])->column("submit_time");

            }else{

                $registerAList = UserObj::where([

                    ['create_time','between',[$start_time,$end_time]],

                    ['progress','in',[

                        UserObj::PROGRESS_OF_REGISTER,

                        UserObj::PROGRESS_OF_PERFECT,

                        UserObj::PROGRESS_OF_INPUT

                    ]]

                ])->column("create_time");

                $registerBList = UserBObj::where([

                    ['create_time','between',[$start_time,$end_time]],

                ])->column("create_time");

                $inputAList = UserObj::where([

                    ['input_time','between',[$start_time,$end_time]],

                    ['progress','eq',UserObj::PROGRESS_OF_INPUT]

                ])->column("input_time");

                $inputBList = UserBObj::where([

                    ['submit_time','between',[$start_time,$end_time]],

                    ['progress','eq',UserBObj::PROGRESS_OF_SUBMIT]

                ])->column("submit_time");

            }



            $registerList = array_merge($registerAList,$registerBList);

            $inputList = array_merge($inputAList,$inputBList);

            $day = ($end_time - $start_time) > 0 ? ceil(($end_time - $start_time) / 86400) : 0;

            for ($i=1;$i<=$day;$i++) {

                $date = date("Y-m-d",strtotime("+".($i-1)." days",$start_time));

                $uv_array = [];

                $pv = $register_num = $input_num = 0;

                foreach ($pageStatisticsList as $v) {

                    $time_ = date("Y-m-d",$v['create_time']);

                    if($time_ != $date) continue;

                    if(!in_array($v['ip'],$uv_array)) array_push($uv_array,$v['ip']);

                    $pv ++;

                }

                foreach ($registerList as $v){

                    $time_ = date("Y-m-d",$v);

                    if($time_ != $date) continue;

                    $register_num ++;

                }


                foreach ($inputList as $v){

                    $time_ = date("Y-m-d",$v);

                    if($time_ != $date) continue;

                    $input_num ++;

                }

                array_push($result,[

                    "uv" => count($uv_array),

                    "pv" => $pv,

                    "register_num" => $register_num,

                    "input_num" => $input_num,

                    "date" => $date

                ]);

            }

            $this->success([

                'param' => [

                    "count" => 0,

                    "data" => $result

                ]

            ]);

        } else {

            $input_progress = [

                ['id'=>"a",'name'=>"A流程"],
                ['id'=>"b",'name'=>"B流程"],

            ];

            //搜索列表
            $search_array = [

                ['field'=>"date","form_type"=>"time","title"=>"日期","search_type"=>"between"],

                ['field'=>"input_progress","form_type"=>"select","title"=>"进件流程","search_type"=>"eq",'data'=>$input_progress],

            ];

            //页面标题
            $sub_title = "平台统计";

            //字段列表
            $fieldList = [

                ['field'=>'uv','title'=>"UV"],
                ['field'=>'pv','title'=>"PV"],
                ['field'=>'register_num','title'=>"注册量"],
                ['field'=>'input_num','title'=>"进件量"],
                ['field'=>'date','title'=>"时间"],

            ];

            //新增工具组
            $sub_toolbar = [];

            foreach ($fieldList as $k => $v) {

                if (isset($v['align'])) continue;

                $fieldList[$k]['align'] = "center";

            }

            $toolbar = [];

            if (!empty($sub_toolbar)) $toolbar = array_merge($toolbar, $sub_toolbar);

            $render_array = [

                'cols' => [$fieldList],

                'toolbar' => implode('', $toolbar),

                'sub_title' => $sub_title,

                'is_page' => false,

                'is_search' => $this->is_search,

                'search_array' => $search_array,

            ];

            return view("index", $render_array);

        }


    }




    public function loan(Request $request) {

        if ($this->request->isAjax()) {

            $start_time = strtotime(date("Y-m-d 00:00:00"));

            $end_time = strtotime("+1 days",$start_time);

            $search_where = $request->get("where/s",'');

            $search_where = empty($search_where) ? [] : json2array($search_where);

            foreach ($search_where as $k=>$v) {

                if($v[0] == "date") {

                    $start_time = strtotime(date("Y-m-d 00:00:00",$v[2][0]));

                    $end_time = strtotime(date("Y-m-d 00:00:00",$v[2][1]));

                }

            }

            $result = [];

            $pageStatisticsList = PageStatistics::where([

                ['create_time','between',[$start_time,$end_time]]

            ])->select();

            $pageStatisticsList = $pageStatisticsList->isEmpty() ? [] : $pageStatisticsList->toArray();

            $day = ($end_time - $start_time) > 0 ? ceil(($end_time - $start_time) / 86400) : 0;

            for ($i=1;$i<=$day;$i++) {

                $date = date("Y-m-d",strtotime("+".($i-1)." days",$start_time));

                $uv_array = [];

                $pv = $product_visit_num = $link_visit_num = 0;

                foreach ($pageStatisticsList as $v) {

                    $time_ = date("Y-m-d",$v['create_time']);

                    if($time_ != $date) continue;

                    switch ($v['page']) {

                        case PageStatistics::PAGE_OF_INDEX:

                            if (!in_array($v['ip'], $uv_array)) array_push($uv_array, $v['ip']);

                            $pv++;

                            break;

                        case PageStatistics::PAGE_OF_PRODUCT_DETAIL:

                            $product_visit_num ++;

                            break;

                        case PageStatistics::PAGE_OF_PRODUCT_LINK:

                            $link_visit_num ++;

                            break;

                    }


                }

                array_push($result,[

                    "date" => $date,

                    "uv" => count($uv_array),

                    "pv" => $pv,

                    "product_visit_num" => $product_visit_num,

                    "link_visit_num" => $link_visit_num,

                ]);

            }

            $this->success([

                'param' => [

                    "count" => 0,

                    "data" => $result

                ]

            ]);

        } else {

            //搜索列表
            $search_array = [

                ['field'=>"date","form_type"=>"time","title"=>"日期","search_type"=>"between"]

            ];

            //页面标题
            $sub_title = "贷超统计";

            //字段列表
            $fieldList = [

                ['field'=>'uv','title'=>"UV"],
                ['field'=>'pv','title'=>"PV"],
                ['field'=>'product_visit_num','title'=>"产品访问量"],
                ['field'=>'link_visit_num','title'=>"外链访问量"],
                ['field'=>'date','title'=>"时间"],

            ];

            //新增工具组
            $sub_toolbar = [];

            foreach ($fieldList as $k => $v) {

                if (isset($v['align'])) continue;

                $fieldList[$k]['align'] = "center";

            }

            $toolbar = [];

            if (!empty($sub_toolbar)) $toolbar = array_merge($toolbar, $sub_toolbar);

            $render_array = [

                'cols' => [$fieldList],

                'toolbar' => implode('', $toolbar),

                'sub_title' => $sub_title,

                'is_page' => false,

                'is_search' => $this->is_search,

                'search_array' => $search_array,

            ];

            return view("index", $render_array);

        }

    }


    public function product(Request $request) {

        if ($this->request->isAjax()) {

            $start_time = strtotime(date("Y-m-d 00:00:00"));

            $end_time = strtotime("+1 days",$start_time);

            $product_id = "";

            $search_where = $request->get("where/s",'');

            $search_where = empty($search_where) ? [] : json2array($search_where);

            foreach ($search_where as $k=>$v) {

                if($v[0] == "date") {

                    $start_time = strtotime(date("Y-m-d 00:00:00",$v[2][0]));

                    $end_time = strtotime(date("Y-m-d 00:00:00",$v[2][1]));

                }

                if($v[0] == "product_id") {

                    $product_id = $v[2];

                }

            }

            $day = ($end_time - $start_time) > 0 ? ceil(($end_time - $start_time) / 86400) : 0;

            $result = [];

            $where = [

                ['product_statistics.create_time','between',[$start_time,$end_time]],

                ['product.is_del','eq',ProductObj::NO_DEL],

            ];

            if(!empty($product_id)) array_push($where,['product.id','IN',$product_id]);

            $productStatisticsList = ProductStatistics::alias("product_statistics")->join([

                ['product product','product_statistics.product_id = product.id','left']

            ])->where($where)->order("product_statistics.id ASC")->field("product_statistics.*,product.name as product_name,product.client_name as product_client_name,product.institution_name as product_institution_name")->select();

            $productStatisticsList = $productStatisticsList->isEmpty() ? [] : $productStatisticsList->toArray();

            for ($i=1;$i<=$day;$i++) {

                $date = date("Y-m-d",strtotime("+".($i-1)." days",$start_time));

                $start_ = strtotime($date." 00:00:00");

                $end_ = strtotime($date." 23:59:59");

                $all_uv = $all_income = 0;

                $institution_name = $client_name = $product_name = $unit_price = $settlement_type = $day_uv_max_num = $uv_ = $result_data = $income = $average_uv_price = $caozuo = [];

                foreach ($productStatisticsList as $v) {

                    if($v['create_time'] >= $start_ && $v['create_time'] <= $end_) {

                        $settlement_type_ = "";

                        $income_ = $v['income'];

                        $caozuo_ = "";

                        $average_uv_price_ = empty($v['income']) || empty($v['uv']) ? 0 : $v['income']/$v['uv'];

                        switch ($v['settlement_type']) {

                            case 1:

                                $settlement_type_ = "UV-UV";

                                array_push($result_data,"<p class='many_p'>".(empty($v['uv']) ? 0 : $v['uv'])."</p>");
//                                array_push($result_data,"<p class='many_p'>".(empty($v['income'] / $v['unit_price']) ? 0 : ceil($v['income'] / $v['unit_price']))."</p>");

                                $income_ = $v['unit_price'] * $v['uv'];

                                $average_uv_price_ = empty($income_) || empty($v['uv']) ? 0 : round($income_ / $v['uv'],2);

                                break;

                            case 2:

                                $settlement_type_ = "CPA-注册数量";

                                array_push($result_data,"<p class='many_p'>".(empty($v['income'] / $v['unit_price']) ? 0 : ceil($v['income'] / $v['unit_price']))."</p>");

                                $caozuo_ = "<button class='layui-btn layui-btn-sm inputPrice' data-id='{$v['id']}' data-settlement_type='{$v['settlement_type']}'>输入注册量</button>";

                                break;

                            case 3:

                                $settlement_type_ = "CPS-放款金额";

                                array_push($result_data,"<p class='many_p'>".(empty($v['loans_price']) ? 0 : $v['loans_price'])."</p>");

                                $caozuo_ = "<button class='layui-btn layui-btn-sm inputPrice' data-id='{$v['id']}' data-settlement_type='{$v['settlement_type']}'>输入金额</button>";

                                break;

                            default:

                                array_push($result_data,"<p class='many_p'></p>");

                                break;

                        }

                        $all_income += $income_;

                        $all_uv += $v['uv'];

                        array_push($institution_name,"<p class='many_p'>{$v['product_institution_name']}</p>");

                        array_push($client_name,"<p class='many_p'>{$v['product_client_name']}</p>");

                        array_push($product_name,"<p class='many_p'>{$v['product_name']}</p>");

                        array_push($unit_price,"<p class='many_p'>{$v['unit_price']}</p>");

                        array_push($settlement_type,"<p class='many_p'>{$settlement_type_}</p>");

                        array_push($day_uv_max_num,"<p class='many_p'>{$v['day_uv_max_num']}</p>");

                        array_push($uv_,"<p class='many_p'>{$v['uv']}</p>");

                        array_push($income,"<p class='many_p'>".round($income_,2)."</p>");

                        array_push($average_uv_price,"<p class='many_p'>".round($average_uv_price_,2)."</p>");

                        array_push($caozuo,"<p class='many_p'>{$caozuo_}</p>");

                    }

                }

                $all_average_uv_price = empty($all_income) || empty($all_uv) ? 0 : round($all_income / $all_uv,2);

                if(count($institution_name) <= 0) continue;

                array_push($result,[

                    "all_uv" => $all_uv,

                    "all_income" => $all_income,

                    "all_average_uv_price" => $all_average_uv_price,

                    "institution_name" => implode('',$institution_name),

                    "client_name" => implode('',$client_name),

                    "product_name" => implode('',$product_name),

                    "unit_price" => implode('',$unit_price),

                    "settlement_type" => implode('',$settlement_type),

                    "day_uv_max_num" => implode('',$day_uv_max_num),

                    "uv" => implode('',$uv_),

                    "result" => implode('',$result_data),

                    "income" => implode('',$income),

                    "average_uv_price" => implode('',$average_uv_price),

                    "date" => $date,

                    "caozuo" => implode('',$caozuo)

                ]);

            }

            $this->success([

                'param' => [

                    "count" => 0,

                    "data" => $result

                ]

            ]);

        } else {

            $product_list = ProductObj::where([

                ['is_del','eq',ProductObj::NO_DEL]

            ])->field("id as value,name")->select();

            $product_list = $product_list->isEmpty() ? [] : $product_list->toArray();

            //搜索列表
            $search_array = [

                ['field'=>"date","form_type"=>"time","title"=>"日期","search_type"=>"between"],
                ['field'=>"product_id","form_type"=>"select_all","title"=>"产品","search_type"=>"eq",'data'=>$product_list],

            ];

            //页面标题
            $sub_title = "产品统计";

            //字段列表
            $fieldList = [

                ['field'=>'date','title'=>"时间"],
                ['field'=>'all_uv','title'=>"总UV数"],
                ['field'=>'all_income','title'=>"总收入"],
                ['field'=>'all_average_uv_price','title'=>"总平均UV单价"],
                ['field'=>'institution_name','title'=>"机构名称",'width'=>"10%"],
                ['field'=>'client_name','title'=>"联系人"],
                ['field'=>'product_name','title'=>"产品名称"],
                ['field'=>'unit_price','title'=>"单价"],
                ['field'=>'settlement_type','title'=>"结算方式"],
                ['field'=>'day_uv_max_num','title'=>"UV上限"],
                ['field'=>'uv','title'=>"实际UV"],
                ['field'=>'result','title'=>"结果"],
                ['field'=>'income','title'=>"收入"],
                ['field'=>'average_uv_price','title'=>"UV平均单价"],
                ['field'=>'caozuo','title'=>"操作",'fixed'=>"right"],

            ];

            //新增工具组
            $sub_toolbar = [];

            foreach ($fieldList as $k => $v) {

                if (isset($v['align'])) continue;

                $fieldList[$k]['align'] = "center";

            }

            $toolbar = [];

            if (!empty($sub_toolbar)) $toolbar = array_merge($toolbar, $sub_toolbar);

            $render_array = [

                'cols' => [$fieldList],

                'toolbar' => implode('', $toolbar),

                'sub_title' => $sub_title,

                'is_page' => false,

                'is_search' => $this->is_search,

                'search_array' => $search_array,

            ];

            return view("index", $render_array);

        }

    }


    public function settingProductPrice(Request $request) {

        try{

            $id = $request->post('id/d',0);

            $value = $request->post('price/s',"");

            if(empty($id) || empty($value)) throw new \Exception("参数有误");

            $productStatisticsInfo = ProductStatistics::where([

                ['id','eq',$id],

            ])->find();

            if(empty($productStatisticsInfo)) throw new \Exception("数据不存在");

            $update_data = [];

            switch ($productStatisticsInfo['settlement_type']) {

                case 2:

                    $update_data = [

                        "income" => $value * $productStatisticsInfo['unit_price'],

                    ];

                    break;

                case 3:

                    $update_data = [

                        "loans_price" => $value,

                        "income" => $value * $productStatisticsInfo['unit_price']

                    ];

                    break;


            }

            if(empty($update_data)) throw new \Exception("参数有误");

            ProductStatistics::where([

                ['id','eq',$id],

            ])->update($update_data);

        }catch (\Exception $e) {

            $this->error($e->getMessage());

        }

        $this->success("设置成功");

    }



    public function channel(Request $request) {

        if ($request->isAjax()) {

            $start_time = strtotime(date("Y-m-d 00:00:00"));

            $end_time = strtotime("+1 days",$start_time);

            $channel_id = 0;

            $input_progress = "";

            $search_where = $request->get("where/s",'');

            $search_where = empty($search_where) ? [] : json2array($search_where);

            foreach ($search_where as $k=>$v) {

                if($v[0] == "date") {

                    $start_time = strtotime(date("Y-m-d 00:00:00",$v[2][0]));

                    $end_time = strtotime(date("Y-m-d 00:00:00",$v[2][1]));

                }

                if($v[0] == "channel_id") {

                    $channel_id = $v[2];

                }

                if($v[0] == "input_progress") $input_progress = $v[2];

            }

            $day = ($end_time - $start_time) > 0 ? ceil(($end_time - $start_time) / 86400) : 0;

            $where = [

                ['is_del','eq',ChannelObj::NO_DEL]

            ];

            if($channel_id > 0) array_push($where,['id','in',$channel_id]);

            $result = [];

            $channelList = ChannelObj::where($where)->field("id,name")->select();

            $channelList = $channelList->isEmpty() ? [] : $channelList->toArray();

            for ($i=1;$i<=$day;$i++) {

                $date = date("Y-m-d",strtotime("+".($i-1)." days",$start_time));

                $start_ = strtotime($date." 00:00:00");

                $end_ = strtotime($date." 23:59:59");

                foreach ($channelList as $v) {

                    $uv = PageStatistics::where([

                        ['channel_id','eq',$v['id']],

                        ['create_time','between',[$start_,$end_]]

                    ])->group("ip")->count("id");

                    $pv = PageStatistics::where([

                        ['channel_id','eq',$v['id']],

                        ['create_time','between',[$start_,$end_]]

                    ])->count("id");

                    if($input_progress == "a") {

                        $register_a_num = UserObj::where([

                            ['create_time', 'between', [$start_, $end_]],

                            ['channel_id', 'eq', $v['id']],

                            ['progress', 'in', [

                                UserObj::PROGRESS_OF_REGISTER,

                                UserObj::PROGRESS_OF_PERFECT,

                                UserObj::PROGRESS_OF_INPUT

                            ]]

                        ])->count('id');

                        $register_b_num = 0;

                        $input_a_num = UserObj::where([

                            ['input_time','between',[$start_,$end_]],

                            ['channel_id','eq',$v['id']],

                            ['progress','eq',UserObj::PROGRESS_OF_INPUT]

                        ])->count('id');

                        $input_b_num = 0;

                    }elseif ($input_progress == "b") {

                        $register_a_num = 0;

                        $register_b_num = UserBObj::where([

                            ['create_time','between',[$start_,$end_]],

                            ['channel_id','eq',$v['id']],

                        ])->count('id');

                        $input_a_num = 0;

                        $input_b_num = UserBObj::where([

                            ['submit_time','between',[$start_,$end_]],

                            ['channel_id','eq',$v['id']],

                            ['progress','eq',UserBObj::PROGRESS_OF_SUBMIT]

                        ])->count('id');

                    }else{

                        $register_a_num = UserObj::where([

                            ['create_time','between',[$start_,$end_]],

                            ['channel_id','eq',$v['id']],

                            ['progress','in',[

                                UserObj::PROGRESS_OF_REGISTER,

                                UserObj::PROGRESS_OF_PERFECT,

                                UserObj::PROGRESS_OF_INPUT

                            ]]

                        ])->count('id');

                        $register_b_num = UserBObj::where([

                            ['create_time','between',[$start_,$end_]],

                            ['channel_id','eq',$v['id']],

                        ])->count('id');

                        $input_a_num = UserObj::where([

                            ['input_time','between',[$start_,$end_]],

                            ['channel_id','eq',$v['id']],

                            ['progress','eq',UserObj::PROGRESS_OF_INPUT]

                        ])->count('id');

                        $input_b_num = UserBObj::where([

                            ['submit_time','between',[$start_,$end_]],

                            ['channel_id','eq',$v['id']],

                            ['progress','eq',UserBObj::PROGRESS_OF_SUBMIT]

                        ])->count('id');

                    }

                    array_push($result,[

                        "channel_name" => $v['name'],

                        "uv" => $uv,

                        "pv" => $pv,

                        "register_num" => $register_a_num + $register_b_num,

                        "input_num" => $input_a_num + $input_b_num,

                        "date" => $date

                    ]);

                }

            }

            $this->success([

                'param' => [

                    "count" => 0,

                    "data" => $result

                ]

            ]);

        } else {

            $channel_list = ChannelObj::where([

                ['is_del','eq',ChannelObj::NO_DEL]

            ])->field("id as value,name")->select();

            $channel_list = $channel_list->isEmpty() ? [] : $channel_list->toArray();

            $input_progress = [

                ['id'=>"a",'name'=>"A流程"],
                ['id'=>"b",'name'=>"B流程"],

            ];


            //搜索列表
            $search_array = [

                ['field'=>"date","form_type"=>"time","title"=>"日期","search_type"=>"between"],
                ['field'=>"channel_id","form_type"=>"select_all","title"=>"渠道商","search_type"=>"eq",'data'=>$channel_list],
                ['field'=>"input_progress","form_type"=>"select","title"=>"进件流程","search_type"=>"eq",'data'=>$input_progress],

            ];

            //页面标题
            $sub_title = "渠道统计";

            //字段列表
            $fieldList = [

                ['field'=>'channel_name','title'=>"渠道名称"],
                ['field'=>'uv','title'=>"UV"],
                ['field'=>'pv','title'=>"PV"],
                ['field'=>'register_num','title'=>"注册"],
                ['field'=>'input_num','title'=>"进件"],
                ['field'=>'date','title'=>"时间"],

            ];

            //新增工具组
            $sub_toolbar = [];

            foreach ($fieldList as $k => $v) {

                if (isset($v['align'])) continue;

                $fieldList[$k]['align'] = "center";

            }

            $toolbar = [];

            if (!empty($sub_toolbar)) $toolbar = array_merge($toolbar, $sub_toolbar);

            $render_array = [

                'cols' => [$fieldList],

                'toolbar' => implode('', $toolbar),

                'sub_title' => $sub_title,

                'is_page' => false,

                'is_search' => $this->is_search,

                'search_array' => $search_array,

            ];

            return view("index", $render_array);

        }

    }


    public function offlineShop(Request $request) {

        if ($request->isAjax()) {

            $start_time = strtotime(date("Y-m-d 00:00:00"));

            $end_time = strtotime("+1 days",$start_time);

            $offline_shop_id = 0;

            $search_where = $request->get("where/s",'');

            $search_where = empty($search_where) ? [] : json2array($search_where);

            foreach ($search_where as $k=>$v) {

                if($v[0] == "date") {

                    $start_time = strtotime(date("Y-m-d 00:00:00",$v[2][0]));

                    $end_time = strtotime(date("Y-m-d 00:00:00",$v[2][1]));

                }

                if($v[0] == "offline_shop_id") {

                    $offline_shop_id = $v[2];

                }

            }

            $day = ($end_time - $start_time) > 0 ? ceil(($end_time - $start_time) / 86400) : 0;

            $where = [

                ['plan.is_del','eq',StickPlan::NO_DEL]

            ];

            if($offline_shop_id > 0) array_push($where,["shop.id",'in',$offline_shop_id]);

            $result = [];

            $StickPlanList = StickPlan::alias("plan")->join([

                ['offline_shop shop','plan.offline_shop_id = shop.id','left']

            ])->where($where)->field("shop.name as shop_name,plan.region_name,plan.offline_shop_id,plan.input_type")->select();

            $StickPlanList = $StickPlanList->isEmpty() ? [] : $StickPlanList->toArray();

            for ($i=1;$i<=$day;$i++) {

                $date = date("Y-m-d",strtotime("+".($i-1)." days",$start_time));

                $start_ = strtotime($date." 00:00:00");

                $end_ = strtotime($date." 23:59:59");

                foreach ($StickPlanList as $v) {

                    $distribution_num = OfflineShopUser::alias("osu")->join([

                        ['user_a user','osu.user_id = user.id','left'],

                    ])->where([

                        ['osu.offline_shop_id','eq',$v['offline_shop_id']],

                        ['osu.create_time','between',[$start_,$end_]],

                        ['user.input_type','eq',$v['input_type']]

                    ])->count('osu.id');

                    $consume_price = OfflineShopUser::alias("osu")->join([

                        ['user_a user','osu.user_id = user.id','left'],

                    ])->where([

                        ['osu.offline_shop_id','eq',$v['offline_shop_id']],

                        ['osu.create_time','between',[$start_,$end_]],

                        ['user.input_type','eq',$v['input_type']]

                    ])->sum("osu.consume_price");

                    array_push($result,[

                        "offline_name" => $v['shop_name'],

                        "region_name" => $v['region_name'],

                        "input_type" => UserObj::getInputTypeText($v['input_type']),

                        "distribution_num" => $distribution_num,

                        "consume_price" => $consume_price,

                        "date" => $date

                    ]);

                }


            }

            $this->success([

                'param' => [

                    "count" => 0,

                    "data" => $result

                ]

            ]);

        } else {

            $offline_shop_list = OfflineShopObj::where([

                ['is_del','eq',OfflineShopObj::NO_DEL]

            ])->field("id as value,name")->select();

            $offline_shop_list = $offline_shop_list->isEmpty() ? [] : $offline_shop_list->toArray();

            //搜索列表
            $search_array = [

                ['field'=>"date","form_type"=>"time","title"=>"日期","search_type"=>"between"],
                ['field'=>"offline_shop_id","form_type"=>"select_all","title"=>"线下店","search_type"=>"eq",'data'=>$offline_shop_list],

            ];

            //页面标题
            $sub_title = "线下店统计";

            //字段列表
            $fieldList = [

                ['field'=>'offline_name','title'=>"线下店名称"],
                ['field'=>'region_name','title'=>"地区"],
                ['field'=>'input_type','title'=>"进件类型"],
                ['field'=>'distribution_num','title'=>"分配量"],
                ['field'=>'consume_price','title'=>"消费金额"],
                ['field'=>'date','title'=>"时间"],

            ];

            //新增工具组
            $sub_toolbar = [];

            foreach ($fieldList as $k => $v) {

                if (isset($v['align'])) continue;

                $fieldList[$k]['align'] = "center";

            }

            $toolbar = [];

            if (!empty($sub_toolbar)) $toolbar = array_merge($toolbar, $sub_toolbar);

            $render_array = [

                'cols' => [$fieldList],

                'toolbar' => implode('', $toolbar),

                'sub_title' => $sub_title,

                'is_page' => false,

                'is_search' => $this->is_search,

                'search_array' => $search_array,

            ];

            return view("index", $render_array);

        }

    }



}