<?php

namespace app\admin\controller;

use app\common\model\Module as ModuleObj;
use app\common\model\ModuleField;
use app\common\model\Region;
use app\common\model\RegionSetting;
use app\common\model\StickPlan;
use app\common\model\UserA as UserObj;
use app\common\model\OfflineShop as OfflineShopObj;
use app\common\model\StickPlan as Obj;
use think\Request;
use think\Validate;

/**
 * 置顶计划管理
 * Class Stick
 * @package app\admin\controller
 */
class Stick extends Base
{

    public $obj, $now_keyword = "置顶计划";

    public function initialize()
    {
        parent::initialize(); // TODO: Change the autogenerated stub

        $this->obj = new Obj();

    }


    public function index(Request $request) {

        $where = [

            ['a.is_del','eq',Obj::NO_DEL]

        ];

        $search_where = $request->get('where/s',"");

        if(!empty($search_where)) $where = array_merge($where,json2array($search_where));

        $regionList = Region::where([

            ['level','in',[2]]

        ])->field("region_id as id,merger_name as name")->select();

        $regionList = $regionList->isEmpty() ? [] : $regionList->toArray();

        foreach ($regionList as $k=>$v) {

            $regionList[$k]['name'] = str_replace("中国,","",$v['name']);

        }

        $result = $this->defaultIndex([

            "field_list" => [

                ['field'=>'region_name','title'=>"区域"],
                ['field'=>'offline_shop_name','title'=>"线下店"],
                ['field'=>'input_type','title'=>"进件类型"],
                ['field'=>'priceList','title'=>"价格"],
                ['field'=>'tags','title'=>"标签组",'width'=>"40%"],
                ['field'=>'ranking','title'=>"排名"],

            ],

            "order" => "a.bidding_price DESC,a.id DESC",

            "where" => $where,

            "join" => [

                ['offline_shop os','a.offline_shop_id = os.id','left']

            ],

            "field" => "a.*,os.name as offline_shop_name",

            "search_array" => [

                ['field'=>"a.region_id",'form_type'=>"select",'search_type'=>"eq",'title'=>"区域",'data'=>$regionList],
                ['field'=>"os.name",'form_type'=>"text",'search_type'=>"like",'title'=>"线下店"],
                ['field'=>"a.input_type",'form_type'=>"select",'search_type'=>"eq",'title'=>"进件类型",'data'=>UserObj::getInputTypeText("list","id",'name')],

            ]

        ]);

        if(!$request->isAjax()) return $result;

        if (!empty($result['data'])) {

            foreach ($result['data'] as $k => &$v) {

                $tags = [];

                $respon = StickPlan::parseModuleField($v['module_field_data']);

                foreach ($respon as $vv) {

                    array_push($tags,"<span style='margin:5px;' class='layui-btn layui-btn-xs'>{$vv['module_name']}:".implode('、',array_column($vv['module_field_list'],"name"))."</span>");

                }

                $v['tags'] = implode('',$tags);

                $v['input_type'] = UserObj::getInputTypeText($v['input_type']);

                $priceList = [

                    "<p>城市底价：{$v['region_price']}</p>",
                    "<p>推荐价格：{$v['recommend_price']}</p>",
                    "<p>置顶价格：{$v['stick_price']}</p>",
                    "<p>出价价格：{$v['bidding_price']}</p>",

                ];

                $v['priceList'] = implode('',$priceList);

                $v['ranking'] = empty($v['ranking']) ? "" : $v['ranking'];

                $v['create_time'] = date("Y-m-d H:i:s",$v['create_time']);

            }
        }

        $this->success([

            'param' => $result

        ]);

    }



    private function getFieldList($id = 0) {

        $regionList = RegionSetting::where([

            ['is_del','eq',RegionSetting::NO_DEL]

        ])->field("region_id as id,region_name as name")->select();

        $regionList = $regionList->isEmpty() ? [] : $regionList->toArray();

        $offline_shop_list = OfflineShopObj::where([

            ['is_del','eq',OfflineShopObj::NO_DEL],

        ])->field("id,name")->select();

        $offline_shop_list = $offline_shop_list->isEmpty() ? [] : $offline_shop_list->toArray();

        $field_list = [

            ['form_type'=>"select","name"=>"region_id",'title'=>"区域",'value'=>"",'data' => $regionList],
            ['form_type'=>"select","name"=>"offline_shop_id",'title'=>"线下店",'value'=>"",'data' => $offline_shop_list],
            ['form_type'=>"radio","name"=>"input_type",'title'=>"进件类型",'value'=>UserObj::INPUT_TYPE_OF_HOUSE_LOAN,'data' => UserObj::getInputTypeText("list",'value','title')],
            ['form_type'=>"text","name"=>"region_price",'title'=>"城市底价",'value'=>"",'disabled'=>""],
            ['form_type'=>"text","name"=>"recommend_price",'title'=>"推荐价格",'value'=>"",'disabled'=>""],
            ['form_type'=>"text","name"=>"stick_price",'title'=>"置顶价格",'value'=>""],

        ];

        return $field_list;

    }


    private function getPostData(Request $request) {

        $id = $request->get('id/d',0);

        $data = $request->post();

        $validate = new Validate([

            "region_id" => "require",

            "offline_shop_id" => "require",

            "input_type" => "require",

            "stick_price" => "require",

            "recommend_price" => "require"

        ],[

            "region_id.require" => "请选择区域",

            "offline_shop_id.require" => "请选择线下店",

            "input_type.require" => "请选择进件类型",

            "stick_price.require" => "请设置置顶价格",

            "recommend_price.require" => "请设置推荐价格",

        ]);

        if(!$validate->check($data)) throw new \Exception($validate->getError());

        $RegionSettingInfo = RegionSetting::where([

            ['region_id','eq',$data['region_id']]

        ])->find();

        if(empty($RegionSettingInfo)) throw new \Exception("该城市暂未配置");

        $field = "";

        switch ($data['input_type']) {

            case UserObj::INPUT_TYPE_OF_HOUSE_LOAN:

                $field = "house_loan_min_price";

                break;

            case UserObj::INPUT_TYPE_OF_CAR_LOAN:

                $field = "car_loan_min_price";

                break;

            case UserObj::INPUT_TYPE_OF_CREDIT_LOAN:

                $field = "credit_loan_min_price";

                break;

        }

        if(empty($field) || !isset($RegionSettingInfo[$field])) throw new \Exception("进件类型有误");

        if($RegionSettingInfo[$field] > $data['stick_price']) throw new \Exception("置顶价格不能小于城市底价");

        $is_exists = Obj::where([

            ['offline_shop_id','eq',$data['offline_shop_id']],

            ['input_type','eq',$data['input_type']],

            ['region_id','eq',$data['region_id']],

            ['id','neq',$id]

        ])->find();

        if(!empty($is_exists)) throw new \Exception("该线下店的进件类型已存在");

        if(!empty($data['region_id'])) {

            $merger_name = Region::where([

                ['region_id','eq',$data['region_id']],

            ])->value("merger_name");

            $data['region_name'] = str_replace("中国,","",$merger_name);

        }

        $module_field_data = [];

        foreach ($data as $k=>$v) {

            if(strstr($k,"module_")) {

                array_push($module_field_data,str_replace("module_","",$k).":".$v);

                unset($data[$k]);

            }

        }

        $data['module_field_data'] = implode('|',$module_field_data);

        $data['region_price'] = $RegionSettingInfo[$field];

        return $data;

    }



    public function add(Request $request) {

        if ($request->isPost()) {

            try{

                $data = $this->getPostData($request);

                $data['status'] = StickPlan::STICK_PLAN_STATUS_OF_WAIT_PAYMENT;

                $data['create_time'] = time();

                Obj::insertGetId($data);

            }catch (\Exception $e) {

                $this->error($e->getMessage());

            }

            $this->success('添加成功');

        } else {

            $field_list = $this->getFieldList();

            return view('form', [

                'field_list' => $field_list,

                'title' => "添加{$this->now_keyword}",

            ]);
        }

    }


    public function publish(Request $request) {

        $id = $request->get('id/d',0);

        if($id < 1) $this->error('参数有误');

        if($request->isPost()) {

            try{

                $data = $this->getPostData($request);

                Obj::where('id',$id)->update($data);

            }catch (\Exception $e) {

                $this->error($e->getMessage());

            }

            $this->success("修改成功");

        }else{

            $field_list = $this->getFieldList($id);

            $info = Obj::where('id',$id)->find();

            $render_array = [

                'field_list' => $field_list,

                'title' => "修改{$this->now_keyword}",

                'info' => $info

            ];

            return view('form', $render_array);

        }

    }


    public function del(Request $request) {

        try{

            $id = $request->get('id/d',0);

            if($id < 1) throw new \Exception("参数有误");

            $is_exists = Obj::where([

                ['id','eq',$id],

                ['is_del','eq',Obj::NO_DEL]

            ])->find();

            if(empty($is_exists)) throw new \Exception("数据不存在");

//             if($is_exists['is_allow_del'] == 1) throw new \Exception("禁止删除");

            Obj::where('id',$is_exists['id'])->update([

                "is_del" => Obj::YES_DEL

            ]);

        }catch (\Exception $e) {

            $this->error($e->getMessage());

        }

        $this->success("已删除");

    }



    public function getRegionPrice(Request $request) {

        try{

            $region_id = $request->get('region_id/d',0);

            $input_type = $request->get('input_type/d',0);

            $field = "";

            $price = 0;

            switch ($input_type) {

                case UserObj::INPUT_TYPE_OF_HOUSE_LOAN:

                    $field = "house_loan_min_price";

                    break;

                case UserObj::INPUT_TYPE_OF_CAR_LOAN:

                    $field = "car_loan_min_price";

                    break;

                case UserObj::INPUT_TYPE_OF_CREDIT_LOAN:

                    $field = "credit_loan_min_price";

                    break;

            }

            if(!empty($field)) {

                $price = RegionSetting::where([

                    ['region_id','eq',$region_id],

                    ['is_del','eq',RegionSetting::NO_DEL]

                ])->value($field);

            }

            $result = [

                "price" => $price

            ];

        }catch (\Exception $e) {

            $this->error($e->getMessage());

        }

        $this->success([

            "data" => $result

        ]);

    }



    public function getModulePrice(Request $request) {

        try{

            $module_field_id = $request->post('module_field_id/s',"");

            $price = ModuleField::where([

                ['id','in',$module_field_id],

                ['is_del','eq',ModuleField::NO_DEL]

            ])->sum("price");

            $result = [

                "price" => $price

            ];

        }catch (\Exception $e) {

            $this->error($e->getMessage());

        }

        $this->success([

            "data" => $result

        ]);

    }



    public function getModuleFieldList(Request $request) {

        try{

            $region_id = $request->post('region_id/d',0);

            $check_data = $request->post("check_data/s",'');

            $check_data_array = empty($check_data) ? [] : explode('|',$check_data);

            $regionSettingInfo = RegionSetting::where([

                ['region_id','eq',$region_id],

                ['is_del','eq',RegionSetting::NO_DEL]

            ])->find();

            if(empty($regionSettingInfo)) throw new \Exception("该城市暂未配置");

            $recommend_price = 0;

            $result = [];

            $respon = StickPlan::parseModuleField($regionSettingInfo['module_field_data']);

            foreach ($respon as $k=>$v) {

                $html = "<div class=\"layui-form-item\">
                            <fieldset class=\"layui-elem-field\">
                                <legend>{$v['module_name']}<span class=\"tip\"></span></legend>
                                <div class=\"layui-field-box\">
                                    <div class=\"layui-input-block\">";
                                    foreach ($v['module_field_list'] as $key => $field) {

                                        if(!empty($check_data_array)) {

                                            if(in_array($v['module_id'].":".$field['id'],$check_data_array)) {
                                                $html .= "<input type=\"radio\" lay-filter=\"radio_".($key+1)."\" name=\"module_{$v['module_id']}\" value=\"{$field['id']}\" title=\"{$field['name']}\" checked>";
                                            }else{
                                                $html .= "<input type=\"radio\" lay-filter=\"radio_".($key+1)."\" name=\"module_{$v['module_id']}\" value=\"{$field['id']}\" title=\"{$field['name']}\">";
                                            }

                                        }else{

                                            if($key == 0) {
                                                $html .= "<input type=\"radio\" lay-filter=\"radio_".($key+1)."\" name=\"module_{$v['module_id']}\" value=\"{$field['id']}\" title=\"{$field['name']}\" checked>";
                                            }else{
                                                $html .= "<input type=\"radio\" lay-filter=\"radio_".($key+1)."\" name=\"module_{$v['module_id']}\" value=\"{$field['id']}\" title=\"{$field['name']}\">";
                                            }

                                        }


                                        $recommend_price += $field['price'];

                                    }
                $html .= "          </div>
                                </div>
                            </fieldset>
                        </div>";

                array_push($result,$html);

            }

        }catch (\Exception $e) {

            $this->error($e->getMessage());

        }

        $this->success([

            "data" => implode('',$result),

            "param" => [

                "recommend_price" => $recommend_price

            ],

        ]);

    }


}