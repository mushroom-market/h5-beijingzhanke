<?php

/**
 * 网站配置管理
 */

namespace app\admin\controller;

use app\common\model\Config as Obj;
use app\common\model\Region;
use think\Request;

class WebConfig extends Base
{

    public function initialize()
    {

        parent::initialize();

    }

    public function index(Request $request){

        $module = [

            "web_info" => "网站信息",

            "custom_service_setting" => "客服设置",

            "user_agreement" => "用户端协议",

            "channel_agreement" => "渠道端协议",

            "offline_agreement" => "线下店协议"

        ];

        reset($module);

        $key = $request->get('key/s',key($module));

        if(!isset($module[$key])) $this->error("参数有误",url('index'));

        $field_list = [

            "web_info" => [

                ['form_type'=>"file","name"=>"logo",'title'=>"LOGO",'value'=>""],
                ['form_type'=>"text","name"=>"title",'title'=>"网站名称",'value'=>""],
                ['form_type'=>"file","name"=>"login_bg",'title'=>"登录页面背景图",'value'=>""],

            ],

            "custom_service_setting" => [

                ['form_type'=>"file","name"=>"channel_wechat_qrcode",'title'=>"二维码",'value'=>"",'tip'=>"渠道客服"],
                ['form_type'=>"text","name"=>"channel_wechat_name",'title'=>"微信名称",'value'=>"",'tip'=>"渠道客服"],
                ['form_type'=>"file","name"=>"offline_shop_wechat_qrcode",'title'=>"二维码",'value'=>"",'tip'=>"线下店客服"],
                ['form_type'=>"text","name"=>"offline_shop_wechat_name",'title'=>"微信名称",'value'=>"",'tip'=>"线下店客服"],


            ],

            "user_agreement" => [

                ['form_type'=>"ueditor","name"=>"user_register_agreement",'title'=>"注册服务协议",'value'=>"",'tip'=>""],
                ['form_type'=>"ueditor","name"=>"user_privacy_policy",'title'=>"隐私政策",'value'=>"",'tip'=>""],
                ['form_type'=>"ueditor","name"=>"user_authorization",'title'=>"授权书",'value'=>"",'tip'=>""],

            ],

            "offline_agreement" => [

                ['form_type'=>"ueditor","name"=>"cooperation_agreement",'title'=>"合作协议",'value'=>"",'tip'=>""],
                ['form_type'=>"ueditor","name"=>"cooperation_behavior_agreement",'title'=>"信贷员/机构合作行为规定",'value'=>"",'tip'=>""],
                ['form_type'=>"ueditor","name"=>"service_convention_agreement",'title'=>"信贷员/机构服务公约",'value'=>"",'tip'=>""],
                ['form_type'=>"ueditor","name"=>"compliance_commitment_agreement",'title'=>"合规承诺函",'value'=>"",'tip'=>""],

            ],

            "channel_agreement" => [

                ['form_type'=>"ueditor","name"=>"cooperation_agreement",'title'=>"合作协议",'value'=>"",'tip'=>""],
                ['form_type'=>"ueditor","name"=>"cooperation_behavior_agreement",'title'=>"个人/渠道合作行为规定",'value'=>"",'tip'=>""],
                ['form_type'=>"ueditor","name"=>"promotion_convention_agreement",'title'=>"个人/机构推广公约",'value'=>"",'tip'=>""],
                ['form_type'=>"ueditor","name"=>"promotion_compliance_commitment_agreement",'title'=>"推广合规承诺函",'value'=>"",'tip'=>""],

            ],

        ];

        $redirect_array = [

            'module_list' => $module,

            'sub_title' => "网站设置",

            'field_list' => $field_list[$key],

            'check_key' => $key,

            'info' => Obj::getCacheValue($key),

        ];

        return view('index', $redirect_array);

    }



    /**
     * 更新配置信息
     * @param Request $request
     */
    public function updateSetting(Request $request) {

        if($request->isPost()) {

            try{

                $key = $request->get('key/s','');

                $data = $request->post();

                $data = serialize($data);

                $id = Obj::where([

                    ['key','eq',$key],

                    ['is_del','eq',Obj::NO_DEL]

                ])->value('id');

                if(empty($id)) {

                    Obj::insertGetId([

                        "key" => $key,

                        "value" => $data,

                    ]);

                }else{

                    Obj::where([

                        ['id','eq',$id]

                    ])->update([

                        "value" => $data,

                    ]);

                }

                Obj::setCacheValue($key);

            }catch (\Exception $e) {

                $this->error($e->getMessage());

            }

            $this->success('提交成功');

        }


    }


    /**
     * 获取下级地区
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getRegionChildrenList() {

        $region_id = input('get.region_id/d',0);

        if($region_id < 1) $this->error('参数有误');

        $list = (new Region())->getChildrenList($region_id);

        array_unshift($list,['id'=>0,'region_id'=>0,'region_name'=>"请选择.."]);

        return $list;

    }


}