<?php

namespace app\api\controller;

use app\common\model\Advertisement;
use app\common\model\PageStatistics;
use app\common\model\Product;
use app\common\model\ProductLinkVisitLog;
use app\common\model\ProductStatistics;
use app\common\model\ProductTags;
use message\Message;
use think\Request;
use think\Validate;

class Index extends Base {


    public function index() {

        $result = [

            "advertisement_list" => $this->advertisement(),

            "nav_list" => $this->nav(),

        ];

        $this->json_result($result);

    }

    private function advertisement() {

        $obj = new Advertisement();

        $result = $obj->where([

            ['is_del','eq',Advertisement::NO_DEL],

            ['status','eq',Advertisement::STATUS_NORMAL]

        ])->order("sort DESC,id DESC")->field("img,link_value")->select();

        $result = $result->isEmpty() ? [] : $result->toArray();

        return $result;

    }

    private function nav() {

        $result = [

            ['name' => "爆款",'value'=>"baokuan"],
//
//            ['name' => "每日更新",'value'=>"daily_update"],
//
//            ['name' => "信贷",'value'=>"credit_loan"],
//
//            ['name' => "信用卡",'value'=>"credit_card"],

        ];

        return $result;

    }


    public function productList(Request $request) {

        $keyword = $request->get('keyword/s','');

        $type = $request->get('type/s','');

        $obj = new Product();

        $where = [

            ['is_del','eq',Product::NO_DEL],

            ['status','eq',Product::STATUS_NORMAL],

        ];

        $whereOr = [];

        if(!empty($keyword)) {

            array_push($whereOr,['name','like',"%{$keyword}%"]);

            $productTagsObj = new ProductTags();

            $product_tags_id = $productTagsObj->where([

                ['name','like',"%{$keyword}%"],

                ['status','eq',ProductTags::STATUS_NORMAL],

                ['is_del','eq',ProductTags::NO_DEL],

            ])->column('id');

            if(!empty($product_tags_id)) {

                $product_id = [];

                foreach ($product_tags_id as $v) {

                    $p_id = $obj->where("FIND_IN_SET('{$v}',`tags_id`)")->column("id");

                    $product_id = array_merge($product_id,$p_id);

                }

                if(!empty($product_id)) array_push($whereOr,['id','in',$product_id]);

            }

        }

        if(!empty($type)) {

            switch ($type) {

                case 'baokuan':

                    array_push($where,['is_baokuan','eq',1]);

                    break;

                case 'daily_update':

                    array_push($where,['is_daily_update','eq',1]);

                    break;

                case 'credit_loan':

                    array_push($where,['type','eq',Product::PRODUCT_TYPE_OF_CREDIT_LOAN]);

                    break;

                case 'credit_card':

                    array_push($where,['type','eq',Product::PRODUCT_TYPE_OF_CREDIT_CARD]);

                    break;


            }

        }

        $page_mod = $obj->where($where)->whereOr($whereOr)->order("sort DESC")->paginate(20)->toArray();

        $result = [];

        foreach ($page_mod['data'] as $v) {

            array_push($result,[

                "id" => $v['id'],

                "name" => $v['name'],

                "thumb" => $v['thumb'],

                "position_scope" => "额度范围：".$v['position_scope'],

                "day_rate" => "日利率：".$v['day_rate'],

                "loan_time" => "贷款年限：".$v['loan_time'],

            ]);

        }

        $this->json_result([

            "data" => $result,

            "has" => $page_mod['last_page'] > $page_mod['current_page']

        ]);

    }




    public function productDetail(Request $request) {

        try{

            $product_id = $request->get('product_id/d',0);

            $obj = new Product();

            $product_info = $obj->where([

                ['id','eq',$product_id],

                ['is_del','eq',Product::NO_DEL]

            ])->find();

            if(empty($product_info)) throw new \Exception("产品信息不存在");

            if($product_info['status'] == Product::STATUS_DISABLED) throw new \Exception("该产品已下架");

            (new PageStatistics())->insertGetId([

                "user_id" => $this->user_id,

                "product_id" => $product_id,

                "page" => PageStatistics::PAGE_OF_PRODUCT_DETAIL,

                "ip" => $request->ip(),

                "create_time" => time()

            ]);

            $result = [

                "id" => $product_info['id'],

                "banner" => empty($product_info['banner']) ? [] : explode(',',$product_info['banner']),

                "name" => $product_info['name'],

                "introduce" => $product_info['introduce'],

            ];

        }catch (\Exception $e) {

            $this->json_error($e->getMessage());

        }

        $this->json_result($result);

    }




    public function checkMobile(Request $request) {

        try{

            requestAccess($request,1);

            $data = [

                "mobile" => $request->post('mobile/s',''),

                "code" => $request->post('code/s',''),

                "product_id" => $request->post('product_id/d',''),

            ];

            $validate = new Validate([

                "mobile" => "require|mobile",

                "code" => "require",

                "product_id" => "require",

            ],[

                "mobile.require" => "请输入手机号",

                "mobile.mobile" => "手机号格式有误",

                "code.require" => "短信验证码有误",

                "product_id.require" => "参数有误",

            ]);

            if(!$validate->only(['product_id'])->check($data)) throw new \Exception($validate->getError());

//            Message::getInstance()->checkSMSCode($data['mobile'], $data['code'], "check");
//
//            Message::getInstance()->clearSMSCode($data['mobile'], "check");

            $obj = new Product();

            $product_info = $obj->where([

                ['id','eq',$data['product_id']],

                ['is_del','eq',Product::NO_DEL]

            ])->find();

            if(empty($product_info)) throw new \Exception("产品信息不存在");

            if($product_info['status'] != Product::STATUS_NORMAL) throw new \Exception("该产品已下架");

            $todayProductStatistics = ProductStatistics::where([

                ['product_id','eq',$data['product_id']],

                ['time','eq',date("Ymd",time())],

            ])->find();

            if(empty($todayProductStatistics)) throw new \Exception("该产品下架");

            $productLinkVisitLog = new ProductLinkVisitLog();

            if($todayProductStatistics['uv'] >= $todayProductStatistics['day_uv_max_num']) {

                $obj->where([

                    ['id','eq',$data['product_id']],

                    ['is_del','eq',Product::NO_DEL]

                ])->update([

                    "status" => 2,

                    "hide_time" => time()

                ]);

                throw new \Exception("该产品已下架");

            }

            $productLinkVisitLog->insertGetId([

                "product_id" => $product_info['id'],

                "link" => $product_info['link'],

                "user_id" => $this->user_id,

                "mobile" => $data['mobile'],

                "ip" => $request->ip(),

                "create_time" => time(),

            ]);

            $is_today = PageStatistics::where([

                ['user_id','eq',$this->user_id],

                ['product_id','eq',$data['product_id']],

                ['page','eq',PageStatistics::PAGE_OF_PRODUCT_LINK]

            ])->whereTime("create_time","today")->find();

            if(empty($is_today)) {

                $update_data = [

                    "uv" => ['inc',1]

                ];

//                if($todayProductStatistics['settlement_type'] == 1) $update_data['result'] = ['inc',1];
//
//                if($todayProductStatistics['settlement_type'] == 2) $update_data['result'] = ['inc',1];
//
//                if($todayProductStatistics['settlement_type'] == 3) $update_data['result'] = ['inc',1];

                ProductStatistics::where([

                    ['product_id','eq',$data['product_id']],

                    ['time','eq',date("Ymd",time())],

                ])->update($update_data);

            }



            PageStatistics::insertGetId([

                "user_id" => $this->user_id,

                "product_id" => $data['product_id'],

                "page" => PageStatistics::PAGE_OF_PRODUCT_LINK,

                "ip" => $request->ip(),

                "create_time" => time()

            ]);

            $result = [

                "link" => $product_info['link']

            ];

        }catch (\Exception $e) {

            $this->json_error($e->getMessage());

        }

        $this->json_result($result,"操作成功");

    }



    public function appLink(Request $request) {

        $result = [

            "link" => $request->root(true)."/?invite_code=950514#/",

        ];

        $this->json_result($result,"操作成功");

    }




}