<?php


namespace app\api\controller;

use app\common\model\ChannelInviteCode;
use app\common\model\Config;
use app\common\model\ExtensionPlan;
use app\common\model\Module;
use app\common\model\OfflineShop;
use app\common\model\OfflineShopMoneyLog;
use app\common\model\OfflineShopUser;
use app\common\model\Region;
use app\common\model\RegionSetting;
use app\common\model\StickPlan;
use app\common\model\StickPlanModule;
use app\common\model\UserA as UserAObj;
use app\common\model\UserB as UserBObj;
use app\common\model\UserAModule;
use app\common\model\UserProcedure;
use message\Message;
use think\Request;
use think\Validate;
use verify\Aliyun;

class Login extends Base{


    public function login(Request $request) {
        if($request->isPost()) {

            try{

                $data = [

                    "mobile" => $request->post('mobile/s',''),

                    "code" => $request->post('code/s',''),

                    "invite_code" => $request->post('invite_code/s',''),

                    "region_id" => $request->post('region_id/s',''),

                    "longitude" => $request->post('longitude/s',''),

                    "latitude" => $request->post('latitude/s',''),

                ];
                $data['invite_code'] = '123456789';
                $validate = new Validate([

                    "mobile" => "require|mobile",

                    "code" => "require",

                    "invite_code" => "require",

                    "region_id" => "require",

                    "longitude" => "require",

                    "latitude" => "require",

                ],[

                    "mobile.require" => "请输入手机号",

                    "mobile.mobile" => "手机号格式有误",

                    "code.require" => "短信验证码有误",

                    "invite_code.require" => "邀请码有误",

                    "region_id.require" => "请先获取位置",

                    "longitude.require" => "请先获取位置",

                    "latitude.require" => "请先获取位置",

                ]);

               /* if(!$validate->only(['mobile',"code","invite_code"])->check($data)) throw new \Exception($validate->getError());

                Message::getInstance()->checkSMSCode($data['mobile'], $data['code'], "check");

                Message::getInstance()->clearSMSCode($data['mobile'], "check");*/

                $userAObj = new UserAObj();

                $userBObj = new UserBObj();

                $user_a_info = $userAObj->where([

                    ['mobile','eq',$data['mobile']],
                    
                    ['is_del','eq',UserAObj::NO_DEL],
                    
                ])->find();

                $user_b_info = $userBObj->where([

                    ['mobile','eq',$data['mobile']],

                    ['is_del','eq',UserBObj::NO_DEL],

                ])->find();

                $procedure = "";

                $redirect_type = 0;

                if(empty($user_a_info) && empty($user_b_info)) {

                    if(!$validate->only(['region_id',"longitude","latitude"])->check($data)) throw new \Exception($validate->getError());

                    $region_info = Region::where([

                        ['region_id','eq',$data['region_id']],

                    ])->find();

                    if(empty($region_info)) throw new \Exception("请先获取位置");

                    $inviteCodeObj = new ChannelInviteCode();

                    $channelInviteCode = $inviteCodeObj->where([

                        ['code','eq',$data['invite_code']],

                        ['is_del','eq',ChannelInviteCode::NO_DEL]

                    ])->find();

                    if(empty($channelInviteCode)) throw new \Exception("邀请码失效");

                    $now_time = time() - strtotime(date("Y-m-d 00:00:00"));

                    $info = ExtensionPlan::where([

                        ['start_time','elt',$now_time],

                        ['end_time','egt',$now_time],

                        ['is_del','eq',ExtensionPlan::NO_DEL],

                        ['status','eq',ExtensionPlan::STATUS_NORMAL],

                        ['channel_id','eq',$channelInviteCode['channel_id']]

                    ])->where("FIND_IN_SET('{$region_info['parent_region_id']}',`region_id`)")->find();

                    $insert_data = [

                        "mobile" => $data['mobile'],

                        "region_id" => $data['region_id'],

                        "region_name" => str_replace("中国,","",$region_info['merger_name']),

                        "city_region_id" => $region_info['parent_region_id'],

                        "longitude" => $data['longitude'],

                        "latitude" => $data['latitude'],

                        "channel_id" => $channelInviteCode['channel_id'],

                        "invite_code_id" => $channelInviteCode['id'],

                        "progress" => UserBObj::PROGRESS_OF_REGISTER,

                        "create_time" => time()

                    ];
                    
                    if(!empty($info)) {

                        $insert_data['extension_plan_id'] = $info['id'];

                        $user_id = $userBObj->insertGetId($insert_data);
                        $procedure = "b";

                    }else{

                        $user_id = $userAObj->insertGetId($insert_data);

                        $procedure = "a";

                    }

                    $inviteCodeObj->where('id',$channelInviteCode['id'])->update([

                        "register_num" => ['inc',1]

                    ]);

                    $progress = "step_".UserAObj::PROGRESS_OF_REGISTER;

                }else{

                    if(!empty($user_a_info)) {

                        $user_id = $user_a_info['id'];

                        $progress = "step_".$user_a_info['progress'];

                        $procedure = "a";

                    }

                    if(!empty($user_b_info)) {

                        $user_id = $user_b_info['id'];

                        $progress = "step_".$user_b_info['progress'];

                        $procedure = "b";

                        $redirect_type = ExtensionPlan::where('id',$user_b_info['extension_plan_id'])->value("redirect_type");

                    }


                }
                if($procedure == "a") {

                    $token = $userAObj->getToken($user_id);

                    $userAObj->where('id',$user_id)->setField("token",$token);

                }elseif($procedure == "b"){

                    $token = $userBObj->getToken($user_id);

                    $userBObj->where('id',$user_id)->setField("token",$token);

                }

                $is_exists_user_procedure = UserProcedure::where([

                    ['user_id','eq',$user_id],

                    ['u_procedure','eq',$procedure]

                ])->find();

                if(!empty($is_exists_user_procedure)) {

                    UserProcedure::where('id',$is_exists_user_procedure['id'])->setField("token",$token);

                }else{

                    UserProcedure::insertGetId([

                        "user_id" => $user_id,

                        "token" => $token,

                        "u_procedure" => $procedure,

                        "create_time" => time()

                    ]);

                }

                $timestamp = time();

                $result = [

                    'token' => $token,

                    'user_id' => $user_id,

                    'sign' => md5($user_id . $token . $timestamp),

                    'timestamp' => $timestamp,

                    "progress" => $progress,

                    "procedure" => $procedure,

                    "redirect_type" => $redirect_type,

                ];

            }catch (\Exception $e) {

                $this->json_error($e->getMessage());

            }

            $this->json_result($result);

        }else{

            $web_info = Config::getCacheValue("web_info");

            $bg_img = isset($web_info['login_bg']) ? $web_info['login_bg'] : "";

            $web_title = isset($web_info['title']) ? $web_info['title'] : "";

            $result = [

                "bg_img" => empty($bg_img) ? "" : LOCAL_URL.$bg_img,

                "web_title" => empty($web_title) ? "" : $web_title,

                "user_register_agreement" => url('userRegisterAgreement','',true,true),

                "user_privacy_policy" => url('userPrivacyPolicy','',true,true),

                "user_authorization" => url('userAuthorization','',true,true),

            ];

            $this->json_result($result);

        }

    }



    public function aa() {

        $channel_id = 3;
        $region_id = 410100;

        $now_time = time() - strtotime(date("Y-m-d 00:00:00"));

        $a = ExtensionPlan::where([

            ['region_id','eq',$region_id],

            ['start_time','elt',$now_time],

            ['end_time','egt',$now_time],

            ['is_del','eq',ExtensionPlan::NO_DEL]

        ])->where("FIND_IN_SET('{$channel_id}',`channel_id`)")->find();

        dump($a);die;

    }


    public function userRegisterAgreement() {

        $user_agreement = Config::getCacheValue("user_register_agreement");

        return view('html/html',[

            "title" => "用户注册服务协议",

            "content" => isset($user_agreement['content']) ? $user_agreement['content'] : ""

        ]);

    }


    public function userPrivacyPolicy() {

        $user_privacy_policy = Config::getCacheValue("user_privacy_policy");

        return view('html/html',[

            "title" => "用户隐私政策",

            "content" => isset($user_privacy_policy['content']) ? $user_privacy_policy['content'] : ""

        ]);

    }


    public function userAuthorization() {

        $user_authorization = Config::getCacheValue("user_authorization");

        return view('html/html',[

            "title" => "用户授权书",

            "content" => isset($user_authorization['content']) ? $user_authorization['content'] : ""

        ]);

    }




    public function sendMessage(Request $request)
    {

        try {

            $mobile = $request->post('mobile/s', '');

            $type = $request->post('type/s', 'check');

            if (empty($mobile)) throw new \Exception("手机号验证有误");

            switch ($type) {

                case 'check':

                    Message::getInstance()->sendSMSCode($mobile, 10, "check");

                    break;

                default:

                    throw new \Exception("类型有误");

                    break;

            }

        } catch (\Exception $e) {

            $this->json_error($e->getMessage());

        }

        $this->json_result(true, "发送成功");

    }




    public function perfect(Request $request) {

        if($request->isPost()) {

            try{

                requestAccess($request,1);

                if($this->user_info['progress'] != UserAObj::PROGRESS_OF_REGISTER) throw new \Exception("请勿重复完善信息");
               
                $change_data = [

                    "real_name" => "real_name",

                    "sex" => "sex",

                    "id_card" => "id_card",

                    "module_13" => "year_income",

                    "module_1" => "occupation",

                    "module_2" => "loan_money",

                    "module_3" => "is_has_car",

                    "module_4" => "is_has_house",

                    "module_5" => "social_security",

                    "module_6" => "public_accumulation_fund",

                    "module_7" => "credit_investigation",

                ];

                $need_data = [

                    "real_name",
                    "sex",
                    "id_card",
                    "module_13",
                    "module_1",
                    "module_2",
                    "module_3",
                    "module_4",
                    "module_5",
                    "module_6",
                    "module_7"

                ];

                $all_data = $request->post();

                $data = [];

                foreach ($all_data as $k=>$v) {

                    if(in_array($k,$need_data)) {

                        $data[$change_data[$k]] = $v;

                    }

                }

                $validate = new Validate([

                    "real_name" => "require",

                    "sex" => "require",

                    "id_card" => "require",

                    "year_income" => "require",

                    "occupation" => "require",

                    "loan_money" => "require",

                    "is_has_car" => "require",

                    "is_has_house" => "require",

                    "social_security" => "require",

                    "public_accumulation_fund" => "require",

                    "credit_investigation" => "require",

                ],[

                    "real_name.require" => "请输入姓名",

                    "sex.require" => "请选择性别",

                    "id_card.require" => "请输入身份证号",

                    "year_income.require" => "请选择年收入",

                    "occupation.require" => "请选择职业",

                    "loan_money.require" => "请选择贷款金额",

                    "is_has_car.require" => "请选择是否有车",

                    "is_has_house.require" => "请选择是否有房",

                    "social_security.require" => "请选择社保",

                    "public_accumulation_fund.require" => "请选择公积金",

                    "credit_investigation.require" => "请选择征信情况",

                ]);

                if(!$validate->only(array_keys($data))->check($data)) throw new \Exception($validate->getError());

                $userAObj = new UserAObj();
                
                $is_exists = $userAObj->where([

                    ['id_card','eq',$data['id_card']],

                    ['is_del','eq',UserAObj::NO_DEL]

                ])->count();

                if($is_exists > 0) throw new \Exception("您的身份信息已经录入，请不要重复录入");

                $data = array_merge($data,[

                    "progress" => UserAObj::PROGRESS_OF_PERFECT,

                    "perfect_time" => time()

                ]);

                $data['input_type'] = UserAObj::INPUT_TYPE_OF_CREDIT_LOAN;

                if(isset($data['is_has_house']) && $data['is_has_house'] == UserAObj::HOUSE_MORTGAGE) $data['input_type'] = UserAObj::INPUT_TYPE_OF_HOUSE_LOAN;

                if(isset($data['is_has_car']) && $data['is_has_car'] == UserAObj::CAR_MORTGAGE && isset($data['is_has_house']) && $data['is_has_house'] != UserAObj::HOUSE_MORTGAGE) $data['input_type'] = UserAObj::INPUT_TYPE_OF_CAR_LOAN;

                $module_field_data = [];

                foreach ($request->post() as $k=>$v) {

                    if(strstr($k,"module_")) {

                        $module_id = str_replace("module_","",$k);

                        array_push($module_field_data,$module_id.":".$v);

                    }

                }

                $data['module_field_data'] = implode('|',$module_field_data);

                $respon = StickPlan::parseModuleField($data['module_field_data']);

                $userModuleData = [];

                foreach ($respon as $v) {

                    $module_field_name = implode(',',array_column($v['module_field_list'],"name"));

                    switch ($v['module_id']) {

                        case 1:

                            $data['occupation_text'] = $module_field_name;

                            break;
                        case 2:

                            $data['loan_money_text'] = $module_field_name;

                            break;
                        case 3:

                            $data['is_has_car_text'] = $module_field_name;

                            break;
                        case 4:

                            $data['is_has_house_text'] = $module_field_name;

                            break;
                        case 5:

                            $data['social_security_text'] = $module_field_name;

                            break;
                        case 6:

                            $data['public_accumulation_fund_text'] = $module_field_name;

                            break;
                        case 7:

                            $data['credit_investigation_text'] = $module_field_name;

                            break;
                          case 13:

                            $data['year_income_text'] = $module_field_name;

                            break;  
                    }

                    array_push($userModuleData,[

                        "user_id" => $this->user_id,

                        "module_id" => $v['module_id'],

                        "module_name" => $v['module_name'],

                        "module_field_id" => implode(',',array_column($v['module_field_list'],"id")),

                        "module_field_name" => $module_field_name,

                    ]);

                }
// var_dump($data,$this->user_info->mobile);
                /*Aliyun::getInstance()->verify(array(
                     
                    'idcard' => $data['id_card'],
                    
                    'name' => $data['real_name'],
                    
                    'mobile' => $this->user_info->mobile
                    
                ));*/
                
                $userAObj->where('id',$this->user_id)->update($data);

                if(!empty($userModuleData)) UserAModule::insertAll($userModuleData);

            }catch (\Exception $e) {

                $this->json_error($e->getMessage());

            }

            $this->json_result(true,"提交成功");

        }else{

            $regionSettingInfo = RegionSetting::where([

                ['region_id','eq',$this->user_info['city_region_id']],

                ['is_del','eq',RegionSetting::NO_DEL]

            ])->field("module_field_data")->find();

            if(empty($regionSettingInfo)) {

                $defaultRegionSettingInfo = RegionSetting::where([

                    ['is_default','eq',1],

                    ['is_del','eq',RegionSetting::NO_DEL]

                ])->field("module_field_data")->find();

                if(empty($defaultRegionSettingInfo)) throw new \Exception("暂无默认城市模板");

                $respon = StickPlan::parseModuleField($defaultRegionSettingInfo['module_field_data'],1);

            }else{

                $respon = StickPlan::parseModuleField($regionSettingInfo['module_field_data'],1);

            }

            $result = [];

            foreach ($respon as $v) {

                $ff = [

                    "title" => $v['module_name'],

                    "field" => "module_".$v['module_id'],

                    "value" => []

                ];

                foreach ($v['module_field_list'] as $field) {

                    array_push($ff['value'],[

                        "name" => $field['name'],

                        "value" => $field['id']

                    ]);

                }

                if(empty($ff['value'])) continue;

                array_push($result,$ff);

            }

            $this->json_result($result);

        }

    }



}