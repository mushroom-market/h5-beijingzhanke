<?php


namespace app\api\controller;

use app\common\model\ChannelInviteCode;
use app\common\model\Config;
use app\common\model\Module;
use app\common\model\OfflineShop;
use app\common\model\OfflineShopMoneyLog;
use app\common\model\OfflineShopUser;
use app\common\model\Region;
use app\common\model\RegionSetting;
use app\common\model\StickPlan;
use app\common\model\StickPlanModule;
use app\common\model\UserA as UserObj;
use app\common\model\UserAModule;
use message\Message;
use think\Request;
use think\Validate;
use verify\Aliyun;

class Login extends Base{


    public function login(Request $request) {

        if($request->isPost()) {

            try{

                $data = [

                    "mobile" => $request->post('mobile/s',''),

                    "code" => $request->post('code/s',''),

                    "invite_code" => $request->post('invite_code/s',''),

                    "region_id" => $request->post('region_id/s',''),

                    "longitude" => $request->post('longitude/s',''),

                    "latitude" => $request->post('latitude/s',''),

                ];

                $validate = new Validate([

                    "mobile" => "require|mobile",

                    "code" => "require",

                    "invite_code" => "require",

                    "region_id" => "require",

                    "longitude" => "require",

                    "latitude" => "require",

                ],[

                    "mobile.require" => "请输入手机号",

                    "mobile.mobile" => "手机号格式有误",

                    "code.require" => "短信验证码有误",

                    "invite_code.require" => "邀请码有误",

                    "region_id.require" => "请先获取位置",

                    "longitude.require" => "请先获取位置",

                    "latitude.require" => "请先获取位置",

                ]);

                if(!$validate->only(['mobile',"code","invite_code"])->check($data)) throw new \Exception($validate->getError());

                Message::getInstance()->checkSMSCode($data['mobile'], $data['code'], "check");

                Message::getInstance()->clearSMSCode($data['mobile'], "check");

                $userObj = new UserObj();

                $user_info = $userObj->where([

                    ['mobile','eq',$data['mobile']],
                    
                    ['is_del','eq',UserObj::NO_DEL],
                    
                ])->find();

                $progress = "step_".UserObj::PROGRESS_OF_REGISTER;

                if(empty($user_info)) {

                    if(!$validate->only(['region_id',"longitude","latitude"])->check($data)) throw new \Exception($validate->getError());

                    $region_info = Region::where([

                        ['region_id','eq',$data['region_id']],

                    ])->find();

                    if(empty($region_info)) throw new \Exception("请先获取位置");

                    $inviteCodeObj = new ChannelInviteCode();

                    $channelInviteCode = $inviteCodeObj->where([

                        ['code','eq',$data['invite_code']],

                        ['is_del','eq',ChannelInviteCode::NO_DEL]

                    ])->find();

                    if(empty($channelInviteCode)) throw new \Exception("邀请码失效");

                    $user_id = $userObj->insertGetId([

                        "mobile" => $data['mobile'],

                        "region_id" => $data['region_id'],

                        "region_name" => str_replace("中国,","",$region_info['merger_name']),

                        "city_region_id" => $region_info['parent_region_id'],

                        "longitude" => $data['longitude'],

                        "latitude" => $data['latitude'],

                        "channel_id" => $channelInviteCode['channel_id'],

                        "invite_code_id" => $channelInviteCode['id'],

                        "progress" => UserObj::PROGRESS_OF_REGISTER,

                        "create_time" => time()

                    ]);

                    $inviteCodeObj->where('id',$channelInviteCode['id'])->update([

                        "register_num" => ['inc',1]

                    ]);

                }else{

                    $user_id = $user_info['id'];

                    $progress = "step_".$user_info['progress'];

                }

                $token = $userObj->getToken($user_id);

                $userObj->where('id',$user_id)->update(['token'=>$token]);

                $timestamp = time();

                $result = [

                    'token' => $token,

                    'user_id' => $user_id,

                    'sign' => md5($user_id . $token . $timestamp),

                    'timestamp' => $timestamp,

                    "progress" => $progress

                ];

            }catch (\Exception $e) {

                $this->json_error($e->getMessage());

            }

            $this->json_result($result);

        }else{

            $web_info = Config::getCacheValue("web_info");

            $bg_img = isset($web_info['login_bg']) ? $web_info['login_bg'] : "";

            $web_title = isset($web_info['title']) ? $web_info['title'] : "";

            $result = [

                "bg_img" => empty($bg_img) ? "" : LOCAL_URL.$bg_img,

                "web_title" => empty($web_title) ? "" : $web_title,

                "user_register_agreement" => url('userRegisterAgreement','',true,true),

                "user_privacy_policy" => url('userPrivacyPolicy','',true,true),

                "user_authorization" => url('userAuthorization','',true,true),

            ];

            $this->json_result($result);

        }

    }


    public function userRegisterAgreement() {

        $user_agreement = Config::getCacheValue("user_register_agreement");

        return view('html/html',[

            "title" => "用户注册服务协议",

            "content" => isset($user_agreement['content']) ? $user_agreement['content'] : ""

        ]);

    }


    public function userPrivacyPolicy() {

        $user_privacy_policy = Config::getCacheValue("user_privacy_policy");

        return view('html/html',[

            "title" => "用户隐私政策",

            "content" => isset($user_privacy_policy['content']) ? $user_privacy_policy['content'] : ""

        ]);

    }


    public function userAuthorization() {

        $user_authorization = Config::getCacheValue("user_authorization");

        return view('html/html',[

            "title" => "用户授权书",

            "content" => isset($user_authorization['content']) ? $user_authorization['content'] : ""

        ]);

    }



    public function perfect(Request $request) {

        if($request->isPost()) {

            try{

                requestAccess($request,1);

                if($this->user_info['progress'] != UserObj::PROGRESS_OF_REGISTER) throw new \Exception("请勿重复完善信息");
               
                $change_data = [

                    "real_name" => "real_name",

                    "sex" => "sex",

                    "id_card" => "id_card",

                    "module_13" => "year_income",

                    "module_1" => "occupation",

                    "module_2" => "loan_money",

                    "module_3" => "is_has_car",

                    "module_4" => "is_has_house",

                    "module_5" => "social_security",

                    "module_6" => "public_accumulation_fund",

                    "module_7" => "credit_investigation",

                ];

                $need_data = [

                    "real_name",
                    "sex",
                    "id_card",
                    "module_13",
                    "module_1",
                    "module_2",
                    "module_3",
                    "module_4",
                    "module_5",
                    "module_6",
                    "module_7"

                ];

                $all_data = $request->post();

                $data = [];

                foreach ($all_data as $k=>$v) {

                    if(in_array($k,$need_data)) {

                        $data[$change_data[$k]] = $v;

                    }

                }

                $validate = new Validate([

                    "real_name" => "require",

                    "sex" => "require",

                    "id_card" => "require",

                    "year_income" => "require",

                    "occupation" => "require",

                    "loan_money" => "require",

                    "is_has_car" => "require",

                    "is_has_house" => "require",

                    "social_security" => "require",

                    "public_accumulation_fund" => "require",

                    "credit_investigation" => "require",

                ],[

                    "real_name.require" => "请输入姓名",

                    "sex.require" => "请选择性别",

                    "id_card.require" => "请输入身份证号",

                    "year_income.require" => "请选择年收入",

                    "occupation.require" => "请选择职业",

                    "loan_money.require" => "请选择贷款金额",

                    "is_has_car.require" => "请选择是否有车",

                    "is_has_house.require" => "请选择是否有房",

                    "social_security.require" => "请选择社保",

                    "public_accumulation_fund.require" => "请选择公积金",

                    "credit_investigation.require" => "请选择征信情况",

                ]);

                if(!$validate->only(array_keys($data))->check($data)) throw new \Exception($validate->getError());

                $userObj = new UserObj();
                
                $is_exists = $userObj->where([

                    ['id_card','eq',$data['id_card']],

                    ['is_del','eq',UserObj::NO_DEL]

                ])->count();

                if($is_exists > 0) throw new \Exception("您的身份信息已经录入，请不要重复录入");

                $data = array_merge($data,[

                    "progress" => UserObj::PROGRESS_OF_PERFECT,

                    "perfect_time" => time()

                ]);

                $data['input_type'] = UserObj::INPUT_TYPE_OF_CREDIT_LOAN;

                if(isset($data['is_has_house']) && $data['is_has_house'] == UserObj::HOUSE_MORTGAGE) $data['input_type'] = UserObj::INPUT_TYPE_OF_HOUSE_LOAN;

                if(isset($data['is_has_car']) && $data['is_has_car'] == UserObj::CAR_MORTGAGE && isset($data['is_has_house']) && $data['is_has_house'] != UserObj::HOUSE_MORTGAGE) $data['input_type'] = UserObj::INPUT_TYPE_OF_CAR_LOAN;

                $module_field_data = [];

                foreach ($request->post() as $k=>$v) {

                    if(strstr($k,"module_")) {

                        $module_id = str_replace("module_","",$k);

                        array_push($module_field_data,$module_id.":".$v);

                    }

                }

                $data['module_field_data'] = implode('|',$module_field_data);

                $respon = StickPlan::parseModuleField($data['module_field_data']);

                $userModuleData = [];

                foreach ($respon as $v) {

                    $module_field_name = implode(',',array_column($v['module_field_list'],"name"));

                    switch ($v['module_id']) {

                        case 1:

                            $data['occupation_text'] = $module_field_name;

                            break;
                        case 2:

                            $data['loan_money_text'] = $module_field_name;

                            break;
                        case 3:

                            $data['is_has_car_text'] = $module_field_name;

                            break;
                        case 4:

                            $data['is_has_house_text'] = $module_field_name;

                            break;
                        case 5:

                            $data['social_security_text'] = $module_field_name;

                            break;
                        case 6:

                            $data['public_accumulation_fund_text'] = $module_field_name;

                            break;
                        case 7:

                            $data['credit_investigation_text'] = $module_field_name;

                            break;
                          case 13:

                            $data['year_income_text'] = $module_field_name;

                            break;  
                    }

                    array_push($userModuleData,[

                        "user_id" => $this->user_id,

                        "module_id" => $v['module_id'],

                        "module_name" => $v['module_name'],

                        "module_field_id" => implode(',',array_column($v['module_field_list'],"id")),

                        "module_field_name" => $module_field_name,

                    ]);

                }

                
                Aliyun::getInstance()->verify(array(
                     
                    'idcard' => $data['id_card'],
                    
                    'name' => $data['real_name'],
                    
                    'mobile' => $this->user_info['mobile']
                    
                ));
                
                $userObj->where('id',$this->user_id)->update($data);

                if(!empty($userModuleData)) UserAModule::insertAll($userModuleData);

            }catch (\Exception $e) {

                $this->json_error($e->getMessage());

            }

            $this->json_result(true,"提交成功");

        }else{

            $regionSettingInfo = RegionSetting::where([

                ['region_id','eq',$this->user_info['city_region_id']],

                ['is_del','eq',RegionSetting::NO_DEL]

            ])->field("module_field_data")->find();

            if(empty($regionSettingInfo)) {

                $defaultRegionSettingInfo = RegionSetting::where([

                    ['is_default','eq',1],

                    ['is_del','eq',RegionSetting::NO_DEL]

                ])->field("module_field_data")->find();

                if(empty($defaultRegionSettingInfo)) throw new \Exception("暂无默认城市模板");

                $respon = StickPlan::parseModuleField($defaultRegionSettingInfo['module_field_data'],1);

            }else{

                $respon = StickPlan::parseModuleField($regionSettingInfo['module_field_data'],1);

            }

            $result = [];

            foreach ($respon as $v) {

                $ff = [

                    "title" => $v['module_name'],

                    "field" => "module_".$v['module_id'],

                    "value" => []

                ];

                foreach ($v['module_field_list'] as $field) {

                    array_push($ff['value'],[

                        "name" => $field['name'],

                        "value" => $field['id']

                    ]);

                }

                if(empty($ff['value'])) continue;

                array_push($result,$ff);

            }

            $this->json_result($result);

        }

    }



    public function immediatelyInput(Request $request) {

        if($request->isPost()) {

            try{

                requestAccess($request,1);

                if($this->user_info['progress'] != UserObj::PROGRESS_OF_PERFECT) throw new \Exception("请勿重复进件");

                $module_field_data = [];

                foreach ($request->post() as $k=>$v) {

                    if(strstr($k,"module_")) {

                        $module_id = str_replace("module_","",$k);

                        if(empty($v)) {

                            $module_name = Module::where([

                                ['id','eq',$module_id]

                            ])->value("name");

                            throw new \Exception("请选择".$module_name);

                            break;

                        }

                        array_push($module_field_data,$module_id.":".$v);

                    }

                }

                $module_field_data_ = [];

                if(!empty($this->user_info['module_field_data'])) array_push($module_field_data_,$this->user_info['module_field_data']);

                if(!empty($module_field_data)) array_push($module_field_data_,implode('|',$module_field_data));

                $data = [

                    "module_field_data" => implode('|',$module_field_data_)

                ];

                $respon = StickPlan::parseModuleField(implode('|',$module_field_data));

                $userModuleData = [];

                foreach ($respon as $v) {

                    array_push($userModuleData,[

                        "user_id" => $this->user_id,

                        "module_id" => $v['module_id'],

                        "module_name" => $v['module_name'],

                        "module_field_id" => implode(',',array_column($v['module_field_list'],"id")),

                        "module_field_name" => implode(',',array_column($v['module_field_list'],"name")),

                    ]);

                }

                if(!empty($userModuleData)) UserAModule::insertAll($userModuleData);

                $userObj = new UserObj();

                $data = array_merge($data,[

                    "progress" => UserObj::PROGRESS_OF_INPUT,

                    "input_time" => time()

                ]);

                $userObj->where('id',$this->user_id)->update($data);

                $dis_result = $this->distribution($this->user_id);

            }catch (\Exception $e) {

                $this->json_error($e->getMessage());

            }

            $this->json_result([
                
                "is_input" => $dis_result,
                
                "text" => "您已进件成功，请电话保持畅通，审核人员可能会电话核实"

            ]);

        }else{

            $regionSettingInfo = RegionSetting::where([

                ['region_id','eq',$this->user_info['city_region_id']],

                ['is_del','eq',RegionSetting::NO_DEL]

            ])->find();

            if(empty($regionSettingInfo)) {

                $defaultRegionSettingInfo = RegionSetting::where([

                    ['is_default','eq',1],

                    ['is_del','eq',RegionSetting::NO_DEL]

                ])->value("module_field_data")->find();

                if(empty($defaultRegionSettingInfo)) throw new \Exception("暂无默认城市模板");

                $respon = StickPlan::parseModuleField($defaultRegionSettingInfo['module_field_data'],0);

            }else{

                $respon = StickPlan::parseModuleField($regionSettingInfo['module_field_data'],0);

            }

            $result = [];

            foreach ($respon as $v) {

                $ff = [

                    "title" => $v['module_name'],

                    "field" => "module_".$v['module_id'],

                    "value" => []

                ];

                foreach ($v['module_field_list'] as $field) {

                    array_push($ff['value'],[

                        "name" => $field['name'],

                        "value" => $field['id']

                    ]);

                }

                if(empty($ff['value'])) continue;

                array_push($result,$ff);

            }

            $this->json_result($result);

        }

    }





    public function sendMessage(Request $request)
    {

        try {

            $mobile = $request->post('mobile/s', '');

            $type = $request->post('type/s', 'check');

            if (empty($mobile)) throw new \Exception("手机号验证有误");

            switch ($type) {

                case 'check':

                    Message::getInstance()->sendSMSCode($mobile, 10, "check");

                    break;

                default:

                    throw new \Exception("类型有误");

                    break;

            }

        } catch (\Exception $e) {

            $this->json_error($e->getMessage());

        }

        $this->json_result(true, "发送成功");

    }



    public function aa() {

        $user_info = UserObj::where('id',10)->find();

        $module_field_data = empty($user_info['module_field_data']) ? [] : explode('|',$user_info['module_field_data']);

        $all_num = count($module_field_data);

        $stick_plan_list = StickPlanModule::alias("spm")->join([

            ['stick_plan sp','spm.stick_plan_id = sp.id','left']

        ])->where([

            ['sp.region_id','eq',$user_info['city_region_id']],

            ['sp.input_type','eq',$user_info['input_type']],

            ['sp.status','eq',StickPlan::STICK_PLAN_STATUS_OF_PAYMENT_SUCCESS],

            ['spm.is_del','eq',StickPlanModule::NO_DEL],

            ['sp.is_del','eq',StickPlan::NO_DEL],

        ])->field("sp.id,spm.module_field_id,spm.module_id")->select();

        $stick_plan_list = $stick_plan_list->isEmpty() ? [] : $stick_plan_list->toArray();

        $temp_array = $array = $stick_plan_ids = [];

        foreach ($module_field_data as $k=>$v) {

            $temp = explode(':',$v);

            $module_id = isset($temp[0]) ? $temp[0] : 0;

            $module_field_id = isset($temp[1]) ? $temp[1] : 0;

            foreach ($stick_plan_list as $vv) {

                if($vv['module_id'] != $module_id) continue;

                $module_field_id_array = empty($vv['module_field_id']) ? [] : explode(',',$vv['module_field_id']);

                if(!empty($module_field_id_array) && !in_array($module_field_id,$module_field_id_array)) continue;

                if(in_array($vv['id'],$temp_array)) {

                    $key = array_search($vv['id'],$temp_array);

                    $array[$key]['num_'] += 1;

                    $array[$key]['ratio_'] = $array[$key]['num_'] / $all_num;

                    if($array[$key]['ratio_'] >= 1) array_push($stick_plan_ids,$vv['id']);

                }else{

                    array_push($temp_array,$vv['id']);

                    array_push($array,array_merge($vv,[

                        "num_" => 1,

                        "ratio_" => 1/$all_num

                    ]));

                }

            }

        }

        $stickPlanList = StickPlan::where([

            ['region_id','eq',$user_info['city_region_id']],

            ['id','IN',$stick_plan_ids],

            ['input_type','eq',$user_info['input_type']],

            ['status','eq',StickPlan::STICK_PLAN_STATUS_OF_PAYMENT_SUCCESS],

            ['is_del','eq',StickPlan::NO_DEL]

        ])->order("bidding_price DESC")->select();

        if($stickPlanList->isEmpty()) throw new \Exception("无置顶计划符合要求");

        dump($stickPlanList);die;

    }




    private function distribution($user_id) {

        try{

            $user_info = UserObj::where([

                ['id','eq',$user_id]

            ])->find();

            if(empty($user_info)) return false;

            $regionSettingInfo = RegionSetting::where([

                ['region_id','eq',$user_info['city_region_id']],

                ['is_del','eq',RegionSetting::NO_DEL]

            ])->find();

            if(empty($regionSettingInfo)) throw new \Exception("该区域未配置");

            ###################################################################################

            $module_field_data = empty($user_info['module_field_data']) ? [] : explode('|',$user_info['module_field_data']);

            $all_num = count($module_field_data);

            $stick_plan_list = StickPlanModule::alias("spm")->join([

                ['stick_plan sp','spm.stick_plan_id = sp.id','left'],

                ['offline_shop os','sp.offline_shop_id = os.id','left'],

            ])->where([

                ['sp.region_id','eq',$user_info['city_region_id']],

                ['sp.input_type','eq',$user_info['input_type']],

                ['sp.status','eq',StickPlan::STICK_PLAN_STATUS_OF_PAYMENT_SUCCESS],

                ['spm.is_del','eq',StickPlanModule::NO_DEL],

                ['sp.is_del','eq',StickPlan::NO_DEL],

            ])->field("sp.id,spm.module_field_id,spm.module_id,os.mode")->select();

            $stick_plan_list = $stick_plan_list->isEmpty() ? [] : $stick_plan_list->toArray();

            $temp_array = $array = $stick_plan_ids = $zhi_stick_plan_ids = [];

            foreach ($module_field_data as $k=>$v) {

                $temp = explode(':',$v);

                $module_id = isset($temp[0]) ? $temp[0] : 0;

                $module_field_id = isset($temp[1]) ? $temp[1] : 0;

                foreach ($stick_plan_list as $vv) {

                    if($vv['module_id'] != $module_id) continue;

                    $module_field_id_array = empty($vv['module_field_id']) ? [] : explode(',',$vv['module_field_id']);

                    if(!empty($module_field_id_array) && !in_array($module_field_id,$module_field_id_array)) continue;

                    if(in_array($vv['id'],$temp_array)) {

                        $key = array_search($vv['id'],$temp_array);

                        $array[$key]['num_'] += 1;

                        $array[$key]['ratio_'] = $array[$key]['num_'] / $all_num;

                        if($array[$key]['ratio_'] >= 1) {

                            if($vv['mode'] == OfflineShop::OFFLINESHOP_MODE_OF_SELF_SUPPORT) {

                                array_push($zhi_stick_plan_ids,$vv['id']);

                            }elseif ($vv['mode'] == OfflineShop::OFFLINESHOP_MODE_OF_SELF_POPULARIZE) {

                                array_push($stick_plan_ids,$vv['id']);

                            }

                        }

                    }else{

                        array_push($temp_array,$vv['id']);

                        array_push($array,array_merge($vv,[

                            "num_" => 1,

                            "ratio_" => 1/$all_num

                        ]));

                    }

                }

            }

            $stickPlanList = StickPlan::where([

                ['id','IN',$stick_plan_ids],

            ])->order("bidding_price DESC")->select();

            if($stickPlanList->isEmpty()) throw new \Exception("无置顶计划符合要求");

            ####################################################################################

            $stickPlanList = $stickPlanList->toArray();

            $stickPlanNum = count($stickPlanList);

            if($stickPlanNum == 1 && isset($stickPlanList[0]['id'])) {

                $this->offlineShopWithUser($stickPlanList[0]['id'],$user_id);

            }else{

                switch ($regionSettingInfo['distribution_type']) {

                    //按量分配
                    case RegionSetting::DISTRIBUTION_TYPE_OF_NUM:

                        $max_distribution_num = floatval($regionSettingInfo['max_distribution_num']);

                        if($max_distribution_num <= 0) throw new \Exception("最大分配数小于或等于0");

                        foreach ($stickPlanList as $k=>$v) {

                            if($k >= $max_distribution_num) continue;

                            $this->offlineShopWithUser($v['id'],$user_id);

                        }

                        break;

                    //概率分配
                    case RegionSetting::DISTRIBUTION_TYPE_OF_CHANCE:

                        $ranking_data = empty($regionSettingInfo['ranking_data']) ? [] : explode(',',$regionSettingInfo['ranking_data']);

                        if(empty($ranking_data)) throw new \Exception("区域未设置排名数据");

                        $stickPlanArray = [];

                        foreach ($stickPlanList as $k=>$v) {

                            if(!isset($ranking_data[$k])) continue;

                            $temp = $v;

                            $temp['probability'] = $ranking_data[$k];

                            array_push($stickPlanArray,$temp);

                        }

                        $result = probabilityCalculation($stickPlanArray,"probability");

                        $this->offlineShopWithUser($result['id'],$user_id);

                        break;

                }

            }

            $this->selfSupport($zhi_stick_plan_ids,$user_id);

        }catch (\Exception $e) {

//            throw new \Exception($e->getMessage());

            return false;

        }
        
        return true;

    }


    private function offlineShopWithUser($stick_plan_id,$user_id) {

        try{

            $stickPlanInfo = StickPlan::where([

                ['id','eq',$stick_plan_id],

                ['status','eq',StickPlan::STICK_PLAN_STATUS_OF_PAYMENT_SUCCESS],

                ['is_del','eq',StickPlan::NO_DEL]

            ])->find();

            if(empty($stickPlanInfo)) throw new \Exception("置顶计划【{$stick_plan_id}】不存在");

            $userInfo = UserObj::where([

                ['id','eq',$user_id],

            ])->find();

            if(empty($userInfo)) throw new \Exception("用户信息【{$user_id}】不存在");

            $offlineShopInfo = OfflineShop::where([

                ['id','eq',$stickPlanInfo['offline_shop_id']],

                ['is_del','eq',OfflineShop::NO_DEL],

                ['status','eq',OfflineShop::STATUS_NORMAL]

            ])->find();

            if(empty($offlineShopInfo)) throw new \Exception("线下店信息【{$stickPlanInfo['offline_shop_id']}】不存在");

            if($offlineShopInfo['money'] < $stickPlanInfo['bidding_price']) throw new \Exception("线下店【{$offlineShopInfo['name']}】余额不足");

            $is_has_user = OfflineShopUser::where([

                ['offline_shop_id','eq',$offlineShopInfo['id']],

                ['user_id','eq',$user_id]

            ])->find();

            if(!empty($is_has_user)) throw new \Exception("该线下店【{$offlineShopInfo['name']}】已分配该用户【{$userInfo['mobile']}】");

            OfflineShop::where([

                ['id','eq',$offlineShopInfo['id']]

            ])->update([

                "money" => ['dec',$stickPlanInfo['bidding_price']]

            ]);

            OfflineShopMoneyLog::insertGetId([

                "title" => "分配用户【".substr_replace($userInfo['mobile'],"******",3,6)."】",

                "offline_shop_id" => $offlineShopInfo['id'],

                "money" => $stickPlanInfo['bidding_price'],

                "type" => 2,

                "create_time" => time()

            ]);

            OfflineShopUser::insertGetId([

                "offline_shop_id" => $stickPlanInfo['offline_shop_id'],

                "user_id" => $user_id,

                'consume_price' => $stickPlanInfo['bidding_price'],

                "create_time" => time()

            ]);

            StickPlan::updateStatusBalanceNotEnough($stickPlanInfo['id']);

        }catch (\Exception $e) {

            return false;

        }

    }



    private function selfSupport($stick_plan_ids = [],$user_id = 0) {

        if(empty($stick_plan_ids) || empty($user_id)) return false;

        $offline_shop_ids = OfflineShop::alias("os")->join([

            ['stick_plan sp','os.id = sp.offline_shop_id','left']

        ])->where([

            ['sp.id','in',$stick_plan_ids],

            ['os.mode','eq',OfflineShop::OFFLINESHOP_MODE_OF_SELF_SUPPORT],

            ['os.is_del','eq',OfflineShop::NO_DEL],

            ['os.status','eq',OfflineShop::STATUS_NORMAL]

        ])->column("os.id");

        foreach ($offline_shop_ids as $v) {

            OfflineShopUser::insertGetId([

                "offline_shop_id" => $v,

                "user_id" => $user_id,

                "create_time" => time()

            ]);

        }

        return true;

    }




}