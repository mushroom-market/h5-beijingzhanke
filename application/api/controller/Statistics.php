<?php

namespace app\api\controller;

use app\common\model\ChannelInviteCode;
use app\common\model\PageStatistics;
use think\Request;

class Statistics extends Base {


    public function loginPageStatistics(Request $request) {

        try{

            $this->statistics($request);

        }catch (\Exception $e) {

            $this->json_error($e->getMessage());

        }

        $this->json_result(true,"请求成功");

    }


    public function pageStatistics(Request $request) {

        try{

            $this->statistics($request);

        }catch (\Exception $e) {

            $this->json_error($e->getMessage());

        }

        $this->json_result(true,"请求成功");

    }



    private function statistics(Request $request) {

        try{

            $pageList = [

                "register" => PageStatistics::PAGE_OF_REGISTER,  //注册页面

                "perfect" => PageStatistics::PAGE_OF_PERFECT,  //完善信息页面

                "input" => PageStatistics::PAGE_OF_INPUT,  //进件页面

                "index" => PageStatistics::PAGE_OF_INDEX,  //首页

//                "product_detail" => PageStatistics::PAGE_OF_PRODUCT_DETAIL,  //产品详情页

            ];

            requestAccess($request,1);

            $page_name = $request->get('page_name/s','');

            if(!isset($pageList[$page_name])) throw new \Exception("参数有误");

            $invite_code = $request->get('invite_code/s','');

            $data = [

                "user_id" => $this->user_id,

                "page" => $pageList[$page_name],

                "ip" => $request->ip(),

                "create_time" => time()

            ];

            if($data['page'] == PageStatistics::PAGE_OF_REGISTER && !empty($invite_code)) {

                $invite_code_is_success = ChannelInviteCode::where([

                    ['code','eq',$invite_code],

                    ['is_del','eq',ChannelInviteCode::NO_DEL]

                ])->find();

                if(!empty($invite_code_is_success)) {

                    ChannelInviteCode::where('id',$invite_code_is_success['id'])->update([

                        "visit_num" => ['inc',1],

                    ]);

                    $data['channel_id'] = $invite_code_is_success['channel_id'];

                    $data['invite_code_id'] = $invite_code_is_success['id'];

                }

            }

            (new PageStatistics())->insertGetId($data);

        }catch (\Exception $e) {

            throw new \Exception($e->getMessage());

        }

    }

}