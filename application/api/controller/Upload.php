<?php

namespace app\api\controller;

use think\Request;

class Upload extends Base{


    /**
     * showdoc
     * @catalog 文件上传
     * @title 上传图片
     * @description 上传图片的接口
     * @method post
     * @url https://www.xxx.com/api/upload/uploadImg
     * @param img 必填 file 图片
     * @return {"msg":"操作成功","retval":{"src":"\/uploads\/images\/20191210\/cc7546fff69a8897e1929186ed7b66c1.jpg"},"code":1000,"done":true}
     * @remark 
     * @number 99
     */



    public function uploadImg(Request $request) {

        $array = [

            //文件类型 0为图片 1为视频 2为文件
            "file_type" => $request->post('file_type/d', 0),

            //是否上传至OSS 1为上传
            "is_oss" => $request->post('is_oss/d', 0),

            "file" => $request->file("_img_"),

            "dir" => $request->post('dir',"images/"),

            "size" => $request->post('size',5),

            "ext" => $request->post('ext',"jpeg,jpg,png,gif"),

        ];

        $result = (new \app\admin\controller\Upload())->upload($array);

        if($result['code'] != 200) {

            $this->json_error($result['msg']);

        }

        $this->json_result(['src'=> $result['data']['src']]);

    }


}