<?php
// 应用公共文件
use think\facade\Cache;
use think\Request;

/**
 * 数组转对象
 * @param $array
 * @return StdClass
 */
function array2object($array)
{
    if (!is_array($array)) return $array;
    $obj = new StdClass();
    foreach ($array as $key => $val) {
        $obj->$key = $val;
    }
    return $obj;

}


/**对象转数组
 * @param $object
 * @return array
 */
function object2array($object)
{
    if (!is_object($object)) return $object;
    $array = [];
    foreach ($object as $key => $value) {
        $array[$key] = $value;
    }
    return $array;
}

/**
 * 文本区域转换
 * @param  $str
 * @return mixed
 */
function areatext2html($str)
{
    $str = htmlspecialchars($str);
    $str = str_replace("\r\n", "<br>", $str);
    $str = str_replace("\r", "<br>", $str);
    $str = str_replace("\n", "<br>", $str);
    return $str;
}

/**
 * 还原文本区域转换
 * @param  $str
 * @return mixed
 */
function html2areatext($str)
{
    $str = str_replace("<br>", "\r\n", $str);
    $str = htmlspecialchars_decode($str);
    return $str;
}


/**
 * 时间格式化
 * @param int $time 时间
 * @param string $type 格式类别
 * @return bool|string
 */
function formatTime($time, $type = 's')
{
    $time = intval($time);
    if ($time < 9999999) return '';
    switch ($type) {
        case 'i':
            $timestr = date('Y-m-d H:i', $time);
            break;
        case 'h':
            $timestr = date('Y-m-d H', $time);
            break;
        case 'd':
            $timestr = date('Y-m-d', $time);
            break;
        case 'm':
            $timestr = date('Y-m', $time);
            break;
        case 'y':
            $timestr = date('Y', $time);
            break;
        case 's':
        default;
            $timestr = date('Y-m-d H:i:s', $time);
            break;
    }
    return $timestr;
}


/**
 * 数字格式化
 * @param $float
 * @param int $num
 * @return string
 */
function formatNumber($float, $num = 2)
{
    return number_format(floatval($float), $num, '.', '');
}


/**
 * 格式化分类为树形结构
 * @param $items
 * @param string $pid
 * @param string $cid
 * @return array
 */
function getTree($items, $pid = "pid",$cid="id")
{

    $map = $tree = [];

    foreach ($items as &$it) {

        $map[$it[$cid]] = &$it;

    }

    foreach ($items as &$at) {

        $parent = &$map[$at[$pid]];

        if ($parent) {

            $parent['children'][] = &$at;

        } else {

            $tree[] = &$at;

        }
    }

    return $tree;

}


/**
 * curl访问
 * @param string $url
 * @param string $type
 * @param boolean $data
 * @param string $err_msg
 * @param int $timeout
 * @param array $cert_info
 * @return string
 */
function goCurl($url, $type, $data = false, &$err_msg = null, $timeout = 20, $cert_info = array())
{
    $type = strtoupper($type);
    if ($type == 'GET' && is_array($data)) {
        $data = http_build_query($data);
    }
    $option = array();
    if ($type == 'POST') {
        $option[CURLOPT_POST] = 1;
    }
    if ($data) {
        if ($type == 'POST') {
            $option[CURLOPT_POSTFIELDS] = $data;
        } elseif ($type == 'GET') {
            $url = strpos($url, '?') !== false ? $url . '&' . $data : $url . '?' . $data;
        }
    }
    $option[CURLOPT_URL] = $url;
    $option[CURLOPT_FOLLOWLOCATION] = TRUE;
    $option[CURLOPT_MAXREDIRS] = 4;
    $option[CURLOPT_RETURNTRANSFER] = TRUE;
    $option[CURLOPT_TIMEOUT] = $timeout;
    //设置证书信息
    if (!empty($cert_info) && !empty($cert_info['cert_file'])) {
        $option[CURLOPT_SSLCERT] = $cert_info['cert_file'];
        $option[CURLOPT_SSLCERTPASSWD] = $cert_info['cert_pass'];
        $option[CURLOPT_SSLCERTTYPE] = $cert_info['cert_type'];
    }
    //设置CA
    if (!empty($cert_info['ca_file'])) {
        // 对认证证书来源的检查，0表示阻止对证书的合法性的检查。1需要设置CURLOPT_CAINFO
        $option[CURLOPT_SSL_VERIFYPEER] = 1;
        $option[CURLOPT_CAINFO] = $cert_info['ca_file'];
    } else {
        // 对认证证书来源的检查，0表示阻止对证书的合法性的检查。1需要设置CURLOPT_CAINFO
        $option[CURLOPT_SSL_VERIFYPEER] = 0;
    }
    $ch = curl_init();
    curl_setopt_array($ch, $option);
    $response = curl_exec($ch);
    $curl_no = curl_errno($ch);
    $curl_err = curl_error($ch);
    curl_close($ch);
    if ($curl_no > 0 && $err_msg !== null) $err_msg = '(' . $curl_no . ')' . $curl_err;
    return $response;
}

/**
 * 生成GUID
 * @return string
 */
function guid()
{
    if (function_exists('com_create_guid')) return com_create_guid();
    mt_srand((double)microtime() * 10000);//optional for php 4.2.0 and up.
    $charid = strtoupper(md5(uniqid(rand(), true)));
    $hyphen = chr(45);// "-"
    $uuid = chr(123)// "{"
        . substr($charid, 0, 8) . $hyphen
        . substr($charid, 8, 4) . $hyphen
        . substr($charid, 12, 4) . $hyphen
        . substr($charid, 16, 4) . $hyphen
        . substr($charid, 20, 12)
        . chr(125);// "}"
    return $uuid;
}


/**
 * 随机字符
 * @param int $length 长度
 * @param string $type 类型
 * @return string
 */
function random($length = 10, $type = 'letter')
{
    $config = [
        'number' => '1234567890',
        'letter' => 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
        'string' => 'abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ23456789',
        'all' => 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
    ];

    if (!isset($config[$type])) $type = 'letter';
    $string = $config[$type];
    $code = '';
    $strlen = strlen($string) - 1;
    for ($i = 0; $i < $length; $i++) {
        //字符串中的随机字符
        $code .= $string[mt_rand(0, $strlen)];
    }
    return $code;
}

/**
 * 生成二维码
 * @param string $url url连接
 * @param array $options 尺寸 纯数字
 * @return string
 */
function qrcode($url, $options = [])
{    //生成二维码
    include_once(\think\facade\Env::get('root_path')."/vendor/phpqrcode/phpqrcode.php");
    // 如果没有http 则添加
    if (strpos($url, 'http') === false) $url = 'http://' . $url;
    //生成的二维码大小,默认4
    $size = isset($options['size']) ? intval($options['size']) : 8;
    //是否保存二维码，默认不保存
    $create = isset($options['create']) ? intval($options['create']) : 0;
    $object = new \QRcode();
    if ($create == 0) {
        ob_end_clean();
        $object->png($url, false, QR_ECLEVEL_L, $size, 2, false, 0xFFFFFF, 0x000000);
    } else {
        //保存路径
        $path = isset($options['path']) ? $options['path'] : 'uploads/qrcode/' . date('Ymd');
        if (!is_dir($path)) {
            if (!mkdir($path, 0777, true)) return false;
        }
        // 生成的文件名
        $fileName = isset($options['name']) ? $path . '/' . $options['name'] . '.png' : $path . '/' . random() . '.png';
        //当文件不存在时，生成
        if (!is_file($fileName)) $object->png($url, $fileName, QR_ECLEVEL_L, $size, 2, false, 0xFFFFFF, 0x000000);

        return "/".$fileName;

    }
}


/**
 * 字符截取
 * @param string $string 需要截取的字符串
 * @param int $length 长度
 * @param string $dot
 */
function strCut($sourcestr, $length, $dot = '...')
{
    if (empty($sourcestr)) return '';
    $returnstr = '';
    $i = 0;
    $n = 0;
    $str_length = strlen($sourcestr); // 字符串的字节数
    while (($n < $length) && ($i <= $str_length)) {
        $temp_str = substr($sourcestr, $i, 1);
        $ascnum = Ord($temp_str); // 得到字符串中第$i位字符的ascii码
        if ($ascnum >= 224) { // 如果ASCII位高与224，
            $returnstr = $returnstr . substr($sourcestr, $i, 3); // 根据UTF-8编码规范，将3个连续的字符计为单个字符
            $i = $i + 3; // 实际Byte计为3
            $n++; // 字串长度计1
        } elseif ($ascnum >= 192) { // 如果ASCII位高与192，
            $returnstr = $returnstr . substr($sourcestr, $i, 2); // 根据UTF-8编码规范，将2个连续的字符计为单个字符
            $i = $i + 2; // 实际Byte计为2
            $n++; // 字串长度计1
        } elseif ($ascnum >= 65 && $ascnum <= 90) { // 如果是大写字母，
            $returnstr = $returnstr . substr($sourcestr, $i, 1);
            $i = $i + 1; // 实际的Byte数仍计1个
            $n++; // 但考虑整体美观，大写字母计成一个高位字符
        } else { // 其他情况下，包括小写字母和半角标点符号，
            $returnstr = $returnstr . substr($sourcestr, $i, 1);
            $i = $i + 1; // 实际的Byte数计1个
            $n = $n + 0.5; // 小写字母和半角标点等与半个高位字符宽...
        }
    }
    if ($str_length > strlen($returnstr)) $returnstr = $returnstr . $dot; // 超过长度时在尾处加上省略号
    return $returnstr;
}

/**
 * 把表中的数据生成缓存
 * @param string $name 缓存名称(表名称)
 * @param string $value 缓存值
 * @param array $options 缓存参数
 * @return mixed
 */
function createCache($name, $value = '', $options = [])
{
    if (empty($value)) {
        // 读取缓存
        $data = cache($name);
        // 如果缓存为空，则重新生成缓存
        if (empty($data)) {
            $order = isset($options['order']) ? $options['order'] : '';
            $where = isset($options['where']) ? $options['where'] : [];
            if (empty($order)) {
                $list = \think\Db::name($name)->where($where)->select();//查询数据
            } else {
                $list = \think\Db::name($name)->where($where)->order($order)->select();//查询数据
            }
            $data = [];
            if (!empty($list)) {
                //查询主键
                $table_name = config('database.prefix') . $name;
                $pks = \think\Db::query("select COLUMN_KEY,COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where table_name='$table_name' AND COLUMN_KEY='PRI'");
                $pk = $pks[0]['COLUMN_NAME'];
                foreach ($list as $item) {
                    $data[$item[$pk]] = $item;
                }
                unset($list, $table_name, $pks, $pk);
            }
            cache($name, $data);//生成缓存
        }
        return $data;
        //当没有value值时表示读取数据，并且需要返回
    } else {
        cache($name, $value);//生成缓存
    }
}

/**
 * 所有用到的密码加密方式
 * @param string $password 密码
 * @param string $password_code 密码额外加密字符
 * @return string
 */
function Password($password, $password_code)
{
    return md5(md5($password) . md5($password_code));
}

/**
 * 是否微信访问
 * @return bool
 */
function isWeixin()
{
    return strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false;
}

/**
 * 解密
 * @param string $encryptedText 已加密字符串
 * @param string $key 密钥
 * @return string
 */
function KeyDecode($encryptedText, $key = null)
{
    $key = $key === null ? config('encryption_key') : $key;
    $cryptText = base64_decode($encryptedText);
    $ivSize = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
    $iv = mcrypt_create_iv($ivSize, MCRYPT_RAND);
    $decryptText = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $cryptText, MCRYPT_MODE_ECB, $iv);
    return trim($decryptText);
}

/**
 * 加密
 * @param string $plainText 未加密字符串
 * @param string $key 密钥
 */
function KeyEncode($plainText, $key = null)
{
    $key = $key === null ? config('encryption_key') : $key;
    $ivSize = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
    $iv = mcrypt_create_iv($ivSize, MCRYPT_RAND);
    $encryptText = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $plainText, MCRYPT_MODE_ECB, $iv);
    return trim(base64_encode($encryptText));
}


/**
 * 加密函数
 * @param string $txt 需加密的字符串
 * @param string $key 加密密钥，默认读取encryption_key配置
 * @return string 加密后的字符串
 */
function keyEncode2($txt, $key = null)
{
    empty($key) && $key = config('encryption_key');
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-=_";
    $nh = rand(0, 64);
    $ch = $chars[$nh];
    $mdKey = md5($key . $ch);
    $mdKey = substr($mdKey, $nh % 8, $nh % 8 + 7);
    $txt = base64_encode($txt);
    $tmp = '';
    $k = 0;
    for ($i = 0; $i < strlen($txt); $i++) {
        $k = $k == strlen($mdKey) ? 0 : $k;
        $j = ($nh + strpos($chars, $txt [$i]) + ord($mdKey[$k++])) % 64;
        $tmp .= $chars[$j];
    }
    return $ch . $tmp;
}


/**
 * 解密函数
 * @param string $txt 待解密的字符串
 * @param string $key 解密密钥，默认读取encryption_key配置
 * @return string 解密后的字符串
 */
function keyDecode2($txt, $key = null)
{
    empty($key) && $key = config('encryption_key');
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-=_";
    $ch = $txt[0];
    $nh = strpos($chars, $ch);
    $mdKey = md5($key . $ch);
    $mdKey = substr($mdKey, $nh % 8, $nh % 8 + 7);
    $txt = substr($txt, 1);
    $tmp = '';
    $k = 0;
    for ($i = 0; $i < strlen($txt); $i++) {
        $k = $k == strlen($mdKey) ? 0 : $k;
        $j = strpos($chars, $txt[$i]) - $nh - ord($mdKey[$k++]);
        while ($j < 0) {
            $j += 64;
        }
        $tmp .= $chars[$j];
    }
    return base64_decode($tmp);
}

/**
 * 数组转换为JSON
 * @param array $array 数组
 * @return string
 */
function array2json($array = [])
{
    if (!is_array($array) || empty($array)) return '';
    return json_encode($array, JSON_UNESCAPED_UNICODE);
}

/**
 * JSON转换为数组
 * @param string $string JSON字符串
 * @return array|mixed
 */
function json2array($string = '')
{
    if (empty($string) || !is_string($string)) return [];
    preg_match('/{.*}/', $string, $json_arr);
    return empty($json_arr) ? [] : json_decode($string, true);
}


/**
 * emoji表情反转译
 * @param $str
 * @return mixed
 */
function emojiDecode($str)
{
    $strDecode = preg_replace_callback('|\[\[EMOJI:(.*?)\]\]|', function ($matches) {
        return rawurldecode($matches[1]);
    }, $str);
    return $strDecode;
}

/**
 * emoji表情转译
 * @param $str
 * @return string
 */
function emojiEncode($str)
{
    preg_match_all('/[\xf0-\xf7].{3}/', $str, $array);
    foreach ($array[0] as $item) {
        $str = str_replace($item, '[[EMOJI:' . rawurlencode($item) . ']]', $str);
    }
    return $str;
}

/**
 * 清除emoji表情
 * @param $str
 * @return string
 */
function emojiRemove($str)
{
    preg_match_all('/[\xf0-\xf7].{3}/', $str, $array);
    foreach ($array[0] as $item) {
        $str = str_replace($item, "", $str);
    }
    return $str;
}


/**
 * 获取配置信息
 * @param $name
 * @return bool|mixed
 */
function getConfig($name)
{
    if (empty($name)) return false;
    $cahce = createCache('config');
    $value = '';
    foreach ($cahce as $item) {
        if ($item['name'] == $name) $value = $item['values'];
    }
    return empty($value) ? false : $value;
}


/**
 * JS打印
 * @param $info
 */
function console($info)
{
    echo "<script> console.log('" . $info . "')</script>";
}


/**
 * 下载远程图片
 * @param string $url 图片地址
 * @param string $filename 保存的文件名（相对于网站根目录）
 * @param string $dir 保存的文件目录（相对于网站根目录）
 * @return bool
 */
function downloadImage($url, $filename,$dir)
{
    if (false === strpos($url, 'http')) return false;

    if(empty($filename)) {

        $url_array = explode('/',$url);

        $filename = end($url_array);

    }

    $filename = $dir."/".$filename;

    $ch = curl_init($url);

    curl_setopt($ch, CURLOPT_TIMEOUT, 6);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $content = curl_exec($ch);

    curl_close($ch);

    if (!$content) return false;

    $resource = fopen($filename, 'a+');

    fwrite($resource, $content);

    fclose($resource);

    return is_file($filename);

}


/**
 * URL base64解码
 * '-' -> '+'
 * '_' -> '/'
 * 字符串长度%4的余数，补'='
 * @param string $string
 */
function urlsafeBase64Decode($string)
{
    $data = str_replace(array('-', '_'), array('+', '/'), $string);
    $mod4 = strlen($data) % 4;
    if ($mod4) $data .= substr('====', $mod4);
    return base64_decode($data);
}

/**
 * URL base64编码
 * '+' -> '-'
 * '/' -> '_'
 * '=' -> ''
 * @param string $string
 */
function urlsafeBase64Encode($string)
{
    return str_replace(array('+', '/', '='), array('-', '_', ''), base64_encode($string));
}


/**
 * 获取远程图片的宽高和体积大小
 * @param string $url 远程图片的链接
 * @param string $type 获取远程图片资源的方式, 默认为 curl 可选 fread
 * @param boolean $isGetFilesize 是否获取远程图片的体积大小, 默认false不获取, 设置为 true 时 $type 将强制为 fread
 * @return false|array
 */
function getImageInfo($url, $type = 'curl', $isGetFilesize = false)
{
    // 若需要获取图片体积大小则默认使用 fread 方式
    $type = $isGetFilesize ? 'fread' : $type;
    if ($type == 'fread') {
        // 或者使用 socket 二进制方式读取, 需要获取图片体积大小最好使用此方法
        $handle = fopen($url, 'rb');
        if (!$handle) return false;
        // 只取头部固定长度168字节数据
        $dataBlock = fread($handle, 168);
    } else {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);// 超时设置
        curl_setopt($ch, CURLOPT_RANGE, '0-167');// 取前面 168 个字符 通过四张测试图读取宽高结果都没有问题,若获取不到数据可适当加大数值
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);// 跟踪301跳转
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);// 返回结果
        $dataBlock = curl_exec($ch);
        curl_close($ch);
        if (!$dataBlock) return false;
    }
    // 将读取的图片信息转化为图片路径并获取图片信息,经测试,这里的转化设置 jpeg 对获取png,gif的信息没有影响,无须分别设置
    // 有些图片虽然可以在浏览器查看但实际已被损坏可能无法解析信息
    $size = getimagesize('data://image/jpeg;base64,' . base64_encode($dataBlock));
    if (empty($size)) return false;
    $result['width'] = $size[0];
    $result['height'] = $size[1];
    $result['image_type'] = $size['mime'];
    // 是否获取图片体积大小
    if ($isGetFilesize) {
        $meta = stream_get_meta_data($handle);// 获取文件数据流信息
        // nginx 的信息保存在 headers 里，apache 则直接在 wrapper_data
        $dataInfo = isset($meta['wrapper_data']['headers']) ? $meta['wrapper_data']['headers'] : $meta['wrapper_data'];
        foreach ($dataInfo as $va) {
            if (preg_match('/length/iU', $va)) {
                $ts = explode(':', $va);
                $result['size'] = trim(array_pop($ts));
                break;
            }
        }
    }
    if ($type == 'fread') fclose($handle);
    return $result;
}


/**
 * 数组排序
 * @param $arr
 * @param $keys
 * @param string $type
 * @return array
 */
function array_sort($arr, $keys, $type = 'asc')
{
    $keysvalue = $new_array = array();
    foreach ($arr as $k => $v) {
        $keysvalue[$k] = $v[$keys];
    }
    if ($type == 'asc') {
        asort($keysvalue);
    } else {
        arsort($keysvalue);
    }
    reset($keysvalue);
    foreach ($keysvalue as $k => $v) {
        $new_array[$k] = $arr[$k];
    }
    return $new_array;
}


/**
 * 公共发送邮件函数
 * @param $data
 * @return bool|string
 */
function sendEmail($data)
{
    $content = isset($data['content']) && !empty($data['content']) ? $data['content'] : '';
    $toemail = isset($data['toemail']) && !empty($data['toemail']) ? $data['toemail'] : '';
    $title = isset($data['title']) && !empty($data['title']) ? $data['title'] : '';
    $frommail = isset($data['frommail']) && !empty($data['frommail']) ? $data['frommail'] : '';
    $smtp = isset($data['smtp']) && !empty($data['smtp']) ? $data['smtp'] : '';
    $password = isset($data['password']) && !empty($data['password']) ? $data['password'] : '';
    $mail = new PHPMailer(true);
    try {
        // 服务器设置
//        $mail->SMTPDebug = 0;                                      // 开启Debug
        $mail->isSMTP();                                             // 使用SMTP
        $mail->CharSet = 'UTF-8';
        $mail->Host = $smtp;                                         // 服务器地址
        $mail->SMTPAuth = true;                                      // 开启SMTP验证
        $mail->Username = $frommail;                                     // SMTP 用户名（你要使用的邮件发送账号）
        $mail->Password = $password;                                 // SMTP 密码
        $mail->SMTPSecure = 'ssl';                                   // 开启SSL 可选
        $mail->Port = 465;                                           // 端口
        // 收件人
        $mail->setFrom($frommail, config('web_title'));        // 来自
        $mail->addAddress($toemail);                                 // 可以只传邮箱地址
        $mail->addReplyTo($frommail, config('web_title'));    // 回复地址
        // 内容
        $mail->isHTML(true);                                  // 设置邮件格式为HTML
        $mail->Subject = $title;
        $mail->Body = $content;
        $mail->send();
        return true;
    } catch (Exception $e) {
        return $mail->ErrorInfo;
    }

}

/**
 * 尺寸转换
 */

//size大小转为kmg格式
function changeSize($size = 0)
{
    $size = intval($size);
    if ($size < 1024) return $size . "B";
    $kb = round($size / 1024, 2);
    if ($kb < 1024) return $kb . "K";
    $mb = round($size / 1024 / 1024, 2);
    if ($mb < 1024) return $mb . "M";
    $gb = round($size / 1024 / 1024 / 1024, 2);
    if ($gb < 1024) return $gb . "G";
    $tb = round($size / 1024 / 1024 / 1024 / 1024, 2);
    if ($tb < 1024) return $tb . "T";
    return $size;
}


/**
 * 判断是否为移动端
 */
function is_mobile()
{
    // 如果有HTTP_X_WAP_PROFILE则一定是移动设备
    if (isset ($_SERVER['HTTP_X_WAP_PROFILE']))
        return true;

    //此条摘自TPM智能切换模板引擎，适合TPM开发
    if (isset ($_SERVER['HTTP_CLIENT']) && 'PhoneClient' == $_SERVER['HTTP_CLIENT'])
        return true;
    //如果via信息含有wap则一定是移动设备,部分服务商会屏蔽该信息
    if (isset ($_SERVER['HTTP_VIA']))
        //找不到为flase,否则为true
        return stristr($_SERVER['HTTP_VIA'], 'wap') ? true : false;
    //判断手机发送的客户端标志,兼容性有待提高
    if (isset ($_SERVER['HTTP_USER_AGENT'])) {
        $clientkeywords = array(
            'nokia', 'sony', 'ericsson', 'mot', 'samsung', 'htc', 'sgh', 'lg', 'sharp', 'sie-', 'philips', 'panasonic', 'alcatel', 'lenovo', 'iphone', 'ipod', 'blackberry', 'meizu', 'android', 'netfront', 'symbian', 'ucweb', 'windowsce', 'palm', 'operamini', 'operamobi', 'openwave', 'nexusone', 'cldc', 'midp', 'wap', 'mobile'
        );
        //从HTTP_USER_AGENT中查找手机浏览器的关键字
        if (preg_match("/(" . implode('|', $clientkeywords) . ")/i", strtolower($_SERVER['HTTP_USER_AGENT']))) {
            return true;
        }
    }
    //协议法，因为有可能不准确，放到最后判断
    if (isset ($_SERVER['HTTP_ACCEPT'])) {
        // 如果只支持wml并且不支持html那一定是移动设备
        // 如果支持wml和html但是wml在html之前则是移动设备
        if ((strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') !== false) && (strpos($_SERVER['HTTP_ACCEPT'], 'text/html') === false || (strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') < strpos($_SERVER['HTTP_ACCEPT'], 'text/html')))) {
            return true;
        }
    }
    return false;
}


/**
 * 浮点数转为整数
 * @param $float
 * @return float
 */
function float2intval($float) {

    if(ceil($float) == $float) {

        return ceil($float);

    }

    if(round($float,1) == $float) {

        return round($float,1);

    }

    if(round($float,2) == $float) {

        return round($float,2);

    }

    return round($float,2);

}


/**
 * 清除目录
 * @param $dir
 * @return bool
 */
function deldir($dir) {
    //先删除目录下的文件：
    $dh=opendir($dir);
    while ($file=readdir($dh)) {
        if($file!="." && $file!="..") {
            $fullpath=$dir."/".$file;
            if(!is_dir($fullpath)) {
                unlink($fullpath);
            } else {
                deldir($fullpath);
            }
        }
    }

    closedir($dh);
    //删除当前文件夹：
    if(rmdir($dir)) {
        return true;
    } else {
        return false;
    }
}

/**
 * 获取某年某月的天数
 * @param $month
 * @param $year
 * @return int
 */
function days_in_month($month, $year) {
    return $month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year % 400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31);
}

/**
 * 获取经纬度与经纬度的距离
 * @param $lng1
 * @param $lat1
 * @param $lng2
 * @param $lat2
 * @return float
 */
function getdistance($lng1,$lat1,$lng2,$lat2)
{
    //将角度转为狐度
    $radLat1=deg2rad($lat1);
    $radLat2=deg2rad($lat2);
    $radLng1=deg2rad($lng1);
    $radLng2=deg2rad($lng2);
    $a=$radLat1-$radLat2;//两纬度之差,纬度<90
    $b=$radLng1-$radLng2;//两经度之差纬度<180
    $s=2*asin(sqrt(pow(sin($a/2),2)+cos($radLat1)*cos($radLat2)*pow(sin($b/2),2)))*6378.137;
    return $s;
}


function requestAccess(Request $request,$expire_time = 2) {

    $ip = md5($request->ip());

    $path = $request->path();

    $param = array2json($request->param());

    $cache_name = md5($ip.$path.$param);

    if(Cache::has($cache_name)) throw new \Exception("频繁请求");

    Cache::set($cache_name,1,$expire_time);

}


/**
 * 概率随机
 * @param array $array
 * @param string $probability
 * @return int|string
 */
function probabilityCalculation($array = [],$probability = "probability") {

    $result = new ArrayObject();

    //概率数组的总概率精度
    $sum = array_sum(array_column($array,$probability));

    //概率数组循环
    foreach ($array as $key => $val) {

        $randNum = mt_rand(1, $sum);

        if ($randNum <= $val[$probability]) {

            $result = $val;

            break;

        } else {

            $sum -= $val[$probability];

        }

    }

    unset ($array);

    return $result;

}