<?php

namespace app\common\controller;

use excels\ExcelExport;
use think\Controller;
use think\Exception;
use think\Request;

class ExcelDeal extends Controller{


    public function import(Request $request) {

        set_time_limit(0);

        ini_set("memory_limit","500M");

        try {

            $file = $request->file("excel");

            $dir = $request->post('dir/s','excel/');

            $size = $request->post('size/d',5);

            if (!$file) throw new \Exception("请上传Excel");

            $path = "uploads/";

            if (!empty($dir)) $path .= $dir;

            if (!is_dir($path)) mkdir($path);

            $res = $file->validate([

                'ext' => "xlsx",

                'size' => $size * 1024 * 1024

            ])->move($path);

            if (!$res) throw new \Exception($file->getError());

            $src = "/" . $path . $res->getSaveName();

            $src = str_replace("\\","/",$src);

            $path = ".".$src;

            $data = $this->excelDeal($path);

            $result = [

                "src" => $src,

                "path" => $path,

                "data" => $data,

            ];

        } catch (Exception $e) {

            throw new \Exception($e->getMessage(),$e->getCode());

        }

        return $result;

    }



    public function excelDeal($path = "") {

        include_once(\think\facade\Env::get('root_path')."/vendor/PHPExcel/Classes/PHPExcel/IOFactory.php");

        ob_end_clean();//清除缓冲区,避免乱码

        set_time_limit(0);

        $inputFileType = \PHPExcel_IOFactory::identify($path);

        $objReader = \PHPExcel_IOFactory::createReader($inputFileType);

        $objPHPExcel = $objReader->load($path);

        $sheet = $objPHPExcel->getActiveSheet();

        $highestRow = $sheet->getHighestRow();

        $highestColumn = $sheet->getHighestColumn();

        $data = [];

        for ($row = 1; $row <= $highestRow; $row++){

            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);

            array_push($data,$rowData[0]);

        }

        return $data;

    }



    public function export($array = []) {

        $data = isset($array['data']) ? $array['data'] : [];

        $path = isset($array['path']) ? $array['path'] : 'excel';

        $colAttr = isset($array['title']) ? $array['title'] : [];

//        $colAttr=array(
//            'A'=>array(//列的属性设置
//                'colName'=>'编号',//第一行的列名
//                'keyName'=>'id',//每一列对应的赋值数组的key值
//                'width'=>'20'//每一列的宽度
//            ),
//            'B'=>array(//列的属性设置
//                'colName'=>'喜欢谁',//第一行的列名
//                'keyName'=>'nick_name',//每一列对应的赋值数组的key值
//                'width'=>'20'//每一列的宽度
//            ),
//            'C'=>array(//列的属性设置
//                'colName'=>'爱谁',//第一行的列名
//                'keyName'=>'property_id',//每一列对应的赋值数组的key值
//                'width'=>'20'//每一列的宽度
//            )
//        );
        //行属性参数
        $rowAttr=array(
            'firstRowHeight'=>'20', //第一行的列名的高度
            'height'=>'40'//2-OO无从行的高度
        );
        //excel表的属性参数
        $options=array(
            'excelname'=>'你喜欢谁', //导出的excel的文件的名称
            'sheettitle'=>'',    //每个工作薄的标题
        );

        (new ExcelExport($data,$colAttr,$rowAttr,$options,$path))->push();


    }



}