<?php

namespace app\common\controller;

use OSS\OssClient;
use think\Controller;
use think\facade\Config;
use think\Request;

class Upload extends Controller
{

    public $img_name = "_img_";

    public $video_name = "video";

    public $file_name = "file";

    public $dir = "uploads/";


    public function index(Request $request) {

        $array = [

            //文件类型 0为图片 1为视频 2为文件
            "file_type" => $request->post('file_type/d', 0),

            //是否上传至OSS 1为上传
            "is_oss" => $request->post('is_oss/d', 0),

        ];

        switch ($array['file_type']) {

            //图片
            case 0:

                $array['file'] = $request->file($this->img_name);

                $array['dir'] = $request->post('dir',"images/");

                $array['size'] = $request->post('size',5);

                $array['ext'] = $request->post('ext',"jpeg,jpg,png,gif");

                break;

            //视频
            case 1:

                $array['file'] = $request->file($this->video_name);

                $array['dir'] = $request->post('dir','video/');

                $array['size'] = $request->post('size',20);

                $array['ext'] = $request->post('ext',"mp4,flv,avi");

                break;

            //文件
            case 2:

                $array['file'] = $request->file($this->file_name);

                $array['dir'] = $request->post('dir','file/');

                $array['size'] = $request->post('size',20);

                break;

        }

        $respon = $this->upload($array);

        if($respon['code'] == 201) {

            $this->error([

                "message" => $respon['message'],

                "data" => [

                    "src" => "",

                ]

            ]);

        }

        $this->success([

            "data" => $respon['data']

        ]);


    }


    public function upload($array = [])
    {

        try{

            $file_type = isset($array['file_type']) ? floatval($array['file_type']) : 0;

            $file = isset($array['file']) ? $array['file'] : "";

            $dir = isset($array['dir']) ? $array['dir'] : "";

            $size = isset($array['size']) ? floatval($array['size']) : 0;

            $ext = isset($array['ext']) ? $array['ext'] : "";

            $is_oss = isset($array['is_oss']) ? $array['is_oss'] : 0;

            if($file_type < 0 || $file_type > 2) throw new \Exception("上传类型有误");

            if(!$file || empty($file)) throw new \Exception("请先上传");

            if(empty($dir)) throw new \Exception("请选择上传目录");

            if(empty($size)) throw new \Exception("请设置上传大小限制");

            $path = $this->dir;

            if (!empty($dir)) $path .= $dir;

            if (!is_dir($path)) mkdir($path,"0777",true);

            $res = $file->validate([

                'size' => 1024 * 1024 * $size,

                'ext' => $ext

            ])->move($path);

            if (!$res) throw new \Exception($file->getError());

            $file_name = $res->getSaveName();

            $path_name = $res->getPathname();

            $src = "/" . $path . $file_name;

            $src = str_replace("\\","/",$src);

            $oss_src = "";

            if($is_oss == 1) {

                $oss_src = $this->uploadOssFile($file_name,$path_name);

            }

        }catch (\Exception $e) {

            return [

                "code" => 201,

                "message" => $e->getMessage(),

                "data" => new \ArrayObject()

            ];

        }

        return [

            "code" => 200,

            "message" => "上传成功",

            "data" => [

                "file_name" => $file_name,

                "file_path" => $path,

                "src" => $src,

                "full_src" => LOCAL_URL.$src,

                "oss_src" => $oss_src,

            ],

        ];




    }


    /**
     * 上传OSS文件
     * @param $file_name
     * @param $path
     * @return string
     */
    public function uploadOssFile($file_name,$path) {

        try{

            $url = "";

            $config = Config::get('oss.');

            $ossClient = new OssClient($config['accessKeyId'], $config['accessKeySecret'], $config['endpoint']);

            $respon = $ossClient->uploadFile($config['bucket'],$file_name,$path);

            if(isset($respon['info']['url']) && !empty($respon['info']['url'])) $url = $respon['info']['url'];

        }catch (\Exception $e) {

            return "";

        }

        return $url;

    }


    /**
     * 富文本编辑器上传图片
     * @param Request $request
     * @return string
     */
    public function uploadImageUe(Request $request)
    {

        $array = array(

            "state" => "SUCCESS",               //上传状态，上传成功时必须返回"SUCCESS"

            "url" => "",                        //CDN地址

            "title" => "",                      //新文件名

            "original" => "",                   //原始文件名

            "type" => "",                       //文件类型

            "size" => "",                       //文件大小

        );

        $file = $request->file('upfile');

        $img_info = $file->getInfo();

        $path = "uploads/";

        if (!is_dir($path)) mkdir($path,"0777",true);

        $res = $file->validate([

            'size' => 1024 * 1024 * 5,

            'ext' => "jpeg,jpg,png,gif"

        ])->move($path);

        if (!$res) $this->error("上传失败");

        $src = "/" . $path . $res->getSaveName();

        $src = str_replace("\\", "/", $src);

        $array['url'] = LOCAL_URL . $src;

        $array['size'] = $img_info['size'];

        $array['type'] = $img_info['type'];

        $array['title'] = $img_info['name'];

        return array2json($array);

    }


}