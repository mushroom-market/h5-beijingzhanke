<?php

namespace app\common\model;

/**
 * 轮播图
 * Class Advertisement
 * @package app\common\model
 */
class Advertisement extends Base {


    public function getAdvertisementList($location) {

        $data = $this->where([

            ['location','eq',$location],

            ['status','eq',self::STATUS_NORMAL],

            ['is_del','eq',self::NO_DEL]

        ])->order("id DESC")->select();

        $result = [];

        foreach ($data as $v) {

            array_push($result,[

                "img" => $v['img'],

                "link_type" => "",

                "link_value" => $v['link_value'],

            ]);

        }

        return $result;

    }


}