<?php

namespace app\common\model;


class Auth extends Base {

    /**
     * 未删除
     */
    const NO_DEL = 0;

    /**
     * 已删除
     */
    const YES_DEL = 1;

    /**
     * 显示的
     */
    const YES_SHOW = 0;

    /**
     * 不显示的
     */
    const NO_SHOW = 1;

}