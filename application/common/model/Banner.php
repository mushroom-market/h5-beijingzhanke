<?php

namespace app\common\model;


class Banner extends Base {

    const NO_DEL = 0;

    const YES_DEL = 1;

    const LOCATION = [

        'index'=>"首页",

        'recommend_buy'=>"推荐购买",

        'invite' => "邀请图"

    ];

    const BANNER_TYPE = [

        '' => '无类型',

        'coupon_detail' => '卡券详情',

        'coupon_list' => "卡券列表",

        'notice' => "通知公告",

        'question' => "常见问题",

    ];

    public function getImgAttr($value)
    {
        return str_replace("\\",'/',$value);
    }

    public function getParamAttr($value) {

        return empty($value) ? "" : $value;

    }

}