<?php

namespace app\common\model;

/**
 * 渠道商邀请码
 * Class ChannelInviteCode
 * @package app\common\model
 */
class ChannelInviteCode extends Base {


    public static function getCode() {

        $code = rand(100000,999999);

        $is_exists = self::where([

            ['code','eq',$code],

            ['is_del','eq',self::NO_DEL]

        ])->find();

        if(empty($is_exists)) return $code;

        return self::getCode();

    }

}