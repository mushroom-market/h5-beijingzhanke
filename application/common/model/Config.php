<?php

namespace app\common\model;


use think\facade\Cache;

class Config extends Base {

    /**
     * 已删除标识
     */
    const CONFIG_YES_DEL = 1;

    /**
     * 未删除标识
     */
    const CONFIG_NO_DEL = 0;

    /**
     * 缓存配置前缀
     */
    const CONFIG_PREFIX = "cache_config_";


    /**
     * 获取配置值
     * @param array $where
     * @return mixed
     */
    public static function getValue($where = []) {

        $where = array_merge($where,['is_del'=>self::CONFIG_NO_DEL]);

        return self::where($where)->value('value');

    }


    /**
     * 获取配置缓存中单条数据
     * @param string $key
     * @return array|mixed
     */
    public static function getCacheValue($key = '') {

        if(empty($key)) return [];

        $cache_name = self::CONFIG_PREFIX.$key;

        if(!Cache::has($cache_name)) self::setCacheValue($key);
        
        $cache_data = Cache::get($cache_name);
        
        $cache_data = empty($cache_data) ? [] : unserialize($cache_data);

        return $cache_data;

    }


    /**
     * 更新配置缓存中单个数据
     * @param string $key
     * @return bool
     */
    public static function setCacheValue($key = '') {

        if(empty($key)) return false;

        $cache_name = self::CONFIG_PREFIX.$key;

        $list = self::getValue(['key'=>$key]);

        Cache::set($cache_name,$list);

        return true;

    }

}