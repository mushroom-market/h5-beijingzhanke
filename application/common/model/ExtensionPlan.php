<?php

namespace app\common\model;

/**
 * 渠道推广计划
 * Class ExtensionPlan
 * @package app\common\model
 */
class ExtensionPlan extends Base {


    //跳转至登录
    const EXTENSION_PLAN_REDIRECT_TYPE_OF_LOGIN = 1;

    //跳转至产品首页
    const EXTENSION_PLAN_REDIRECT_TYPE_OF_PRODUCT = 2;

}