<?php

namespace app\common\model;

/**
 * 模块
 * Class Module
 * @package app\common\model
 */
class Module extends Base {



    public function hasManyModuleField() {

        return $this->hasMany("module_field",'module_id','id')->where([

            ['is_del','eq',ModuleField::NO_DEL],

        ])->order("sort DESC");

    }


}