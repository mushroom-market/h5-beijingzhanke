<?php

namespace app\common\model;

/**
 * 线下店
 * Class OfflineShop
 * @package app\common\model
 */
class OfflineShop extends Base {


    //自营
    const OFFLINESHOP_MODE_OF_SELF_SUPPORT = 1;

    //推广
    const OFFLINESHOP_MODE_OF_SELF_POPULARIZE = 2;


    public static function getModeText($mode,$key="",$val="") {

        $result = [

            self::OFFLINESHOP_MODE_OF_SELF_SUPPORT => "自营",

            self::OFFLINESHOP_MODE_OF_SELF_POPULARIZE => "推广",

        ];

        return self::getArrayText($result,$mode,$key,$val);

    }


}