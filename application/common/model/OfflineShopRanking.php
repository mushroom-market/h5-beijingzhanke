<?php

namespace app\common\model;

/**
 * 线下店排名
 * Class OfflineShopRanking
 * @package app\common\model
 */
class OfflineShopRanking extends Base {


    public static function updateRanking($array = []) {

        $region_id = isset($array['region_id']) ? $array['region_id'] : "";

        $input_type = isset($array['input_type']) ? $array['input_type'] : "";

        if(empty($region_id) || empty($input_type)) return false;

        $regionSettingInfo = RegionSetting::where([

            ['region_id','eq',$region_id],

            ['is_del','eq',RegionSetting::NO_DEL]

        ])->find();

        if(empty($regionSettingInfo)) return false;

        $distribution_type = $regionSettingInfo['distribution_type'];

        $max_distribution_num = floatval($regionSettingInfo['max_distribution_num']);

        $ranking_data = empty($regionSettingInfo['ranking_data']) ? [] : explode(',',$regionSettingInfo['ranking_data']);

        $stickPlanList = StickPlan::alias("sp")->join([

            ['offline_shop os','sp.offline_shop_id = os.id','left']

        ])->where([

            ['sp.region_id','eq',$region_id],

            ['sp.input_type','eq',$input_type],

            ['os.mode','eq',OfflineShop::OFFLINESHOP_MODE_OF_SELF_POPULARIZE],

            ['sp.status','eq',StickPlan::STICK_PLAN_STATUS_OF_PAYMENT_SUCCESS],

            ['sp.is_del','eq',StickPlan::NO_DEL]

        ])->order("sp.bidding_price DESC")->field("sp.id,sp.ranking,sp.region_name,sp.offline_shop_id")->select();

        $stickPlanList = $stickPlanList->isEmpty() ? [] : $stickPlanList->toArray();

        $stickNoticeData = [];

        foreach ($stickPlanList as $k=>$v) {

            $ranking = $k + 1;

            if($v['ranking'] == $ranking) continue;

            StickPlan::where([

                ['id','eq',$v['id']]

            ])->update([

                "ranking" => $ranking

            ]);

            $is_distribution = 0;

            switch ($distribution_type) {

                case RegionSetting::DISTRIBUTION_TYPE_OF_NUM:

                    if($ranking <= $max_distribution_num) $is_distribution = 1;

                    break;

                case RegionSetting::DISTRIBUTION_TYPE_OF_CHANCE:

                    if(isset($ranking_data[$k]) && floatval($ranking_data[$k]) > 0) $is_distribution = 1;

                    break;

            }

            array_push($stickNoticeData,[

                "offline_shop_id" => $v['offline_shop_id'],

                "content" => "由于别的友商置顶计划变更，您在".$v['region_name']."地区的置顶计划，在".UserA::getInputTypeText($input_type)."这个类目下排名变更为第".($k+1)."名，".($is_distribution == 1 ?"有":"无")."分配资格的权重",

                "create_time" => time()

            ]);

        }

        $stickPlanId = implode(',',array_column($stickPlanList,"id"));

        self::where([

            ['region_id','eq',$region_id],

            ['input_type','eq',$input_type],

            ['is_del','eq',self::NO_DEL]

        ])->update([

            "ranking_data" => $stickPlanId,

            "update_time" => time()

        ]);

        if(!empty($stickNoticeData)) StickNotice::insertAll($stickNoticeData);

    }

}