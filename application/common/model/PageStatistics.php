<?php

namespace app\common\model;

/**
 * 页面统计
 * Class PageStatistics
 * @package app\common\model
 */
class PageStatistics extends Base {


    //注册页面
    const PAGE_OF_REGISTER = 10;

    //完善信息
    const PAGE_OF_PERFECT = 20;

    //立即领钱
    const PAGE_OF_INPUT = 30;

    //首页
    const PAGE_OF_INDEX = 40;

    //产品详情
    const PAGE_OF_PRODUCT_DETAIL = 50;

    //产品详情外链
    const PAGE_OF_PRODUCT_LINK = 60;




}