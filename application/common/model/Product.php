<?php

namespace app\common\model;

/**
 * 产品
 * Class Product
 * @package app\common\model
 */
class Product extends Base {


    //信用卡
    const PRODUCT_TYPE_OF_CREDIT_CARD = 1;

    //信贷
    const PRODUCT_TYPE_OF_CREDIT_LOAN = 2;

    public static function getTypeText($type,$key="",$val="") {

        $result = [

            self::PRODUCT_TYPE_OF_CREDIT_CARD => "信用卡",

            self::PRODUCT_TYPE_OF_CREDIT_LOAN => "信贷",

        ];

        return self::getArrayText($result,$type,$key,$val);

    }

}