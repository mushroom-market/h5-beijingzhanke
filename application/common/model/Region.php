<?php

namespace app\common\model;


use think\facade\Cache;

class Region extends Base
{

    /**
     * 从缓存中获取省市区所有数据
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getCacheList()
    {

        if (!Cache::has('region_list')) {

            $list = $this->order("level ASC,id ASC")->select();

            $list = $list->isEmpty() ? [] : $list->toArray();

            Cache::set('region_list',$list);

        }

        $result = Cache::get('region_list');

        return $result;

    }


    /**
     * 获取下级所有地区
     * @param int $parent_id
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getChildrenList($parent_id = 100000) {

        $list = $this->getCacheList();

        $array = [];

        foreach ($list as $k=>$v) {

            if($v['parent_region_id'] != $parent_id) continue;

            array_push($array,$v);

        }

        return $array;

    }



}