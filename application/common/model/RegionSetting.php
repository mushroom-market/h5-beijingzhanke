<?php

namespace app\common\model;

/**
 * 区域配置
 * Class RegionSetting
 * @package app\common\model
 */
class RegionSetting extends Base {



    //按量分配
    const DISTRIBUTION_TYPE_OF_NUM = 1;

    //按概率分配
    const DISTRIBUTION_TYPE_OF_CHANCE = 2;


    public static function getDistributionTypeText($type,$key="",$val="") {

        $result = [

            self::DISTRIBUTION_TYPE_OF_NUM => "按量分配",

            self::DISTRIBUTION_TYPE_OF_CHANCE => "概率分配",

        ];

        return self::getArrayText($result,$type,$key,$val);

    }

}