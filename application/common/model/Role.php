<?php

namespace app\common\model;


class Role extends Base {

    /**
     * 已删除标识
     */
    const ROLE_YES_DEL = 1;

    /**
     * 未删除标识
     */
    const ROLE_NO_DEL = 0;

    /**
     * 正常标识
     */
    const ROLE_YES_STATUS = 0;

    /**
     * 禁用标识
     */
    const ROLE_NO_STATUS = 1;

}