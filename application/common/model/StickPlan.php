<?php

namespace app\common\model;

/**
 * 置顶计划
 * Class StickPlan
 * @package app\common\model
 */
class StickPlan extends Base {


    //待付款
    const STICK_PLAN_STATUS_OF_WAIT_PAYMENT = 10;

    //支付成功
    const STICK_PLAN_STATUS_OF_PAYMENT_SUCCESS = 20;

    //余额不足
    const STICK_PLAN_STATUS_OF_BALANCE_NOT_ENOUGH = 30;



    public static function parseModuleField($module_field_data = "",$is_system = -1) {

        $module_field_data = empty($module_field_data) ? [] : explode('|',$module_field_data);

        $new_module_data = [];

        foreach ($module_field_data as $v) {

            $v_array = explode(':', $v);

            $module_id = isset($v_array[0]) ? $v_array[0] : "";

            $module_field_id = isset($v_array[1]) ? $v_array[1] : "";

            if (empty($module_id) || empty($module_field_id)) continue;

            $new_module_data[$module_id] = $module_field_id;

        }

        $result = [];

        $module_list = Module::where([

            ['id','in',array_keys($new_module_data)],

            ['is_del','eq',Module::NO_DEL]

        ])->order("sort DESC")->field("id,name,is_allow_del")->select();

        foreach ($module_list as $v) {

            if($is_system > -1 && $v['is_allow_del'] != $is_system) continue;

            $module_id = $v['id'];

            $module_field_id = isset($new_module_data[$module_id]) ? $new_module_data[$module_id] : "";

            $module_field_list = ModuleField::where([

                ['module_id', 'eq', $module_id],

                ['id', 'in', $module_field_id],

                ['is_del', 'eq', ModuleField::NO_DEL]

            ])->order("sort DESC")->field("id,name,price")->select();

            if ($module_field_list->isEmpty()) continue;

            $module_field_ = [

                "module_id" => $module_id,

                "module_name" => $v['name'],

                "module_field_list" => $module_field_list->toArray()

            ];

            array_push($result,$module_field_);

        }

        return $result;

    }


    public static function parseModuleField1($module_field_data = "",$is_system = -1) {

        $module_field_data = empty($module_field_data) ? [] : explode('|',$module_field_data);

        $result = [];

        foreach ($module_field_data as $v) {

            $v_array = explode(':', $v);

            $module_id = isset($v_array[0]) ? $v_array[0] : "";

            $module_field_id = isset($v_array[1]) ? $v_array[1] : "";

            if (empty($module_id) || empty($module_field_id)) continue;

            $module_where = [

                ['id', 'eq', $module_id],

                ['is_del', 'eq', Module::NO_DEL]

            ];

            if($is_system > -1) array_push($module_where,["is_allow_del",'eq',$is_system]);

            $module_name = Module::where($module_where)->value("name");

            if (empty($module_name)) continue;

            $module_field_list = ModuleField::where([

                ['module_id', 'eq', $module_id],

                ['id', 'in', $module_field_id],

                ['is_del', 'eq', ModuleField::NO_DEL]

            ])->order("sort DESC")->field("id,name,price")->select();

            if ($module_field_list->isEmpty()) continue;

            $module_field_ = [

                "module_id" => $module_id,

                "module_name" => $module_name,

                "module_field_list" => $module_field_list->toArray()

            ];

            array_push($result,$module_field_);

        }

        return $result;

    }



    public static function updateStatusBalanceNotEnough($stickPlanId) {

        $stickPlanMaxInfo = StickPlan::where([

            ['id','eq',$stickPlanId],

            ['status','eq',StickPlan::STICK_PLAN_STATUS_OF_PAYMENT_SUCCESS],

            ['is_del','eq',StickPlan::NO_DEL]

        ])->order("bidding_price DESC")->find();

        $offlineShopMoney = OfflineShop::where([

            ['id','eq',$stickPlanMaxInfo['offline_shop_id']],

            ['is_del','eq',OfflineShop::NO_DEL],

            ['status','eq',OfflineShop::STATUS_NORMAL]

        ])->value("money");

        if($offlineShopMoney < $stickPlanMaxInfo['bidding_price']) {

            $stickPlanList = StickPlan::where([

                ['offline_shop_id','eq',$stickPlanMaxInfo['offline_shop_id']],

                ['status','eq',StickPlan::STICK_PLAN_STATUS_OF_PAYMENT_SUCCESS],

                ['is_del','eq',StickPlan::NO_DEL]

            ])->field("id,region_id,input_type")->select();

            foreach ($stickPlanList as $v) {

                StickPlan::where([

                    ['id','eq',$v['id']],

                ])->update([

                    "status" => StickPlan::STICK_PLAN_STATUS_OF_BALANCE_NOT_ENOUGH,

                    "ranking" => 0

                ]);

                OfflineShopRanking::updateRanking([

                    "region_id" => $v['region_id'],

                    "input_type" => $v['input_type'],

                ]);

            }

        }

    }



    public static function updateStatusPaymentSuccess($offline_shop_id) {

        $stickPlanMaxInfo = StickPlan::where([

            ['offline_shop_id','eq',$offline_shop_id],

            ['status','eq',StickPlan::STICK_PLAN_STATUS_OF_PAYMENT_SUCCESS],

            ['is_del','eq',StickPlan::NO_DEL]

        ])->order("bidding_price DESC")->find();

        $offlineShopMoney = OfflineShop::where([

            ['id','eq',$offline_shop_id],

            ['is_del','eq',OfflineShop::NO_DEL],

            ['status','eq',OfflineShop::STATUS_NORMAL]

        ])->value("money");

        if($offlineShopMoney >= $stickPlanMaxInfo['bidding_price']) {

            $stickPlanList = StickPlan::where([

                ['offline_shop_id','eq',$offline_shop_id],

                ['status','eq',StickPlan::STICK_PLAN_STATUS_OF_BALANCE_NOT_ENOUGH],

                ['is_del','eq',StickPlan::NO_DEL]

            ])->field("id,region_id,input_type")->select();

            foreach ($stickPlanList as $v) {

                StickPlan::where([

                    ['id','eq',$v['id']]

                ])->update([

                    "status" => StickPlan::STICK_PLAN_STATUS_OF_PAYMENT_SUCCESS

                ]);

                OfflineShopRanking::updateRanking([

                    "region_id" => $v['region_id'],

                    "input_type" => $v['input_type'],

                ]);

            }

        }

    }


}