<?php

namespace app\common\model;


class Team extends Base
{

    public function getParentById($parent_id, $level = 5, $data = array())
    {

        if ($level > 0) {

            $user_info = $this->where(array('user_id' => $parent_id))->find();

            if (!empty($user_info)) {

                array_push($data, $user_info);

                if ($user_info['parent_id'] <= 0) {

                    return $data;

                }

                return $this->getParentById($user_info['parent_id'], $level - 1, $data);

            }

        }

        return $data;

    }

}