<?php

namespace app\common\model;


class UserA extends Base
{

    public function getAvatarAttr($value)
    {
        return str_replace("\\", '/', $value);
    }


    public function getNicknameAttr($value)
    {

        return emojiDecode($value);

    }

    public function getToken($user_id)
    {

        return md5(md5($user_id) . time() . "token_a");

    }


    public function getInviteCode()
    {

        srand((double)microtime() * 1000000);//create a random number feed.

        $ychar = "0,1,2,3,4,5,6,7,8,9";//,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z

        $list = explode(",", $ychar);

        $authnum = '';

        for ($i = 0; $i < 6; $i++) {

            $randnum = rand(0, 9); // 10+26;

            $authnum .= $list[$randnum];

        }

        $has = $this->where(array('invite_code' => $authnum))->count();

        if ($has > 0) return $this->getInviteCode();

        return $authnum;

    }



    //无车
    const NO_CAR = 12;

    //有车
    const HAS_CAR = 11;

    //车抵
    const CAR_MORTGAGE = 13;

    public static function getCarText($car,$key="",$val="") {

        $result = [

            self::NO_CAR => "无车",
            self::HAS_CAR => "有车",
            self::CAR_MORTGAGE => "车抵",

        ];

        return self::getArrayText($result,$car,$key,$val);

    }

    //无房
    const NO_HOUSE = 10;

    //有房
    const HAS_HOUSE = 9;

    //房抵
    const HOUSE_MORTGAGE = 14;


    public static function getHouseText($house,$key="",$val="") {

        $result = [

            self::NO_HOUSE => "无房",
            self::HAS_HOUSE => "有房",
            self::HOUSE_MORTGAGE => "房抵",

        ];

        return self::getArrayText($result,$house,$key,$val);

    }



    //注册进度
    const PROGRESS_OF_REGISTER = 1;

    //完善进度
    const PROGRESS_OF_PERFECT = 2;

    //进件进度
    const PROGRESS_OF_INPUT = 3;



    //房贷
    const INPUT_TYPE_OF_HOUSE_LOAN = 1;

    //车贷
    const INPUT_TYPE_OF_CAR_LOAN = 2;

    //信用贷
    const INPUT_TYPE_OF_CREDIT_LOAN = 3;



    public static function getInputTypeText($type,$key = "", $val="") {

        $result = [

            self::INPUT_TYPE_OF_HOUSE_LOAN => "房贷",

            self::INPUT_TYPE_OF_CAR_LOAN => "车贷",

            self::INPUT_TYPE_OF_CREDIT_LOAN => "信用贷",

        ];

        return self::getArrayText($result,$type,$key,$val);

    }



}