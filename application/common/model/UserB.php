<?php

namespace app\common\model;

/**
 * 用户
 * Class UserB
 * @package app\common\model
 */
class UserB extends Base {


    public function getAvatarAttr($value)
    {
        return str_replace("\\", '/', $value);
    }


    public function getNicknameAttr($value)
    {

        return emojiDecode($value);

    }

    public function getToken($user_id)
    {

        return md5(md5($user_id) . time() . "token_b");

    }



    //注册进度
    const PROGRESS_OF_REGISTER = 1;

    //认证进度
    const PROGRESS_OF_AUTHENTICATION = 2;

    //完善进度
    const PROGRESS_OF_PERFECT = 3;

    //提交进度
    const PROGRESS_OF_SUBMIT = 4;



    public static $month_income = [

        "2000-5000",

        "5000-10000",

        "10000-50000",

        "50000以上",

    ];


    public static $sesame_branch = [

        "小于350分",

        "350-500",

        "500-700",

        "700以上",

    ];


    public static $relation = [

        "同事",

        "朋友",

        "夫妻",

        "亲属"

    ];


}