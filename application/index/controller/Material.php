<?php

namespace app\index\controller;

use think\Controller;

class Material extends Controller{

    public function detail() {

        $content = "";

        $id = input('get.id/d',0);

        if($id > 0) {

            $content = (new \app\common\model\Material())->where([

                ['id','eq',$id]

            ])->value('content');

        }

        return $this->fetch('detail',[

            'content' => empty($content) ? "" : $content

        ]);

    }


}