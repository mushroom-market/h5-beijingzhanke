<?php

namespace app\index\controller;

use think\Controller;
use think\Request;

class Rule extends Controller{

    public function web(Request $request) {

        $id = $request->get('id/d',0);

        $content = "";

        if($id > 0) {

            $content = (new \app\common\model\Rule())->where([

                ['id','eq',$id]

            ])->value('content');

        }

        return $this->fetch('web',[

            'content' => $content

        ]);

    }

}