<?php

namespace app\offline\controller;

use think\Request;
use app\common\model\OfflineShop as Obj;
use think\facade\Cookie;
/**
 * 后台
 * Class Index
 * @package app\offline\controller
 */
class Index extends Base
{
    /**
     * 后台首页
     */
    public function index()
    {
        return $this->fetch();
    }


    public function info(Request $request) {
    
        $field_list = [
    
            ['form_type'=>"text","name"=>"name",'title'=>"账号",'value'=>""],
            ['form_type'=>"password","name"=>"password",'title'=>"密码",'value'=>""],
        
    
        ];
    
        if ($request->isPost()) {
    
            try{
    
                $field_list = array_column($field_list,"name");
    
                $data = $request->only($field_list,"post");
    
                if(isset($data['name']) && !empty($data['name'])) {
    
                    $is_exists = Obj::where([
    
                        ['name','eq',$data['name']],
    
                        ['id','neq',$this->account_id],
    
                        ['is_del','eq',Obj::NO_DEL]
    
                    ])->find();
    
                    if(!empty($is_exists)) throw new \Exception("该账号已存在");
    
                }
    
                if(isset($data['password']) && !empty($data['password'])) {
    
                    $data['password'] = md5(md5($data['password']) . 'login');
    
                }else{
    
                    unset($data['password']);
    
                }
    
                Obj::where('id',$this->account_id)->update($data);
    
                $admin_info = Obj::where('id',$this->account_id)->find();
    
                $admin_info = $admin_info->isEmpty() ? [] : $admin_info->toArray();
    
                unset($admin_info['password']);
        
                
                Cookie::set($this->cookie_name,$admin_info);
    
            }catch (\Exception $e) {
    
                $this->error($e->getMessage());
    
            }
    
            $this->success('修改成功');
    
        } else {
    
            $info = Obj::where('id',$this->account_id)->find();
    
            unset($info['password']);
    
            return view('index/info', [
    
                'field_list' => $field_list,
    
                'title' => "基本资料",
    
                'info' => $info
    
            ]);
        }
    
    }

}
