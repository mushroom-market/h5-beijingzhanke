<?php

namespace app\offline\controller;

use app\common\model\OfflineShop;
use app\common\model\Config as ConfigObj;
use think\captcha\Captcha;
use think\facade\Config;
use think\Controller;
use think\facade\Cookie;
use think\Request;

/**
 * 登录
 * Class Login
 * @package app\offline\controller
 */
class Login extends Base
{


    public $agreement;

    public function initialize()
    {
        parent::initialize(); // TODO: Change the autogenerated stub

        $this->agreement = [

            "cooperation_agreement" => "合作协议",

            "cooperation_behavior_agreement" => "信贷员/机构合作行为规定",

            "service_convention_agreement" => "信贷员/机构服务公约",

            "compliance_commitment_agreement" => "合规承诺函",

        ];

    }


    //后台登录
    public function login(Request $request)
    {


        $is_open_qrcode = !Config::get($this->module.'.is_open_qrcode') ? 0 : 1;

        if ($request->isPost()) {

            try{

                $qrcode = $request->post('qrcode/s','');

                $adminname = $request->post('adminname/s','');

                $password = $request->post('password/s','');

                if ($adminname == '' || $password == '') throw new \Exception("参数不能为空");

                if($is_open_qrcode == 1) {

                    if(empty($qrcode)) throw new \Exception("请输入验证码");

                    if(!captcha_check($qrcode)) throw new \Exception("验证码有误");

                }

                $OfflineShopObj = new OfflineShop();

                $OfflineShopInfo = $OfflineShopObj->where([

                    ['name','eq',$adminname],

                    ['status','eq',OfflineShop::STATUS_NORMAL],

                    ['is_del','eq',OfflineShop::NO_DEL],

                ])->find();

                if(empty($OfflineShopInfo)) throw new \Exception("用户名不存在");

                $OfflineShopInfo = $OfflineShopInfo->toArray();

                if ($OfflineShopInfo['password'] !== md5(md5($password) . 'login')) throw new \Exception("密码有误");

                unset($OfflineShopInfo['password']);

                Cookie::set($this->cookie_name,$OfflineShopInfo);

            }catch (\Exception $e) {

                $this->error($e->getMessage());

            }

            $this->success("登录成功");

        } else {

            if(Cookie::has($this->cookie_name)) $this->redirect(url('index/index'));

            return view('login',[

                'is_open_qrcode'=>$is_open_qrcode,

                "agreementList" => $this->agreement

            ]);

        }
    }



    /**
     * 退出登录
     */
    public function logout()
    {
        if (Cookie::has($this->cookie_name)) {

            Cookie::delete($this->cookie_name);

        }

        $this->redirect(url('login/login'));

    }



    public function captchaImg() {

        $is_open_qrcode = !Config::get($this->module.'.is_open_qrcode') ? 0 : 1;

        if($is_open_qrcode == 0) return "";

        $obj = new Captcha();

        $obj->codeSet = "0123456789";

        $obj->useCurve = false;

        $obj->useNoise = false;

        $obj->length = 4;

        return $obj->entry();

    }



    public function offlineAgreement(Request $request) {

        try{

            $key = $request->get('key/s','');

            $result = ConfigObj::getCacheValue("offline_agreement");

            $title = isset($this->agreement[$key]) ? $this->agreement[$key] : "";

            $content = isset($result[$key]) ? $result[$key] : "";

        }catch (\Exception $e) {

            echo "";

        }

        return view("html",[

            "title" => $title,

            "content" => $content

        ]);

    }



}