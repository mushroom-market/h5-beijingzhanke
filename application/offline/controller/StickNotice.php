<?php

namespace app\offline\controller;

use app\common\model\StickNotice as Obj;
use think\Request;
use think\Validate;

/**
 * 置顶公告管理
 * Class StickNotice
 * @package app\offline\controller
 */
class StickNotice extends Base
{

    public $obj, $now_keyword = "置顶公告";

    public function initialize()
    {
        parent::initialize(); // TODO: Change the autogenerated stub

        $this->obj = new Obj();

    }


    public function index(Request $request) {

        $where = [

            ['a.offline_shop_id','eq',$this->account_id]

        ];

        $field_list = [

            ['field'=>'id','title'=>"#"],
            ['field'=>'content','title'=>"内容"],
            ['field'=>'create_time','title'=>"创建时间"],

        ];

        $result = $this->defaultIndex([

            "field_list" => $field_list,

            "where" => $where,

            "order" => "a.id DESC",

            "field" => array_column($field_list,"field")

        ]);

        if(!$request->isAjax()) return $result;

        if (!empty($result['data'])) {

            foreach ($result['data'] as $k => &$v) {

                $v['create_time'] = date("Y-m-d H:i:s",$v['create_time']);

            }
        }

        $this->success([

            'param' => $result

        ]);

    }


}