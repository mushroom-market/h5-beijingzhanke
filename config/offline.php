<?php

return [

    //是否开启权限管理
    'is_open_auth' => false,
    //后台登录是否开启验证码
    'is_open_qrcode' => true,
    //图片默认加载图
    'img_onload_default' => LOCAL_URL.'/static/public/layui/css/modules/layer/default/loading-1.gif',
    //地图KEY
    'gd_map_key' => "0ce50fa7afbc133c255270717e0888ad",
    //后台访问白名单
    'white_list' => [

//        ['module' => 'admin', 'controller' => 'Login', 'action' => "login"],
//        ['module' => 'admin', 'controller' => 'Login', 'action' => "logout"],
//        ['module' => 'admin', 'controller' => 'Login', 'action' => "captchaimg"],
//        ['module' => 'admin', 'controller' => 'Index', 'action' => "index"],
//        ['module' => 'admin', 'controller' => 'Admin', 'action' => "info"],

    ],

];