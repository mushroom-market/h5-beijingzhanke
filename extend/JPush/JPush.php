<?php

namespace JPush;

use app\common\model\Admin;
use think\facade\Config;

class JPush {

    public $client;

    public function __construct()
    {

        $jpush_config = Config::get('jpush.');

        $this->client = (new Client($jpush_config['appKey'], $jpush_config['masterSecret']));

    }


    public function push($array = []) {

        $platform = isset($array['platform']) ? $array['platform'] : ['ios', 'android'];

        $alias = isset($array['alias']) ? $array['alias'] : [];

        $content = isset($array['content']) ? $array['content'] : "";

        $extras = isset($array['extras']) ? $array['extras'] : [];

        if(!empty($alias) && !empty($content)) {

            $alias = (new Admin())->where([

                ['id','in',$alias],

                ['is_del','eq',Admin::NO_DEL],

                ['status','eq',0],

                ['is_app','eq',1]

            ])->column("id");

            foreach ($alias as $k=>$v) {

                $alias[$k] = "admin_".$v;

            }

            if(!empty($alias)) {

                $this->client->push()
                    ->setPlatform($platform)
                    ->addAlias($alias)
                    ->iosNotification($content, ['extras' => $extras])
                    ->androidNotification($content, ['extras' => $extras])
                    ->send();

            }

        }

    }

}