<?php
namespace alipay;

use alipay\aop\AopClient;
use alipay\aop\request\AlipayFundTransToaccountTransferRequest;
use alipay\aop\request\AlipayTradeAppPayRequest;
use think\Log;

class Alipay{
    
    private $config=array();
    
    /**
     * @return the $config
     */
    public function getConfig()
    {
        
        return $this->config;
        
    }

    /**
     * @param multitype:string  $config
     */
    public function setConfig($config)
    {
        
        $this->config = $config;
        
    }
    
    public function setOneConfig($key,$val){
        
        $this->config[$key]=$val;
        
    }

    public function __construct($config=array()){
        $def_config=array(
            'app_id'=>'',
            'partner_key'=>'',
            'partner_id'=>'',
            'notify_url'=>''
        );
        $this->config=array_merge($def_config,$config);
        
    }
    public function verify($data, $sign, $rsaPublicKeyFilePath){
        $aop = new AopClient();
        $aop->alipayrsaPublicKey=$this->config['alipay_public_key'];
        return $aop->rsaCheckV1($data, NULL, "RSA2");
    }
    public function getPayOfApp($data){
        
        $aop = new AopClient();
        
        $aop->gatewayUrl = "https://openapi.alipay.com/gateway.do";
        
        $aop->appId = $this->config['app_id'];//合作ID
        
        $aop->rsaPrivateKey = $this->config['merchant_private_key'];//请填写开发者私钥去头去尾去回车，一行字符串
        
        $aop->format = "json";
        
        $aop->charset = "UTF-8";
        
        $aop->signType = "RSA2";
        
        $aop->alipayrsaPublicKey =$this->config['alipay_public_key'];//请填写支付宝公钥，一行字符串
        
        //实例化具体API对应的request类,类名称和接口名称对应,当前调用接口名称：alipay.trade.app.pay
        
        $request = new AlipayTradeAppPayRequest();
        
        //SDK已经封装掉了公共参数，这里只需要传入业务参数
        Log::write(var_export($this->config,1));
         
        $request->setNotifyUrl($this->config['notify_url']);//回调接口
         
        $request->setBizContent(json_encode(array_merge(array(
            
            'timeout_express'=>'30m',
            
            'product_code'=>'QUICK_MSECURITY_PAY'
            
        ),$data)));
        
        //这里和普通的接口调用不同，使用的是sdkExecute
        return $aop->sdkExecute($request);
        
    }
    
    public function checkOrder($data){
        
        $aop = new AopClient();
        $aop->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
        
        $aop->appId = $this->config['app_id'];
        
        $aop->rsaPrivateKey = $this->config['merchant_private_key'];
        
        $aop->alipayrsaPublicKey=$this->config['alipay_public_key'];
        
        $aop->apiVersion = '1.0';
        
        $aop->signType = 'RSA2';
        
        $aop->postCharset='GBK';
        
        $aop->format='json';
        
        $request = new \AlipayTradeQueryRequest();
        
        $request->setBizContent(json_encode($data));
        
        $result = $aop->execute ( $request);
        
        $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
        
        $resultCode = $result->$responseNode->code;
        
        if(!empty($resultCode)&&$resultCode == 10000){
           
            return $result;
            
        } else {
              
            throw new \Exception("查询订单失败");
            
        }
        
        
    }
    public function refundOfApp($data){
        
        $aop = new AopClient ();
        
        $aop->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
        
        $aop->appId = $this->config['app_id'];
        
        $aop->rsaPrivateKey = $this->config['merchant_private_key'];
        
        $aop->alipayrsaPublicKey=$this->config['alipay_public_key'];
        
        $aop->apiVersion = '1.0';
        
        $aop->signType = 'RSA2';
        
        $aop->postCharset='GBK';
        
        $aop->format='json';
        
        $request = new \AlipayTradeRefundRequest();
        
        $request->setBizContent(json_encode($data));
        
        /**
        $request->setBizContent("{" .
            "\"out_trade_no\":\"20150320010101001\"," .
            "\"trade_no\":\"2014112611001004680073956707\"," .
            "\"refund_amount\":200.12," .
            "\"refund_currency\":\"USD\"," .
            "\"refund_reason\":\"正常退款\"," .
            "\"out_request_no\":\"HZ01RF001\"," .
            "\"operator_id\":\"OP001\"," .
            "\"store_id\":\"NJ_S_001\"," .
            "\"terminal_id\":\"NJ_T_001\"," .
            "      \"goods_detail\":[{" .
            "        \"goods_id\":\"apple-01\"," .
            "\"alipay_goods_id\":\"20010001\"," .
            "\"goods_name\":\"ipad\"," .
            "\"quantity\":1," .
            "\"price\":2000," .
            "\"goods_category\":\"34543238\"," .
            "\"body\":\"特价手机\"," .
            "\"show_url\":\"http://www.alipay.com/xxx.jpg\"" .
            "        }]," .
            "      \"refund_royalty_parameters\":[{" .
            "        \"royalty_type\":\"transfer\"," .
            "\"trans_out\":\"2088101126765726\"," .
            "\"trans_out_type\":\"userId\"," .
            "\"trans_in_type\":\"userId\"," .
            "\"trans_in\":\"2088101126708402\"," .
            "\"amount\":0.1," .
            "\"amount_percentage\":100," .
            "\"desc\":\"分账给2088101126708402\"" .
            "        }]," .
            "\"org_pid\":\"2088101117952222\"" .
            "  }");
        */
        $result = $aop->execute ( $request);
        
        $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
         
        return $result->$responseNode;
        
    }




    public function transfer($data) {

        $aop = new AopClient ();

        $aop->gatewayUrl = 'https://openapi.alipay.com/gateway.do';

        $aop->appId = $this->config['app_id'];

        $aop->rsaPrivateKey = $this->config['merchant_private_key'];

        $aop->alipayrsaPublicKey=$this->config['alipay_public_key'];

        $aop->apiVersion = '1.0';

        $aop->signType = 'RSA2';

        $aop->postCharset="UTF-8";

        $aop->format='json';

        $request = new AlipayFundTransToaccountTransferRequest();

        $request->setBizContent(json_encode($data));

//        $request->setBizContent("{" .
//            "\"out_biz_no\":\"3142321423432\"," .
//            "\"payee_type\":\"ALIPAY_LOGONID\"," .
//            "\"payee_account\":\"abc@sina.com\"," .
//            "\"amount\":\"12.23\"," .
//            "\"payer_show_name\":\"上海交通卡退款\"," .
//            "\"payee_real_name\":\"张三\"," .
//            "\"remark\":\"转账备注\"" .
//            "  }");

        $result = $aop->execute ( $request);

        $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";

        return $result->$responseNode;

    }



    public function auth($data) {

        $aop = new AopClient ();

        $params = [

            'apiname' => "com.alipay.account.auth",

            "method" => "alipay.open.auth.sdk.code.get",

            "app_id" => $this->config['app_id'],

//            "app_name" => "龙猫天下",

            "biz_type" => "openservice",

            "pid" => "2088431926313901",

            "product_id" => "APP_FAST_LOGIN",

            "scope" => "kuaijie",

//            "target_id" => $this->visitor->get('user_id'),

            "auth_type" => "AUTHACCOUNT",

            "sign_type" => "RSA2",

        ];

        $params = array_merge($params,$data);

        $paramStr = $aop->getSignContent($params);//组装请求签名参数
        $sign = $aop->alonersaSign($paramStr, $this->config['merchant_private_key'], 'RSA2', false);//生成签名

        $params['sign'] = $sign;

        return $aop->getSignContentUrlencode($params);



    }



}

