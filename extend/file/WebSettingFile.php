<?php

namespace file;

/**
 * 网站配置数据
 * Class WebSettingFile
 * @package file
 */
class WebSettingFile extends ArrayFile{

    public function __construct($params=array()){

        $this->_filename = CONF_PATH.'admin/web_setting.php';

        parent::__construct($params);

    }


}