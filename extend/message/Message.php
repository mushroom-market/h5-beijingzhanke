<?php
/**
 * 短信接口类
 * ==============================================
 * 版权所有 2014-2017 http://www.zhaiyong.net
 * ----------------------------------------------
 * 这不是一个自由软件，未经授权不许任何使用和传播。
 * ==============================================
 * @date: 2017年7月27日
 * @author: cgm
 * @version:1.0
 */

namespace message;

use think\Config;
use think\Loader;
use think\Exception;
use message\aliyun\Aliyun;
use app\common\model\SmsLog;

class Message
{
    const PHONE_NUMBER_EMPTY = "手机号码不能为空";
    const SECONDS_IS_NOT_YET = "秒内不能重复发送";
    const SEND_MSG_ERROR = "发送失败";
    const MESSAGE_TIMEOUT_ERROR = "短信已超时,请重新发送";
    const MESSAGE_NUMBER_ERROR = "验证已失效,请重新发送短信";
    const MESSAGE_CODE_ERROR = "验证码错误";
    const TEMPLATE_CODE_ERROR = "短信模板未定义";
    const NOTIFIY_TEMPLATE_CODE_ERROR = "通知模板未定义";

    private static $instance;
    private $template;
    private $options;
    private $message;

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            $config = \think\facade\Config::get("sms.");
            self::$instance = new Message($config);
        }
        return self::$instance;
    }

    /**
     * 参数初始化
     */
    private function __construct($config)
    {


        /**
         * $this->appKey    = $config['appid'];
         * $this->appSecret = $config['secret'];
         * $this->template  = $config['template'];
         * $this->options   = $config['options'];
         */


        switch ($config['platform'] || 'netease') {
            case 'aliyun':
                $this->message = new Aliyun($config['aliyun']);
                $this->template = $config['aliyun']['template'];
                break;
            case 'netease':
                //fsockopen伪造请求
                $this->message = new Netease($config['netease']);
                $this->template = $config['aliyun']['netease'];
                break;
        }


    }

    public function sendTemplate($mobile, $tpl, $param = array())
    {
        if (!isset($this->template[$tpl])) {
            throw new \Exception(self::NOTIFIY_TEMPLATE_CODE_ERROR, 1503);
        }
        return $this->message->sendSMSTemplate($this->template[$tpl], $mobile, $param);

    }

    public function sendSMSCode($mobile, $checkNo, $tpl)
    {

        $log_mod = new SmsLog();

        $log_info = $log_mod->where(array('mobile' => $mobile, 'status' => 1))->order('id', 'desc')->find();

        $time_out = \think\facade\Config::get("sms.time_out");

        $time_out = $time_out ? $time_out : 90;

        $log_data = empty($log_info) ? array() : $log_info->toArray();

        if (!empty($log_data)) {

            if ((time() - $log_data['add_time']) < $time_out) {

                throw new \Exception($time_out . self::SECONDS_IS_NOT_YET, 1502);

            }

        }

        //1.发送短信
        if (empty($mobile)) {

            throw new \Exception(self::PHONE_NUMBER_EMPTY, 1501);

        }

        $current_time = time();

        if (!isset($this->template[$tpl])) {

            throw new \Exception(self::TEMPLATE_CODE_ERROR, 1503);

        }

        $code = rand(1000, 9999);

        $result = $this->message->sendSmsCode(array(

            'template' => $this->template[$tpl],

            'mobile' => $mobile,

            'device' => '',

            'code' => $code . ''

        ));

        if (empty($result['obj'])) {

            throw new \Exception($result['message']);

        }
        if (!empty($log_data)) {

            $affect = $log_mod->where(array(

                'id' => $log_data['id']

            ))->update(array(

                'mobile' => $mobile,

                'add_time' => $current_time,

                'code' => $result['obj'],

                'status' => 1,

                'config' => json_encode(array('tpl' => $this->template[$tpl])),

                'scene' => $tpl,

                'error_msg' => $result['message'],

                'check_num' => $checkNo

            ));

            if ($affect === false) {

                throw new \Exception($log_mod->getError(), 1504);

            }

        } else {
            $affect = $log_mod->insertGetId(array(

                'mobile' => $mobile,

                'add_time' => $current_time,

                'code' => isset($result['obj']) ? $result['obj'] : '',

                'status' => 1,

                'config' => json_encode(array('tpl' => $this->template[$tpl])),

                'scene' => $tpl,

                'error_msg' => $result['message'],

                'check_num' => $checkNo

            ));

            if (empty($affect)) {
                throw new \Exception($log_mod->getError(), 1504);
            }
        }

    }

    /* public function checkSMSCodeById($id,$mobile,$check_num=true){
        if(empty($id)){
            throw new \Exception(self::MESSAGE_CODE_ERROR,1510);
        } 
        $current_time   = time();
        $log_mod        = new SmsLog();
        $log_info       = $log_mod->field('code,check_num,add_time,id+10000 as mask_id,check_num-1 as after_num')->where(array('id'=>$id-10000, 'mobile'=>$mobile))->order('id','desc')->find();
        if(empty($log_info)){
            throw new \Exception(self::MESSAGE_CODE_ERROR,1510);
        }
        $log_data=$log_info->toArray();
        if(($current_time - $log_data['add_time']) >= 600){
            throw new \Exception(self::MESSAGE_TIMEOUT_ERROR,1510);
        }
        //校验次数
        if($check_num && $log_data['check_num']<=0){
            throw new \Exception(self::MESSAGE_NUMBER_ERROR,1511);
        } 
        if($check_num==true){
            $result=$log_mod->where(array('id'=>$log_data['id']))->setDec("check_num",1);
            if($result===false){
                throw new \Exception($log_mod->getError(),1504);
            }
        } 
    } */

    public function checkSMSCode($mobile, $code, $tpl, $check_num = true)
    {

        $current_time = time();

        $log_mod = new SmsLog();

        $log_info = $log_mod->field('id,code,check_num,add_time,id+10000 as mask_id,check_num-1 as after_num')->where(array('mobile' => $mobile, 'status' => 1, 'scene' => $tpl))->order('id', 'desc')->find();

        if (empty($log_info)) {

            throw new \Exception(self::MESSAGE_CODE_ERROR, 1510);

        }

        $log_data = $log_info->toArray();

        if (($current_time - $log_data['add_time']) >= 600) {
            throw new \Exception(self::MESSAGE_TIMEOUT_ERROR, 1510);
        }

        //校验次数
        if ($check_num == false && $log_data['check_num'] <= 0) { //不检查次数的情况下 判断次数是否是0-1次？如果是就确实检查的次数过多了

            throw new \Exception(self::MESSAGE_NUMBER_ERROR, 1511);

        } elseif ($log_data['check_num'] <= 0) {

            throw new \Exception(self::MESSAGE_NUMBER_ERROR, 1511);

        }

        if (empty($code) || $log_data['code'] != $code) {

            throw new \Exception(self::MESSAGE_CODE_ERROR, 1512);

        }

        if ($check_num == true) {

            $result = $log_mod->where(array('id' => $log_data['id']))->setDec("check_num", 1);

        }

        return $log_data;

    }

    /**
     * 清除短信接口
     * @param unknown $phone
     * @param unknown $code
     * @param string $key
     */
    public function clearSMSCode($mobile, $tpl)
    {


        $log_mod = new SmsLog();

        $result = $log_mod->where(array('scene' => $tpl, 'mobile' => $mobile))->delete();

        if ($result === false) {

            throw new \Exception($log_mod->getError(), 1504);

        }
    }

}

