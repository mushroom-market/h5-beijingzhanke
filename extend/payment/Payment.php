<?php

/**
 * 支付类
 * Class Payment
 * @package app\purpose\controller
 */

namespace payment;

use think\facade\Config;

class Payment
{

    protected $wechat_config, $alipay_config, $wechatObj, $alipayObj;

    /**
     * 微信app
     */
    const WECHAT_PAY_MODULE_APP = 1;

    /**
     * 微信小程序
     */
    const WECHAT_PAY_MODULE_MINI_APP = 2;

    /**
     * 微信公众号
     */
    const WECHAT_PAY_MODULE_GONGZHONGHAO = 3;

    /**
     * 微信手机网站
     */
    const WECHAT_PAY_MODULE_WAP = 4;

    /**
     * 微信刷卡支付
     */
    const WECHAT_PAY_MODULE_POS = 5;

    /**
     * 微信扫码支付
     */
    const WECHAT_PAY_MODULE_SCAN = 6;

    /**
     * 微信账户转账
     */
    const WECHAT_PAY_MODULE_TRANSFER = 7;

    /**
     * 微信普通红包
     */
    const WECHAT_PAY_MODULE_RED_PACK = 8;

    /**
     * 微信裂变红包
     */
    const WECHAT_PAY_MODULE_GROUP_RED_PACK = 9;

    /**
     * 支付宝电脑支付
     */
    const ALIPAY_PAY_MODULE_WEB = 1;

    /**
     * 支付宝手机网站支付
     */
    const ALIPAY_PAY_MODULE_WAP = 2;

    /**
     * 支付宝APP支付
     */
    const ALIPAY_PAY_MODULE_APP = 3;

    /**
     * 支付宝刷卡支付
     */
    const ALIPAY_PAY_MODULE_POS = 4;

    /**
     * 支付宝扫码支付
     */
    const ALIPAY_PAY_MODULE_SCAN = 5;

    /**
     * 支付宝账户转账
     */
    const ALIPAY_PAY_MODULE_TRANSFER = 6;

    /**
     * 支付宝小程序支付
     */
    const ALIPAY_PAY_MODULE_MINI_APP = 7;


    public function __construct()
    {

        $this->wechat_config = Config::get('pay.wechat');

        $this->alipay_config = Config::get('pay.alipay');

        $this->wechatObj = \Yansongda\Pay\Pay::wechat($this->wechat_config);

        $this->alipayObj = \Yansongda\Pay\Pay::alipay($this->alipay_config);

    }


    /**
     * 微信支付
     * @param array $array
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response|\Yansongda\Supports\Collection
     * @throws \Exception
     */
    public function wechat($array = [])
    {

        try{

            $order_data = isset($array['order']) && !empty($array['order']) ? $array['order'] : [];

            $pay_module = isset($array['pay_module']) && !empty($array['pay_module']) ? $array['pay_module'] : 0;

            if(empty($order_data)) throw new \Exception("订单参数有误");

            if(empty($pay_module)) throw new \Exception("请选择支付类型");

            switch ($pay_module) {

                //APP支付
                case self::WECHAT_PAY_MODULE_APP:

                    $order_data = array_merge([

                        'out_trade_no' => time(),

                        'body' => 'subject-测试',

                        'total_fee' => '1',

                    ],$order_data);

                    $result = $this->wechatObj->app($order_data);

                    break;

                //小程序支付
                case self::WECHAT_PAY_MODULE_MINI_APP:

                    $order_data = array_merge([

                        'out_trade_no' => time(),

                        'body' => 'subject-测试',

                        'total_fee' => '1',

                        'openid' => 'onkVf1FjWS5SBxxxxxxxx',

                    ],$order_data);

                    $result = $this->wechatObj->miniapp($order_data);

                    break;

                //公众号支付
                case self::WECHAT_PAY_MODULE_GONGZHONGHAO:

                    $order_data = array_merge([

                        'out_trade_no' => time(),

                        'body' => 'subject-测试',

                        'total_fee' => '1',

                        'openid' => 'onkVf1FjWS5SBxxxxxxxx',

                    ],$order_data);

                    $result = $this->wechatObj->mp($order_data);

                    break;

                //手机网站支付
                case self::WECHAT_PAY_MODULE_WAP:

                    $order_data = array_merge([

                        'out_trade_no' => time(),

                        'body' => 'subject-测试',

                        'total_fee' => '1',

                    ],$order_data);

                    $result = $this->wechatObj->wap($order_data);

                    break;

                //刷卡支付
                case self::WECHAT_PAY_MODULE_POS:

                    $order_data = array_merge([

                        'out_trade_no' => time(),

                        'body' => 'subject-测试',

                        'total_fee'      => '1',

                        'auth_code' => '1354804793001231564897',

                    ],$order_data);

                    $result = $this->wechatObj->pos($order_data);

                    break;

                //扫码支付
                case self::WECHAT_PAY_MODULE_SCAN:

                    $order_data = array_merge([

                        'out_trade_no' => time(),

                        'body' => 'subject-测试',

                        'total_fee'      => '1',

                    ],$order_data);

                    $result = $this->wechatObj->scan($order_data);

                    break;

                //转账
                case self::WECHAT_PAY_MODULE_TRANSFER:

                    $order_data = array_merge([

                        'partner_trade_no' => '',              //商户订单号

                        'openid' => '',                        //收款人的openid

                        'check_name' => 'NO_CHECK',            //NO_CHECK：不校验真实姓名\FORCE_CHECK：强校验真实姓名

                        // 're_user_name'=>'张三',              //check_name为 FORCE_CHECK 校验实名的时候必须提交

                        'amount' => '1',                       //企业付款金额，单位为分

                        'desc' => '帐户提现',                  //付款说明

                    ],$order_data);

                    $result = $this->wechatObj->transfer($order_data);

                    break;

                //普通红包
                case self::WECHAT_PAY_MODULE_RED_PACK:

                    $order_data = array_merge([

                        'mch_billno' => '商户订单号',

                        'send_name' => '商户名称',

                        'total_amount' => '1',

                        're_openid' => '用户openid',

                        'total_num' => '1',

                        'wishing' => '祝福语',

                        'act_name' => '活动名称',

                        'remark' => '备注',

                    ],$order_data);

                    $result = $this->wechatObj->redpack($order_data);

                    break;

                //裂变红包
                case self::WECHAT_PAY_MODULE_GROUP_RED_PACK:

                    $order_data = array_merge([

                        'mch_billno' => '商户订单号',

                        'send_name' => '商户名称',

                        'total_amount' => '1',

                        're_openid' => '用户openid',

                        'total_num' => '3',

                        'wishing' => '祝福语',

                        'act_name' => '活动名称',

                        'remark' => '备注',

                    ],$order_data);

                    $result = $this->wechatObj->groupRedpack($order_data);

                    break;

                default:

                    throw new \Exception("请选择支付类型");

                    break;

            }

        }catch (\Exception $e) {

            throw new \Exception($e->getMessage(),$e->getCode());

        }

        return $result;

    }


    /**
     * 支付宝支付
     * @param array $array
     * @return \Symfony\Component\HttpFoundation\Response|\Yansongda\Supports\Collection
     * @throws \Exception
     */
    public function alipay($array = [])
    {

        try{

            $order_data = isset($array['order']) && !empty($array['order']) ? $array['order'] : [];

            $pay_module = isset($array['pay_module']) && !empty($array['pay_module']) ? $array['pay_module'] : 0;

            if(empty($order_data)) throw new \Exception("订单参数有误");

            if(empty($pay_module)) throw new \Exception("请选择支付类型");

            switch ($pay_module) {

                //电脑支付
                case self::ALIPAY_PAY_MODULE_WEB:

                    $order_data = array_merge([

                        'out_trade_no' => time(),

                        'total_amount' => '0.01',

                        'subject'      => 'test subject-测试订单',

                    ],$order_data);

                    $result = $this->alipayObj->web($order_data);

                    break;

                //手机网站支付
                case self::ALIPAY_PAY_MODULE_WAP:

                    $order_data = array_merge([

                        'out_trade_no' => time(),

                        'total_amount' => '0.01',

                        'subject'      => 'test subject-测试订单',

                    ],$order_data);

                    $result = $this->alipayObj->wap($order_data);

                    break;

                //APP支付
                case self::ALIPAY_PAY_MODULE_APP:

                    $order_data = array_merge([

                        'out_trade_no' => time(),

                        'total_amount' => '0.01',

                        'subject'      => 'test subject-测试订单',

                    ],$order_data);

                    $result = $this->alipayObj->app($order_data);

                    break;

                //刷卡支付
                case self::ALIPAY_PAY_MODULE_POS:

                    $order_data = array_merge([

                        'out_trade_no' => time(),

                        'total_amount' => '0.01',

                        'subject'      => 'test subject-刷卡支付',

                        'auth_code' => '289756915257123456',

                    ],$order_data);

                    $result = $this->alipayObj->pos($order_data);

                    break;

                //扫码支付
                case self::ALIPAY_PAY_MODULE_SCAN:

                    $order_data = array_merge([

                        'out_trade_no' => time(),

                        'total_amount' => '0.01',

                        'subject'      => 'test subject-测试订单',

                    ],$order_data);

                    $result = $this->alipayObj->scan($order_data);

                    break;

                //转账
                case self::ALIPAY_PAY_MODULE_TRANSFER:

                    $order_data = array_merge([

                        'out_biz_no' => time(),

                        'trans_amount' => '0.01',

                        'product_code' => 'TRANS_ACCOUNT_NO_PWD',

                        'payee_info' => [

                            'identity' => 'ghdhjw7124@sandbox.com',

                            'identity_type' => 'ALIPAY_LOGON_ID',

                        ],

                    ],$order_data);

                    $result = $this->alipayObj->transfer($order_data);

                    break;

                //小程序支付
                case self::ALIPAY_PAY_MODULE_MINI_APP:

                    $order_data = array_merge([

                        'out_trade_no' => time(),

                        'subject' => 'test subject-小程序支付',

                        'total_amount' => '0.01',

                        'buyer_id' => 2088622190161234,

                    ],$order_data);

                    $result = $this->alipayObj->mini($order_data);

                    break;

                default:

                    throw new \Exception("请选择支付类型");

                    break;

            }

        }catch (\Exception $e){

            throw new \Exception($e->getMessage(),$e->getCode());

        }

        return $result;

    }

}