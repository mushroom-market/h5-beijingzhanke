<?php

namespace third_login;

use alipay\aop\AopClient;
use alipay\aop\request\AlipaySystemOauthTokenRequest;
use alipay\aop\request\AlipayUserInfoShareRequest;
use think\facade\Cache;
use think\facade\Config;
use think\Request;
use wechat\WXBizDataCrypt;

class ThirdLogin {

    private $wechat_config,$alipay_config;

    public function __construct()
    {
        $this->wechat_config = Config::get('pay.wechat');

        $this->alipay_config = Config::get("pay.alipay");
    }


    public function getWechatJsApiConfig($sub_array = []) {

        $config = $this->wechat_config;

        $array = [

            "is_enterprise" => 0,

        ];

        if(!empty($sub_array)) $array = array_merge($array,$sub_array);

        $jsapiTicket = $this->getWechatJsApiTicket($config,$array);

        if(isset($array['url']) && !empty($array['url'])) {

            $url = $array['url'];

        }else{

            $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";

            $url = "$protocol$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

        }

        $timestamp = time();

        $nonceStr = random(16,"all");

        $string = "jsapi_ticket=$jsapiTicket&noncestr=$nonceStr&timestamp=$timestamp&url=$url";

        $signature = sha1($string);

        $signPackage = array(

            "appId"     => $config['app_id'],

            "nonceStr"  => $nonceStr,

            "timestamp" => $timestamp,

            "url"       => $url,

            "signature" => $signature,

            "rawString" => $string

        );

        return $signPackage;

    }


    private function getWechatJsApiTicket($config,$array = []) {

        $ticket = Cache::get('wechat_jsapi_ticket_');

        if(!empty($ticket)) return $ticket;

        $accessToken = $this->getWechatAccessToken($config,$array);

        $is_enterprise = isset($array['is_enterprise']) ? $array['is_enterprise'] : 0;

        if($is_enterprise == 1) {

            $url = "https://qyapi.weixin.qq.com/cgi-bin/get_jsapi_ticket?access_token=$accessToken";

        }else{

            $url = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?type=jsapi&access_token=$accessToken";

        }

        $res = json2array(file_get_contents($url));

        $ticket = $res['ticket'];

        if ($ticket) Cache::set('wechat_jsapi_ticket_',$ticket,7000);

        return $ticket;

    }

    private function getWechatAccessToken($config,$array = []) {

        $access_token = Cache::get('wechat_access_token_');

        if (!empty($access_token)) return $access_token;

        $is_enterprise = isset($array['is_enterprise']) ? $array['is_enterprise'] : 0;

        if($is_enterprise == 1) {

            $url = "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid={$config['app_id']}&corpsecret={$config['appsercet']}";

        }else{

            $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={$config['app_id']}&secret={$config['appsercet']}";

        }

        $res = json2array(file_get_contents($url));

        if(!isset($res['access_token']) || empty($res['access_token'])) throw new \Exception($res['errmsg']);

        $access_token = $res['access_token'];

        Cache::set('wechat_access_token_',$access_token,7000);

        return $access_token;

    }


    /**
     * 获取微信公众号信息
     * @param Request $request
     * @param array $data
     * @return array
     * @throws \Exception
     */
    public function getWechatGzhInfo(Request $request,$data = []) {

        try{

            $gzh = $this->wechat_config;

            $code = $request->param("code/s",'');

            if(!empty($data) && isset($data['code'])) {

                $code = $data['code'];

            }

            if(empty($code)) throw new \Exception("参数有误");

            $first_url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid={$gzh['app_id']}&secret={$gzh['appsercet']}&code={$code}&grant_type=authorization_code";

            $respon = file_get_contents($first_url);

            $respon = empty($respon) ? [] : json2array($respon);

            $openId = isset($respon['openid']) ? $respon['openid'] : "";

            $access_token = isset($respon['access_token']) ? $respon['access_token'] : "";

            $unionid = isset($respon['unionid']) ? $respon['unionid'] : "";

            if(empty($openId) || empty($access_token)) throw new \Exception("获取微信信息失败");

            $second_url = "https://api.weixin.qq.com/sns/userinfo?access_token={$access_token}&openid={$openId}&lang=zh_CN";

            $content = file_get_contents($second_url);

            if(empty($content)) throw new \Exception("获取用户信息失败");

            $wechat_data = empty($content) ? [] : json2array($content);

            $result = [

                'nickname' => isset($wechat_data['nickname']) ? emojiEncode($wechat_data['nickname']) : "",

                'avatar' => isset($wechat_data['headimgurl']) ? $wechat_data['headimgurl'] : "",

                'openid' => isset($wechat_data['openid']) ? $wechat_data['openid'] : "",

                'unionid' => empty($unionid) ? (isset($wechat_data['unionid']) ? $wechat_data['unionid'] : "") : $unionid,

                'sex' => isset($wechat_data['sex']) ? $wechat_data['sex'] : "",

                'province' => isset($wechat_data['province']) ? $wechat_data['province'] : "",

                'city' => isset($wechat_data['city']) ? $wechat_data['city'] : "",

                'country' => isset($wechat_data['country']) ? $wechat_data['country'] : "",

                'privilege' => isset($wechat_data['privilege']) ? json2array($wechat_data['privilege']) : [],

            ];

        }catch (\Exception $e) {

            throw new \Exception($e->getMessage(),$e->getCode());

        }

        return $result;

    }

    public function checkWechatUserIsFollow($openid){

        $config = $this->wechat_config;

        $subscribe_msg = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=" . $this->getWechatAccessToken($config) . "&openid=" . $openid;

        $subscribe = json2array(file_get_contents($subscribe_msg));

        $is_follow = $subscribe['subscribe'] == 1 ? true : false;   //1为关注 反之未关注

        return $is_follow;

    }


    public function sendWechatGzhmsg($openid, $content){

        $config = $this->wechat_config;

        $url = 'https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=' . $this->getWechatAccessToken($config);

        $data = [

            'touser' => $openid,

            'msgtype' => 'text',

            'text' => [

                'content' => $content

            ]

        ];

        $res = json2array(goCurl($url, 'post', array2json($data)));

        if ($res['errcode'] == 0 && $res['errmsg'] == 'ok') return true;

        return false;

    }


    /**
     * 获取微信小程序信息
     * @param Request $request
     * @param array $data
     * @return array
     * @throws \Exception
     */
    public function getWechatWeappInfo(Request $request,$data = []) {

        try {

            $weapp = $this->wechat_config;

            $array = [

                "iv" => $request->param("iv/s",''),

                "code" => $request->param("weapp_code/s",''),

                "encrypt" => $request->param("encrypt/s",''),

            ];

            if(!empty($data)) $array = array_merge($array,$data);

            $url = "https://api.weixin.qq.com/sns/jscode2session?appid={$weapp['miniapp_id']}&secret={$weapp['miniappsercet']}&js_code={$array['code']}&grant_type=authorization_code";

            $wechat_info = json2array(file_get_contents($url));

            if (empty($wechat_info) || !isset($wechat_info['session_key'])) throw new \Exception("获取微信信息失败");

            $data_ = "";

            $obj = new WXBizDataCrypt($weapp['appid'], $wechat_info['session_key']);

            $obj->decryptData(urldecode($array['encrypt']), urldecode($array['iv']), $data_);

            $wx_user_info = json2array($data_);

            if (empty($wx_user_info) || empty($wx_user_info['openId'])) throw new \Exception("获取用户信息失败");

            $open_id = $wx_user_info['openId'];

            $result = [

                "nickname" => isset($wx_user_info['nickName']) ? emojiEncode($wx_user_info['nickName']) : "",

                "avatar" => isset($wx_user_info['avatarUrl']) ? $wx_user_info['avatarUrl'] : "",

                "openid" => $open_id,

                "unionid" => isset($wx_user_info['unionId']) ? $wx_user_info['unionId'] : "",

                "sex" => isset($wx_user_info['gender']) ? $wx_user_info['gender'] : "",

                "province" => isset($wx_user_info['province']) ? $wx_user_info['province'] : "",

                "city" => isset($wx_user_info['city']) ? $wx_user_info['city'] : "",

                "country" => isset($wx_user_info['country']) ? $wx_user_info['country'] : "",

            ];

        } catch (\Exception $e) {

            throw new \Exception($e->getMessage(), $e->getCode());

        }

        return $result;

    }


    /**
     * 获取微信APP信息
     * @param Request $request
     * @param array $data
     * @return array
     * @throws \Exception
     */
    public function getWechatAppInfo(Request $request,$data = []) {

        try{

            $array = [

                "open_id" => $request->param('open_id/s',''),

                "access_token" => $request->param('access_token/s',''),

            ];

            if(!empty($data)) $array = array_merge($array,$data);

            $url = "https://api.weixin.qq.com/sns/userinfo?access_token={$array['access_token']}&openid={$array['open_id']}";

            $content = file_get_contents($url);

            if(empty($content)) throw new \Exception("获取用户信息失败");

            $wechat_data = empty($content) ? [] : json2array($content);

            $result = [

                'nickname' => isset($wechat_data['nickname']) ? emojiEncode($wechat_data['nickname']) : "",

                'avatar' => isset($wechat_data['headimgurl']) ? $wechat_data['headimgurl'] : "",

                'openid' => isset($wechat_data['openid']) ? $wechat_data['openid'] : "",

                'unionid' => empty($unionid) ? (isset($wechat_data['unionid']) ? $wechat_data['unionid'] : "") : $unionid,

                'sex' => isset($wechat_data['sex']) ? $wechat_data['sex'] : "",

                'province' => isset($wechat_data['province']) ? $wechat_data['province'] : "",

                'city' => isset($wechat_data['city']) ? $wechat_data['city'] : "",

                'country' => isset($wechat_data['country']) ? $wechat_data['country'] : "",

                'privilege' => isset($wechat_data['privilege']) ? json2array($wechat_data['privilege']) : [],

            ];

        }catch (\Exception $e) {

            throw new \Exception($e->getMessage(),$e->getCode());

        }

        return $result;

    }


    /**
     * 获取支付宝服务号信息
     * @param Request $request
     * @param array $data
     * @return array|bool|mixed|\SimpleXMLElement
     * @throws \Exception
     */
    public function getAlipayFwhInfo(Request $request,$data = []) {

        try{

            $code = $request->param('fwh_code/s','');

            if(!empty($data) && isset($data['code'])) {

                $code = $data['code'];

            }

            $config = $this->alipay_config;

            $aop = new AopClient();

            $aop->gatewayUrl = 'https://openapi.alipay.com/gateway.do';

            $aop->appId = $config['app_id'];

            $aop->rsaPrivateKey = $config['private_key'];

            $aop->alipayrsaPublicKey = $config['ali_public_key'];

            $aop->apiVersion = '1.0';

            $aop->signType = 'RSA2';

            $aop->postCharset='UTF-8';

            $aop->format='json';

            $requestObj = new AlipaySystemOauthTokenRequest();

            $requestObj->setGrantType("authorization_code");

            $requestObj->setCode($code);

            $result = $aop->execute($requestObj);

            if(isset(json2array(json_encode($result))['error_response'])) throw new \Exception(json2array(json_encode($result))['error_response']['sub_msg']);

            $responseNode = str_replace(".", "_", $requestObj->getApiMethodName()) . "_response";

            $resultData = (array) $result->$responseNode;

            if(empty($resultData['access_token'])) throw new \Exception('获取access_token失败');

            $requestObj = new AlipayUserInfoShareRequest();

            $result = $aop->execute($requestObj,$resultData['access_token'] );

            $responseNode = str_replace(".", "_", $requestObj->getApiMethodName()) . "_response";

            $userData = (array) $result->$responseNode;

            if (empty($userData['code']) || $userData['code'] != 10000) throw new \Exception('获取用户信息失败');

            $gender = 0;

            if(isset($userData['gender'])) {

                if($userData['gender'] == 'M') $gender = 1;

                elseif ($userData['gender'] == 'F') $gender = 2;

            }


            $result = [

                'nickname' => isset($userData['nick_name']) ? emojiEncode($userData['nick_name']) : "",

                'avatar' => isset($userData['avatar']) ? $userData['avatar'] : "",

                'userid' => isset($userData['user_id']) ? $userData['user_id'] : "",

                'sex' => $gender,

                'province' => isset($userData['province']) ? $userData['province'] : "",

                'city' => isset($userData['city']) ? $userData['city'] : "",

                //用户类型   1代表公司账户2代表个人账户
                'user_type' => isset($userData['user_type']) ? $userData['user_type'] : "",

                //用户状态	Q代表快速注册用户 T代表已认证用户 B代表被冻结账户 W代表已注册，未激活的账户
                'user_status' => isset($userData['user_status']) ? $userData['user_status'] : "",

                //是否通过实名认证   T是通过 F是没有实名认证
                "is_certified" => isset($userData['is_certified']) ? $userData['is_certified'] :"",

                //是否是学生   T是学生 F不是学生
                "is_student_certified" => isset($userData['is_student_certified']) ? $userData['is_student_certified'] :"",

            ];

        }catch (\Exception $e) {

            throw new \Exception($e->getMessage(),$e->getCode());

        }

        return $result;

    }




    public function getAlipayAppInfo(Request $request,$data = []) {

        try{

            $config = $this->alipay_config;

            $result = [];

        }catch (\Exception $e) {

            throw new \Exception($e->getMessage(),$e->getCode());

        }

        return $result;

    }




}