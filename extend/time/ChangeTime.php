<?php

namespace time;
/**
 * 格式化时间转为几天前、几月前、几年前
 * Class ChangeTime
 * @package time
 */
class ChangeTime
{

    public $createtime, $gettime;

    function __construct($gettime,$createtime = "")
    {
        $this->createtime = empty($createtime) ? time() : $createtime;

        $this->gettime = $gettime;
    }

    function getSeconds()
    {
        return $this->createtime - $this->gettime;
    }

    function getMinutes()
    {
        return ($this->createtime - $this->gettime) / (60);
    }

    function getHours()
    {
        return ($this->createtime - $this->gettime) / (60 * 60);
    }

    function getDay()
    {
        return ($this->createtime - $this->gettime) / (60 * 60 * 24);
    }

    function getMonth()
    {
        return ($this->createtime - $this->gettime) / (60 * 60 * 24 * 30);
    }

    function getYear()
    {
        return ($this->createtime - $this->gettime) / (60 * 60 * 24 * 30 * 12);
    }

    function index()
    {
        if ($this->getYear() > 1) {
            if ($this->getYear() > 2) {
                return date("Y-m-d H:i:s", $this->gettime);
            }
            return intval($this->getYear()) . "年前";
        }
        if ($this->getMonth() > 1) {
            return intval($this->getMonth()) . "月前";
        }
        if ($this->getDay() > 1) {
            return intval($this->getDay()) . "天前";
        }
        if ($this->getHours() > 1) {
            return intval($this->getHours()) . "小时前";
        }
        if ($this->getMinutes() > 1) {
            return intval($this->getMinutes()) . "分钟前";
        }
        if ($this->getSeconds() > 1) {
            return intval($this->getSeconds() - 1) . "秒前";
        }





        if ($this->getYear() < -1) {
            if ($this->getYear() < -2) {
                return date("Y-m-d H:i:s", $this->gettime);
            }
            return intval($this->getYear() * -1) . "年后";
        }
        if ($this->getMonth() < -1) {
            return intval($this->getMonth() * -1) . "月后";
        }
        if ($this->getDay() < -1) {
            return intval($this->getDay() * -1) . "天后";
        }
        if ($this->getHours() < -1) {
            return intval($this->getHours() * -1) . "小时后";
        }
        if ($this->getMinutes() < -1) {
            return intval($this->getMinutes() * -1) . "分钟后";
        }
        if ($this->getSeconds() < -1) {
            return intval(($this->getSeconds() - 1)  * -1) . "秒后";
        }

    }
}