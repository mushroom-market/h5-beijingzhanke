<?php
namespace wechat;
 
use think\Log;
use wechat\base\HttpClient;

class Wechat{
    
    private $wechat_config; 
    
    function __construct($config=array()){
        
        $def_config=array(
            
            'app_id'=>'',
            
            'partner_key'=>'',
            
            'partner_id'=>'',
            
            'notify_url'=>''
            
        );
        
        $this->wechat_config = array_merge($def_config,$config);
        
    }
    
    public function getPayOfMobile($data){
    
        $data['trade_type']='MWEB';
    
        $tran_result = $this->createOrder($data);
    
        if ($tran_result["return_code"] != 'SUCCESS' || $tran_result["result_code"] === 'FAIL') {
    
            throw new \Exception($tran_result['return_msg']);
    
        }
         
        return $tran_result;
    
    }
    
    public function getPayOfPc($data){
    
        $data['trade_type']='NATIVE';
    
        $tran_result = $this->createOrder($data);
    
        if ($tran_result["return_code"] != 'SUCCESS' || $tran_result["result_code"] === 'FAIL') {
    
            throw new \Exception($tran_result['return_msg']);
    
        }
         
        return $tran_result;
    
    }
    
    public function getPayOfWeapp($data){
    
        $data['trade_type']='JSAPI';
    
        $tran_result = $this->createOrder($data);
    
        if ($tran_result["return_code"] != 'SUCCESS' || $tran_result["result_code"] === 'FAIL') {
    
            throw new \Exception($tran_result['return_msg']);
    
        }
         
        return $tran_result;
    
    }
    
    public function getPayOfApp($data){
        
        $tran_result = $this->createOrder($data);
        
        if ($tran_result["return_code"] != 'SUCCESS' || $tran_result["result_code"] === 'FAIL') {
            
            throw new \Exception($tran_result['return_msg']);
            
        }
        
        Log::write(var_export($tran_result,1));
        
        $retval=array(
            
            'appid'=>$tran_result['appid'],
            
            'noncestr'=>$tran_result['nonce_str'],
            
            'package' =>'Sign=WXPay',
            
            'partnerid'=>$this->wechat_config['partner_id'],
            
            'prepayid'=>$tran_result['prepay_id'],
            
            'timestamp'=>time()
            
        );
        
        $retval['sign'] = $this->buildSign($retval); 
        
        return $retval;
    }
    
    public function refundOfApp($data){
        
        return $this->createRefund($data);
        
    }
    
    
    /**
     * Wechat::verifyNotify()
     * 验证服务器通知
     * @param array $data
     * @return array
     */
    
    public function verifyNotify() {
        
        $xml = $this->getXmlArray();
        
        if (! $xml) {
            
            return false;
            
        }
        Log::write(var_export($xml,1));
        
        $AppSignature = $xml['sign'];
        
        unset($xml['sign']);
        
        $sign = strtoupper( md5(urldecode(http_build_query($xml).'&key='.$this->wechat_config['partner_key'])) );
        
        Log::write(var_export(array(
            
            'parent_key'=>$this->wechat_config['partner_key'],
            
            'sign'=>$sign,
            
            'AppSignature'=>$AppSignature
            
        ),1));
        
        if ($AppSignature != $sign) {
            
            return false;
            
        }
        
        return $xml;
        
    }
   
    /**
     * Wechat::getXmlArray()
     * 从xml中获取数组
     * @return array
     */
    private function getXmlArray() {
        
        $xmlData = file_get_contents("php://input");
        
        if ($xmlData) {
            
            $postObj = simplexml_load_string($xmlData, 'SimpleXMLElement', LIBXML_NOCDATA);
            
            if (! is_object($postObj)) {
                
                return false;
                
            }
            
            $array = json_decode(json_encode($postObj), true); // xml对象转数组
            
            return array_change_key_case($array, CASE_LOWER); // 所有键小写
            
        } else {
            
            return false;
            
        }
    }
    
     
     
    /**
     * Wechat::buildSign()
     * 生成sign值
     * @param array $array
     * @return string
     */
    public function buildSign($array) {
        $signPars = "";
        
        ksort($array);
        
        foreach($array as $k => $v) {
            
            $signPars.=$k."=".$v."&";
            
        }
        
        return strtoupper(md5($signPars.'key='.$this->wechat_config['partner_key']));
    
    }
    
 
    
    /**
     * Wechat::createorder()
     * 生成预支付订单
     * @param array $access_token
     * @param array $parameter
     * @return array
     */
    private function createOrder($parameter) {
        
        $url = 'https://api.mch.weixin.qq.com/pay/unifiedorder';
        
        $nonce_str=time().uniqid(); 
        
        $params=array_merge(array(
            
            'appid'=>$this->wechat_config['app_id'],
            
            //'attach'=>$parameter['attach'],
            
            //'body'=>$parameter['body'],
            
            'device_info'=>'WEB',
            
            'goods_tag'=>'WXG',
            
            'mch_id'=>$this->wechat_config['partner_id'],
            
            'nonce_str'=>$nonce_str,
            
            'notify_url'=> $this->wechat_config['notify_url'],
            
            //'out_trade_no'=>$parameter['out_trade_no'],
            
            'spbill_create_ip'=>request()->ip(),
            
            'time_expire'=>date('YmdHis', time()+(5*60) ),
            
            'time_start'=>date('YmdHis',time()),
            
            //'total_fee'=>$parameter['price'],
            
            'trade_type'=>'APP',
            
        ),$parameter);
        
        $params['sign']=$this->buildSign($params);
        
        $xml_mod = new XmlPrass();
        
        $result = HttpClient::quickPost($url, $xml_mod->toXml($params));
        
        $xml=simplexml_load_string($result,'SimpleXMLElement',LIBXML_NOCDATA);
         
        return json_decode(json_encode($xml),1);
    }
    
    
    /**
     * 申请退款
     * @param array $access_token
     * @param array $parameter
     * @return array
     */
    private function createRefund($parameter) {
        
        $url = 'https://api.mch.weixin.qq.com/secapi/pay/refund';
        
        $nonce_str=time().uniqid();
        
        $params=array(
            
            'appid'=>$this->wechat_config['app_id'],
            
            'mch_id'=>$this->wechat_config['partner_id'],
            
            'nonce_str'=>$nonce_str,
            
            'transaction_id'=>$parameter['out_trade_no'],
            
            'out_refund_no'=>$parameter['out_refund_no'],
            
            'total_fee'=>$parameter['total_fee'],
            
            'refund_fee'=>$parameter['refund_fee'],
            
            'op_user_id'=>$this->wechat_config['partner_id'],
            
        );
        
        ksort($params);
        
        $params['sign']=$this->buildSign($params);
        
        $xml_mod = new XmlPrass();
        
        $result = HttpClient::quickPostBySsl($url, $xml_mod->toXml($params));
        
        $xml=simplexml_load_string($result,'SimpleXMLElement',LIBXML_NOCDATA);
        
        return json_decode(json_encode($xml),1);
        
    }
    
}

class XmlPrass {
    private $version  = '1.0';
    private $encoding  = 'UTF-8';
    private $root    = 'xml';
    private $xml    = null;
    function __construct() {
        $this->xml = new \XMLWriter();
    }


    function toXml($data, $eIsArray=false) {
        if(!$eIsArray) {
            $this->xml->openMemory();
            //             $this->xml->startDocument($this->version, $this->encoding);
            $this->xml->startElement($this->root);
        }
        foreach($data as $key => $value){

            if(is_array($value)){
                $this->xml->startElement($key);
                $this->toXml($value, true);
                $this->xml->endElement();
                continue;
            }
            $this->xml->writeElement($key, $value);
        }
        if(!$eIsArray) {
            $this->xml->endElement();
            return $this->xml->outputMemory(true);
        }
    }
}
