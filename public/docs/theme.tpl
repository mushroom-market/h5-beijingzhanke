
.layui-layout-admin .layui-header .layui-logo {
background-color: {{logo}};
color: {{logo_font}};
}

.layui-layout-admin .layui-header {
background-color: {{header}};
}

.layui-layout-admin .layui-header a {
color: {{header_font}};
}

.layui-layout-admin .layui-header a:hover {
color: {{header_font}};
}

.layui-layout-admin .layui-header .layui-nav .layui-nav-more {
border-color: {{header_font}} transparent transparent;
}

.layui-layout-admin .layui-header .layui-nav .layui-nav-mored {
border-color: transparent transparent {{header_font}};
}

.layui-layout-admin .layui-header .layui-nav .layui-this:after, .layui-layout-admin .layui-header .layui-nav-bar {
background-color: {{header_font}};
}

.layui-layout-admin .layui-side {
background-color: {{side}}!important;
}

.layui-nav-tree .layui-nav-child dd.layui-this, .layui-nav-tree .layui-nav-child dd.layui-this a, .layui-nav-tree .layui-this, .layui-nav-tree .layui-this > a, .layui-nav-tree .layui-this > a:hover {
background-color: {{primary}};
}

.layui-nav-tree .layui-nav-bar {
background-color: {{primary}};
}

.layui-btn:not(.layui-btn-primary):not(.layui-btn-normal):not(.layui-btn-warm):not(.layui-btn-danger):not(.layui-btn-disabled) {
background-color: {{primary}};
}

.layui-btn.layui-btn-primary:hover {
border-color: {{primary}};
}

.layui-form-onswitch {
border-color: {{primary}};
background-color: {{primary}};
}

.layui-laypage .layui-laypage-curr .layui-laypage-em {
background-color: {{primary}};
}

.layui-table-page .layui-laypage input:focus {
border-color: {{primary}} !important;
}

.layui-table-view select:focus {
border-color: {{primary}} !important;
}

.layui-table-page .layui-laypage a:hover {
color: {{primary}};
}

.layui-form-radio > i:hover, .layui-form-radioed > i {
color: {{primary}};
}

.layui-form-select dl dd.layui-this {
background-color: {{primary}};
}

.layui-tab-brief > .layui-tab-title .layui-this {
color: {{primary}};
}

.layui-tab-brief > .layui-tab-more li.layui-this:after, .layui-tab-brief > .layui-tab-title .layui-this:after {
border-color: {{primary}} !important;
}

.layui-breadcrumb a:hover {
color: {{primary}} !important;
}

.layui-nav.layui-nav-tree{
background-color:{{side}} !important;
}

.layui-nav-tree .layui-nav-item a:hover{
background-color: {{primary}} !important;
}