<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// [ 应用入口文件 ]
namespace think;

// 加载基础文件
require __DIR__ . '/../thinkphp/base.php';

header("content-type:text/html;charset=utf-8");

//header('Access-Control-Allow-Origin: *');

//header('Access-Control-Allow-Headers:'.implode(',',array(
//
//        "token","userid","sign","timestamp","platform"
//
//    )));

// 响应类型
//header('Access-Control-Allow-Methods:POST,GET');

// 网站路径
define('SITE_PATH', str_replace('\\', '/', __DIR__ . '/'));
//网站域名
define('SITE_URL', isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443' ? 'https://' . $_SERVER['HTTP_HOST'] : 'http://' . $_SERVER['HTTP_HOST']);
//定义应用配置目录
define('CONF_PATH', __DIR__.'/../config/');

$script_name = $_SERVER['SCRIPT_NAME'];

if($script_name == "/index.php") {

    define("LOCAL_URL","");

}else{

    if(strstr($script_name,"/index.php") >= 0) $script_name = str_replace("/index.php","",$script_name);

    define("LOCAL_URL",SITE_URL.$script_name);

}

define("VERSION",time());

// 支持事先使用静态方法设置Request对象和Config对象

// 执行应用并响应
Container::get('app')->run()->send();
