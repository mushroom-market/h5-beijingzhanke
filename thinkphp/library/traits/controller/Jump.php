<?php

/**
 * 用法：
 * class index
 * {
 *     use \traits\controller\Jump;
 *     public function index(){
 *         $this->error();
 *         $this->redirect();
 *     }
 * }
 */
namespace traits\controller;

use think\Container;
use think\exception\HttpResponseException;
use think\Response;
use think\response\Redirect;

trait Jump
{
    /**
     * 应用实例
     * @var \think\App
     */
    protected $app;

    /**
     * 操作成功跳转的快捷方法
     * @access protected
     * @param  mixed     $msg 提示信息
     * @param  string    $url 跳转的URL地址
     * @param  integer    $code 错误码【默认201】
     * @param  mixed     $data 返回的数据
     * @param  integer   $wait 跳转等待时间
     * @param  array     $header 发送的Header信息
     * @return void
     */
    protected function success($msg = '', $url = null,$code = 200, $data = '', $wait = 3, array $header = [])
    {


        if(is_array($msg)) {

            $message = isset($msg['message']) ? $msg['message'] : "操作成功";

            $url = isset($msg['url']) ? $msg['url'] : null;

            $code = isset($msg['code']) ? $msg['code'] : 200;

            $data = isset($msg['data']) ? $msg['data'] : [];

            $wait = isset($msg['wait']) ? $msg['wait'] : 3;

            $header = isset($msg['header']) ? $msg['header'] : [];

            $param = isset($msg['param']) ? $msg['param'] : [];

            $result = [

                'code' => $code,

                'message'  => $message,

                'data' => $data,

                'url'  => $url,

                'wait' => $wait,

            ];

            if(!empty($param)) $result = array_merge($result,$param);

        }else{

            $result = [
                'code' => $code,
                'message'  => $msg,
                'data' => $data,
                'url'  => $url,
                'wait' => $wait,
            ];

        }

        if (is_null($result['url']) && isset($_SERVER["HTTP_REFERER"])) {

            $result['url'] = $_SERVER["HTTP_REFERER"];

        } elseif ('' !== $result['url']) {

            $result['url'] = (strpos($result['url'], '://') || 0 === strpos($result['url'], '/')) ? $result['url'] : Container::get('url')->build($result['url']);

        }

        $type = $this->getResponseType();
        // 把跳转模板的渲染下沉，这样在 response_send 行为里通过getData()获得的数据是一致性的格式
        if ('html' == strtolower($type)) $type = 'jump';

        $response = Response::create($result, $type)->header($header)->options(['jump_template' => $this->app['config']->get('dispatch_success_tmpl')]);

        throw new HttpResponseException($response);


    }

    /**
     * 操作错误跳转的快捷方法
     * @access protected
     * @param  mixed     $msg 提示信息
     * @param  string    $url 跳转的URL地址
     * @param  integer    $code 错误码【默认201】
     * @param  mixed     $data 返回的数据
     * @param  integer   $wait 跳转等待时间
     * @param  array     $header 发送的Header信息
     * @return void
     */
    protected function error($msg = '', $url = null,$code = 201, $data = '', $wait = 3, array $header = [])
    {


        if(is_array($msg)) {

            $message = isset($msg['message']) ? $msg['message'] : "操作失败";

            $url = isset($msg['url']) ? $msg['url'] : null;

            $code = isset($msg['code']) ? $msg['code'] : 201;

            $data = isset($msg['data']) ? $msg['data'] : [];

            $wait = isset($msg['wait']) ? $msg['wait'] : 3;

            $header = isset($msg['header']) ? $msg['header'] : [];

            $param = isset($msg['param']) ? $msg['param'] : [];

            $result = [

                'code' => $code,

                'message'  => $message,

                'data' => $data,

                'url'  => $url,

                'wait' => $wait,

            ];

            if(!empty($param)) $result = array_merge($result,$param);

        }else{

            $result = [
                'code' => $code,
                'message'  => $msg,
                'data' => $data,
                'url'  => $url,
                'wait' => $wait,
            ];

        }



        if (is_null($result['url'])) {

            $result['url'] = $this->app['request']->isAjax() ? '' : 'javascript:history.back(-1);';

        } elseif ('' !== $result['url']) {

            $result['url'] = (strpos($result['url'], '://') || 0 === strpos($result['url'], '/')) ? $result['url'] : $this->app['url']->build($result['url']);

        }


        $type = $this->getResponseType();

        if ('html' == strtolower($type)) $type = 'jump';

        $response = Response::create($result, $type)->header($header)->options(['jump_template' => $this->app['config']->get('dispatch_error_tmpl')]);

        throw new HttpResponseException($response);
    }

    /**
     * 返回封装后的API数据到客户端
     * @access protected
     * @param  mixed     $data 要返回的数据
     * @param  integer   $code 返回的code
     * @param  mixed     $msg 提示信息
     * @param  string    $type 返回数据格式
     * @param  array     $header 发送的Header信息
     * @return void
     */
    protected function result($data, $code = 0, $msg = '', $type = '', array $header = [])
    {
        $result = [
            'code' => $code,
            'msg'  => $msg,
            'time' => time(),
            'data' => $data,
        ];

        $type     = $type ?: $this->getResponseType();
        $response = Response::create($result, $type)->header($header);

        throw new HttpResponseException($response);
    }

    /**
     * URL重定向
     * @access protected
     * @param  string         $url 跳转的URL表达式
     * @param  array|integer  $params 其它URL参数
     * @param  integer        $code http code
     * @param  array          $with 隐式传参
     * @return void
     */
    protected function redirect($url, $params = [], $code = 302, $with = [])
    {
        $response = new Redirect($url);

        if (is_integer($params)) {
            $code   = $params;
            $params = [];
        }

        $response->code($code)->params($params)->with($with);

        throw new HttpResponseException($response);
    }

    /**
     * 获取当前的response 输出类型
     * @access protected
     * @return string
     */
    protected function getResponseType()
    {
        if (!$this->app) {
            $this->app = Container::get('app');
        }

        $isAjax = $this->app['request']->isAjax();
        $config = $this->app['config'];

        return $isAjax
            ? $config->get('default_ajax_return')
            : $config->get('default_return_type');
    }
}
